#include <SFML/Graphics.hpp>
#include <core/InputEngine.hpp>

#include <tile/Tile.hpp>

#include <events/KeyboardEvent.hpp>
#include <events/MouseEvent.hpp>

#include <core/GameEngine.hpp>
#include <core/Util.hpp>

#include <gamestates/Gameplay.hpp>
#include <resources/TextureManager.hpp>
#include <utility>

namespace gg
{

Tile::Tile(
    sf::Vector2f coordinates,
    sf::Texture &texture,
    std::string texture_name,
    unsigned rotation = 0)
    : mCoordinates(coordinates), mTexture(&texture)
{
    mTextureName = std::move(texture_name);
    mSprite.setTexture(*mTexture);
    mSprite.setPosition(mCoordinates);
    mRotation = rotation;

    EventDispatcher::Subscribe(this, EventType::MouseMoved);
    EventDispatcher::Subscribe(this, EventType::MousePressed);
    EventDispatcher::Subscribe(this, EventType::KeyboardReleased);
}

Tile::~Tile() { EventDispatcher::RemoveSubscriber(this); }

void Tile::OnEvent(IEvent *e)
{
    switch (e->GetEventType()) {
    case gg::EventType::MouseMoved:
        OnMouseMoved(e);
        break;
    case gg::EventType::MousePressed:
        OnMousePressed(e);
        break;
    case gg::EventType::KeyboardReleased:
        OnKeyboardReleased(e);
        break;
        ;
    default:
        break;
    }
}

void Tile::OnMouseMoved(IEvent *e)
{
    auto *mouse_event = dynamic_cast<MouseMovedEvent *>(e);
    if (IsContained(
            sf::Vector2f(mouse_event->GetMouseX(), mouse_event->GetMouseY()))) {
        SetHover(true);
    } else {
        SetHover(false);
    }
}

void Tile::OnMousePressed(IEvent *e)
{
    auto *mouse_event = dynamic_cast<MouseButtonEvent *>(e);
    if (mHovered) {
        if (mouse_event->GetKeycode() == GG_PLACE_TILE) {
            dynamic_cast<Gameplay *>(GameEngine::CurrentState())->Build(this);
        }
    }
}

void Tile::OnKeyboardReleased(IEvent *e)
{
    auto *keyboard_released_event =
        static_cast<KeyReleasedEvent *>(e);
    if (mHovered) {
        if (keyboard_released_event->GetKeycode() == GG_ROTATE_TILE) {
            dynamic_cast<Gameplay *>(GameEngine::CurrentState())
                ->SetOrientation(this);
            mRotation = (mRotation + 1) % 4;
        }

        if (keyboard_released_event->GetKeycode() == GG_DEMOLISH_TILE) {
            dynamic_cast<Gameplay *>(GameEngine::CurrentState())
                ->Demolish(this);
        }
    }
}

void Tile::SetHover(bool hovered)
{
    if (mHovered == hovered) {
        return;
    }

    mHovered = hovered;
    ApplyGlow();
}

auto Tile::GetOrientation() -> unsigned { return mRotation; }

// Returns vertices in the following order:
// Left, Up, Right, Down
auto Tile::GetVertices() -> TileVertices
{
    TileVertices vertices;

    vertices.left = sf::Vector2f(mCoordinates.x + 2.0f, mCoordinates.y + 35.0f);
    vertices.top = sf::Vector2f(mCoordinates.x + 65.0f, mCoordinates.y + 2.0f);
    vertices.right =
        sf::Vector2f(mCoordinates.x + 128.0f, mCoordinates.y + 35.0f);
    vertices.bottom =
        sf::Vector2f(mCoordinates.x + 65.0f, mCoordinates.y + 63.0f);

    return vertices;
}

auto Tile::IsContained(const sf::Vector2f &point) -> bool
{
    // https://math.stackexchange.com/a/190117/662860
    double eps = 0.001;

    TileVertices vertices = GetVertices();

    double abp_area = Util::TriangleArea(vertices.left, vertices.top, point);
    double bcp_area = Util::TriangleArea(vertices.top, vertices.right, point);
    double cdp_area =
        Util::TriangleArea(vertices.right, vertices.bottom, point);
    double dap_area = Util::TriangleArea(vertices.bottom, vertices.left, point);

    double sum_area = std::round(abp_area + bcp_area + cdp_area + dap_area);

    double abcd_area = std::round(Util::RectangleArea(
        vertices.left, vertices.top, vertices.right, vertices.bottom));

    if (sum_area <= abcd_area + eps) {
        if (abp_area > eps && bcp_area > eps && cdp_area > eps &&
            dap_area > eps) {
            return true;
        }
    }

    return false;
}

void Tile::ApplyGlow()
{
    if (mHovered) {
        mSprite.setColor(sf::Color::Red);
    } else {
        mSprite.setColor(sf::Color::White);
    }
}

void Tile::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    target.draw(mSprite, states);
}

void Tile::Update()
{
    if (mDirty) {
        if (mHovered) {
            mSprite.setColor(sf::Color::Red);
        } else {
            mSprite.setColor(sf::Color::White);
        }
        mDirty = false;
    }
}

auto Tile::GetCoordinates() -> sf::Vector2f { return mCoordinates; }

auto Tile::GetTexture() -> sf::Texture * { return mTexture; }

void Tile::SetTexture(std::string &texture_name)
{
    mTexture = TextureManager::GetTexture(texture_name);
    mTextureName = texture_name;
    mSprite.setTexture(*mTexture);
}

void Tile::SetCoordinates(sf::Vector2f coordinates)
{
    mCoordinates = coordinates;
    mSprite.setPosition(mCoordinates);
    Align();
}

} // namespace gg
