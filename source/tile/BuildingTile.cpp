#include "core/GameEngine.hpp"
#include "gamestates/Gameplay.hpp"
#include <resources/TextureManager.hpp>
#include <tile/BuildingTile.hpp>

namespace gg
{

BuildingTile::BuildingTile(Attributes &attributes)
    : Tile(
          attributes.Get<sf::Vector2f>("coordinates"),
          *TextureManager::GetTexture(attributes.Get("building_type")),
          attributes.Get("building_type"),
          attributes.Get<unsigned int>("rotation"))
    , mType(StringToBuildingType(attributes.Get("building_type")))
    , mCost(attributes.Get<unsigned int>("cost"))
    , mPopulation(attributes.Get<unsigned int>("population"))
    , mProfit(attributes.Get<unsigned int>("profit"))
{
    BuildingTile::Align();

    for (int i = 0; i < mRotation; i++) {
        dynamic_cast<Gameplay *>(GameEngine::CurrentState())
            ->SetOrientation(this);
    }
}

auto BuildingTile::IsIndustrial() -> bool
{
    return mType == BuildingType::INDUSTRIAL;
}

auto BuildingTile::IsResidential() -> bool
{
    return mType == BuildingType::RESIDENTIAL;
}

auto BuildingTile::GetAttributes() -> Attributes
{
    Attributes attributes;

    attributes.Add("type", "building");
    attributes.Add("building_type", BuildingTypeToString(mType));
    attributes.Add("cost", std::to_string(mCost));
    attributes.Add("population", std::to_string(mPopulation));
    attributes.Add("profit", std::to_string(mProfit));
    attributes.Add("rotation", std::to_string(mRotation));

    return attributes;
}

auto BuildingTile::GetTileType() -> TileType { return TileType::Building; }

auto BuildingTile::GetCost() -> unsigned int { return mCost; }

auto BuildingTile::GetPopulation() -> unsigned int { return mPopulation; }

auto BuildingTile::BuildingTypeToString(const BuildingType type) -> std::string
{
    switch (type) {
        // TODO: fix return strings
    case BuildingType::RESIDENTIAL:
        return "residential_front";
    case BuildingType::INDUSTRIAL:
        return "industrial_front";
    default:
        return "";
    }
}

auto BuildingTile::StringToBuildingType(const std::string &str) -> BuildingType
{
    if (str.find("residential") != std::string::npos)
        return BuildingType::RESIDENTIAL;
    if (str.find("industrial") != std::string::npos)
        return BuildingType::INDUSTRIAL;
    else
        GG_ASSERT(false, "building type string invalid!");
}

auto BuildingTile::GetProfit() -> unsigned int { return mProfit; }

void BuildingTile::Align() { mSprite.move(0.0f, -28.0f); }

} // namespace gg
