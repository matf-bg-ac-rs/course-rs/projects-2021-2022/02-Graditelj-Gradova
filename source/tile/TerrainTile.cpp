#include "core/GameEngine.hpp"
#include "gamestates/Gameplay.hpp"
#include <resources/TextureManager.hpp>
#include <tile/TerrainTile.hpp>

namespace gg
{

TerrainTile::TerrainTile(const Attributes &attributes)
    : Tile(
          attributes.Get<sf::Vector2f>("coordinates"),
          *TextureManager::GetTexture(attributes.Get("terrain_type")),
          attributes.Get("terrain_type"),
          attributes.Get<unsigned int>("rotation"))
    , mTerrainType(StringToTerrainType(attributes.Get("terrain_type")))
{
    TerrainTile::Align();

    for (int i = 0; i < mRotation; i++) {
        dynamic_cast<Gameplay *>(GameEngine::CurrentState())
            ->SetOrientation(this);
    }
}

auto TerrainTile::IsWater() -> bool
{
    return mTerrainType == TerrainType::WATER;
}

auto TerrainTile::IsGrass() -> bool
{
    return mTerrainType == TerrainType::GRASS;
}

auto TerrainTile::GetTerrainType() -> TerrainType { return mTerrainType; }

auto TerrainTile::GetAttributes() -> Attributes
{
    Attributes attributes;

    attributes.Add("type", "terrain");
    attributes.Add("terrain_type", TerrainTypeToString(mTerrainType));
    attributes.Add("rotation", std::to_string(mRotation));

    return attributes;
}

auto TerrainTile::GetTileType() -> TileType { return TileType::Terrain; }

auto TerrainTile::TerrainTypeToString(const TerrainType terrainType) -> std::string
{
    switch (terrainType) {
    case TerrainType::WATER:
        return "water";
    case TerrainType::WATERDOWN:
        return "waterDown_front";
    case TerrainType::CANAL:
        return "canal_front";
    case TerrainType::GRASS:
        return "grass";
    case TerrainType::TREE:
        return "tree_front";
    default:
        return "";
    }
}

auto TerrainTile::StringToTerrainType(const std::string &str) -> TerrainType
{
    if (str == "water")
        return TerrainType::WATER;
    if (str == "waterDown_front")
        return TerrainType::WATERDOWN;
    if (str == "canal_front")
        return TerrainType::CANAL;
    if (str == "grass")
        return TerrainType::GRASS;
    if (str == "tree_front")
        return TerrainType::TREE;
    GG_ASSERT(false, "terrain type string invalid!");
}

void TerrainTile::Align()
{
    if (mTerrainType == TerrainType::WATER)
        mSprite.move(0.0f, 16.0f);
    if (mTerrainType == TerrainType::TREE)
        mSprite.move(0.0f, -9.8f);
}
} // namespace gg
