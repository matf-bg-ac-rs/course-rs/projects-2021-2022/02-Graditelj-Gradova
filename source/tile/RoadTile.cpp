#include "core/GameEngine.hpp"
#include "gamestates/Gameplay.hpp"
#include <resources/TextureManager.hpp>
#include <tile/RoadTile.hpp>

namespace gg
{

RoadTile::RoadTile(Attributes &attributes)
    : Tile(
          attributes.Get<sf::Vector2f>("coordinates"),
          *TextureManager::GetTexture(attributes.Get("road_type")),
          attributes.Get("road_type"),
          attributes.Get<unsigned int>("rotation"))
    , mType(StringToRoadType(attributes.Get("road_type")))
    , mCost(attributes.Get<unsigned int>("cost"))
{
    RoadTile::Align();

    for (int i = 0; i < mRotation; i++) {
        dynamic_cast<Gameplay *>(GameEngine::CurrentState())
            ->SetOrientation(this);
    }
}

void RoadTile::Update()
{
    // TODO: Update
}

void RoadTile::SetType(const RoadType &type) { mType = type; }

auto RoadTile::GetType() -> RoadType { return mType; }

auto RoadTile::GetAttributes() -> Attributes
{
    Attributes attributes;

    attributes.Add("type", "road");
    attributes.Add("road_type", RoadTypeToString(mType));
    attributes.Add("cost", std::to_string(mCost));
    attributes.Add("rotation", std::to_string(mRotation));

    return attributes;
}

auto RoadTile::GetTileType() -> TileType { return TileType::Road; }

auto RoadTile::GetCost() -> unsigned { return mCost; }

auto RoadTile::RoadTypeToString(const RoadType type) -> std::string
{
    switch (type) {
    case RoadType::BRIDGE:
        return "bridge_front";
    case RoadType::BRIDGE2:
        return "bridge2_front";
    case RoadType::STRAIGHT:
        return "straight_front";
    case RoadType::END:
        return "end_front";
    case RoadType::CROSS:
        return "cross";
    case RoadType::DOWN:
        return "down_front";
    case RoadType::T:
        return "t_front";
    case RoadType::CROSSROAD:
        return "crossroad_front";
    default:
        return "";
    }
}

auto RoadTile::StringToRoadType(const std::string &str) -> RoadType
{
    if (str == "bridge_front")
        return RoadType::BRIDGE;
    if (str == "bridge2_front")
        return RoadType::BRIDGE2;
    if (str == "straight_front")
        return RoadType::STRAIGHT;
    if (str == "end_front")
        return RoadType::END;
    if (str == "cross")
        return RoadType::CROSS;
    if (str == "down_front")
        return RoadType::DOWN;
    if (str == "t_front")
        return RoadType::T;
    if (str == "crossroad_front")
        return RoadType::CROSSROAD;
    else
        GG_ASSERT(false, "road type string invalid!");
}

void RoadTile::Align()
{
    if (mType == RoadType::BRIDGE || mType == RoadType::BRIDGE2)
        mSprite.move(0.0f, 1.9f);
    else
        mSprite.move(0.0f, -1.7f);
}
} // namespace gg
