#include <events/SceneEvent.hpp>
#include <gui/ToggleGroup.hpp>

namespace gg
{

ToggleGroup::ToggleGroup() : mActiveToggleIndex(-1), mToggles() {}

void ToggleGroup::AddToggle(ToggleButton *button)
{
    auto it = std::find(mToggles.begin(), mToggles.end(), button);
    GG_ASSERT(it == mToggles.end(), "Button is already in toggle group!");
    mToggles.push_back(button);
    if (button->GetToggleState()) {
        SelectToggle(button);
    }
}

void ToggleGroup::RemoveToggle(ToggleButton *button)
{
    auto it = std::find(mToggles.begin(), mToggles.end(), button);
    GG_ASSERT(it != mToggles.end(), "Button is not in toggle group!");
    mToggles.erase(it);
}

void ToggleGroup::ClearToggle()
{
    mToggles[mActiveToggleIndex]->ToggleState();
    mActiveToggleIndex = -1;
}

void ToggleGroup::SelectToggle(ToggleButton *button)
{
    for (auto *toggle : mToggles) {
        if (toggle->GetToggleState()) {
            toggle->ToggleState();
        }
    }
    button->ToggleState();

    mActiveToggleIndex = 0;
    for (auto *toggle : mToggles) {
        if (toggle->GetToggleState()) {
            break;
        }
        mActiveToggleIndex++;
    }
}

auto ToggleGroup::GetSelectedToggle() -> ToggleButton *
{
    GG_ASSERT(mActiveToggleIndex != -1, "There is not active toggle!");
    return mToggles[mActiveToggleIndex];
}

} // namespace gg
