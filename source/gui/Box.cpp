#include <gui/Box.hpp>
#include <gui/Scene.hpp>

namespace gg
{

Box::Box(const Attributes &attributes) : SceneNode(attributes)
{
    SetSpacing(attributes.Get<float>("Spacing"));
    SetHAlignment(attributes.Get<HAlignment>("HAlignment"));
    SetVAlignment(attributes.Get<VAlignment>("VAlignment"));
    SetMargins(attributes.Get<Margins>("Margins"));
}

void Box::SetSpacing(float spacing) { mSpacing = spacing; }

void Box::SetHAlignment(const HAlignment &horizontalAlignment)
{
    mHorizontalAlignment = horizontalAlignment;
}

void Box::SetVAlignment(const VAlignment &verticalAlignment)
{
    mVerticalAlignment = verticalAlignment;
}

void Box::SetMargins(const Margins &margins) { mMargins = margins; }

void Box::UpdateSize()
{
    for (auto *child : mChildren) {
        child->UpdateSize();
    }
    CalculateSize();
}

void Box::UpdateAlignment()
{
    Align();
    for (auto *child : mChildren) {
        child->UpdateAlignment();
    }
}

void Box::draw(sf::RenderTarget &target, sf::RenderStates states) const {}

HBox::HBox(const Attributes &attributes) : Box(attributes) {}

void HBox::Align()
{
    sf::Vector2f offset = mInsidePosition;

    switch (mHorizontalAlignment) {
    case HAlignment::Left:
        offset.x += mMargins.left;
        break;
    case HAlignment::Center:
        offset.x += mOutsideSize.x * 0.5 +
                    (mMargins.left - mMargins.right) * 0.5 -
                    mInsideSize.x * 0.5;
        break;
    case HAlignment::Right:
        offset.x += mOutsideSize.x - mInsideSize.x - mMargins.right;
        break;
    }

    for (auto *child : mChildren) {
        offset.y = mInsidePosition.y + mMargins.top;
        switch (mVerticalAlignment) {
        case gg::VAlignment::Top:
            break;
        case gg::VAlignment::Center:
            offset.y +=
                (mOutsideSize.y - mMargins.top - mMargins.bottom) * 0.5 -
                child->GetOutsideSize().y * 0.5;
            break;
        case gg::VAlignment::Bottom:
            offset.y += (mOutsideSize.y - mMargins.top - mMargins.bottom) -
                        child->GetOutsideSize().y;
            break;
        }
        child->SetOutsidePosition(offset);

        offset.x += child->GetOutsideSize().x + mSpacing;
    }
}

void HBox::CalculateSize()
{
    sf::Vector2f children_size(0.0f, 0.0f);
    for (auto *child : mChildren) {
        children_size.x += child->GetOutsideSize().x + mSpacing;
        children_size.y = std::max(children_size.y, child->GetOutsideSize().y);
    }
    children_size.x -= mSpacing;

    sf::Vector2f margins_size(
        mMargins.left + mMargins.right, mMargins.top + mMargins.bottom);

    mInsideSize = children_size;
    mOutsideSize = mInsideSize + margins_size;

    if (this->IsRoot()) {
        mOutsideSize = GetScene()->GetSize();
    }
}

VBox::VBox(const Attributes &attributes) : Box(attributes) {}

void VBox::Align()
{
    sf::Vector2f offset = mInsidePosition;

    switch (mVerticalAlignment) {
    case gg::VAlignment::Top:
        offset.y += mMargins.top;
        break;
    case gg::VAlignment::Center:
        offset.y += mOutsideSize.y * 0.5 +
                    (mMargins.top - mMargins.bottom) * 0.5 -
                    mInsideSize.y * 0.5;
        break;
    case gg::VAlignment::Bottom:
        offset.y += mOutsideSize.y - mInsideSize.y - mMargins.bottom;
        break;
    }

    for (auto *child : mChildren) {
        offset.x = mInsidePosition.x + mMargins.left;
        switch (mHorizontalAlignment) {
        case HAlignment::Left:
            break;
        case HAlignment::Center:
            offset.x +=
                (mOutsideSize.x - mMargins.left - mMargins.right) * 0.5 -
                child->GetOutsideSize().x * 0.5;
            break;
        case HAlignment::Right:
            offset.x += mOutsideSize.x - mMargins.left - mMargins.right -
                        child->GetOutsideSize().x;
            break;
        }
        child->SetOutsidePosition(offset);

        offset.y += child->GetOutsideSize().y + mSpacing;
    }
}

void VBox::CalculateSize()
{
    sf::Vector2f children_size(0.0f, 0.0f);
    for (auto *child : mChildren) {
        children_size.x = std::max(children_size.x, child->GetOutsideSize().x);
        children_size.y += child->GetOutsideSize().y + mSpacing;
    }
    children_size.y -= mSpacing;

    sf::Vector2f margins_size(
        mMargins.left + mMargins.right, mMargins.top + mMargins.bottom);

    mInsideSize = children_size;
    mOutsideSize = mInsideSize + margins_size;

    if (this->IsRoot()) {
        mOutsideSize = GetScene()->GetSize();
    }
}

} // namespace gg
