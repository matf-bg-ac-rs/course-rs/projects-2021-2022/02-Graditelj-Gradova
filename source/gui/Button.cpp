#include <core/InputEngine.hpp>
#include <events/MouseEvent.hpp>
#include <events/SceneEvent.hpp>
#include <gui/Button.hpp>
#include <gui/Scene.hpp>
#include <resources/Attributes.hpp>
#include <resources/FontMenager.hpp>
#include <resources/XMLDocument.hpp>

namespace gg
{

Button::Button(const Attributes &attributes)
    : SceneNode(attributes), mText(), mShape(), mState(ButtonState::Normal)
{
    SetText(attributes.Get("Text"));
    SetSize(attributes.Get<float>("Width"), attributes.Get<float>("Height"));

    SetHAlignment(attributes.Get<HAlignment>("HAlignment"));
    SetVAlignment(attributes.Get<VAlignment>("VAlignment"));

    SetMargins(attributes.Get<Margins>("Margins"));

    SetID(attributes.Get("ID"));

    EventDispatcher::Subscribe(this, EventType::MouseMoved);
    EventDispatcher::Subscribe(this, EventType::MousePressed);
    EventDispatcher::Subscribe(this, EventType::MouseReleased);
}

Button::~Button() { EventDispatcher::RemoveSubscriber(this); }

void Button::SetText(const std::string &text) { mText.setString(text); }

void Button::SetSize(const float width, const float height)
{
    SetOutsideSize(sf::Vector2f(width, height));
}

void Button::SetHAlignment(HAlignment hAlignment)
{
    mTextHAlignment = hAlignment;
}

void Button::SetVAlignment(VAlignment vAlignment)
{
    mTextVAlignment = vAlignment;
}

void Button::SetMargins(Margins margins) { mMargins = margins; }

void Button::OnEvent(IEvent *e)
{
    switch (e->GetEventType()) {
    case gg::EventType::MouseMoved:
        OnMouseMovedEvent(e);
        break;
    case gg::EventType::MousePressed:
        OnMousePressedEvent(e);
        break;
    case gg::EventType::MouseReleased:
        OnMouseReleasedEvent(e);
        break;
    default:
        break;
    }
}

void Button::SetState(ButtonState state)
{
    if (state == mState) {
        return;
    }

    mState = state;
    this->ApplyStyle();
}

void Button::OnMouseMovedEvent(IEvent *e)
{
    auto *event = static_cast<MouseMovedEvent *>(e);
    if (this->ContainsPoint(
            sf::Vector2f(event->GetMouseX(), event->GetMouseY()))) {
        if (mState == ButtonState::Normal) {
            SetState(ButtonState::Hovered);
        }
    } else if (mState == ButtonState::Hovered) {
        SetState(ButtonState::Normal);
    }
}

void Button::OnMousePressedEvent(IEvent *e)
{
    if (!mScene->IsActive()) {
        return;
    }

    auto *event = static_cast<MouseButtonEvent *>(e);
    if (event->GetKeycode() == sf::Mouse::Button::Left) {
        if (mState == ButtonState::Hovered) {
            SetState(ButtonState::Pressed);
        }
    }
}

void Button::OnMouseReleasedEvent(IEvent *e)
{
    if (!mScene->IsActive()) {
        return;
    }

    auto *event = static_cast<MouseButtonEvent *>(e);
    if (event->GetKeycode() == sf::Mouse::Button::Left) {
        if (mState == ButtonState::Pressed) {
            EventDispatcher::NotifySubscribers(new SceneButtonEvent(mId));
            if (this->ContainsPoint(InputEngine::GetMousePosition())) {
                SetState(ButtonState::Hovered);
            } else {
                SetState(ButtonState::Normal);
            }
        }
    }
}

void Button::ApplyStyle()
{
    SceneNode::ApplyStyle();

    for (auto &child : mStyle->GetChildren()) {

        if (child.GetName() == "Background") {
            mShape.setFillColor(
                child.GetAttributes().Get<sf::Color>("NormalColor"));
            mShape.SetRadius(child.GetAttributes().Get<float>("Radius"));
            if (child.GetAttributes().Has("NormalBorderSize")) {
                mShape.setOutlineThickness(
                    child.GetAttributes().Get<unsigned int>(
                        "NormalBorderSize"));
                mShape.setOutlineColor(
                    child.GetAttributes().Get<sf::Color>("NormalBorderColor"));
            }
            if (mState == ButtonState::Hovered) {
                if (child.GetAttributes().Has("HoveredColor")) {
                    mShape.setFillColor(
                        child.GetAttributes().Get<sf::Color>("HoveredColor"));
                }
                if (child.GetAttributes().Has("HoveredBorderSize")) {
                    mShape.setOutlineThickness(
                        child.GetAttributes().Get<unsigned int>(
                            "HoveredBorderSize"));
                    mShape.setOutlineColor(child.GetAttributes().Get<sf::Color>(
                        "HoveredBorderColor"));
                }
            } else if (mState == ButtonState::Pressed) {
                if (child.GetAttributes().Has("PressedColor")) {
                    mShape.setFillColor(
                        child.GetAttributes().Get<sf::Color>("PressedColor"));
                }
                if (child.GetAttributes().Has("PressedBorderSize")) {
                    mShape.setOutlineThickness(
                        child.GetAttributes().Get<unsigned int>(
                            "PressedBorderSize"));
                    mShape.setOutlineColor(child.GetAttributes().Get<sf::Color>(
                        "PressedBorderColor"));
                }
            }
        }

        if (child.GetName() == "Text") {
            mText.setFont(
                *FontManager::GetFont(child.GetAttributes().Get("FontName")));
            mText.setStyle(child.GetAttributes().Get<unsigned int>("Style"));
            SetTextCharacterSize(
                child.GetAttributes().Get<unsigned int>("NormalFontSize"));
            mText.setFillColor(
                child.GetAttributes().Get<sf::Color>("NormalColor"));
            if (mState == ButtonState::Hovered) {
                if (child.GetAttributes().Has("HoveredColor")) {
                    mText.setFillColor(
                        child.GetAttributes().Get<sf::Color>("HoveredColor"));
                }
                if (child.GetAttributes().Has("HoveredFontSize")) {
                    SetTextCharacterSize(
                        child.GetAttributes().Get<unsigned int>(
                            "HoveredFontSize"));
                }
            } else if (mState == ButtonState::Pressed) {
                if (child.GetAttributes().Has("PressedColor")) {
                    mText.setFillColor(
                        child.GetAttributes().Get<sf::Color>("PressedColor"));
                }
                if (child.GetAttributes().Has("HoveredFontSize")) {
                    SetTextCharacterSize(
                        child.GetAttributes().Get<unsigned int>(
                            "PressedFontSize"));
                }
            }
        }
    }
}

void Button::ApplyDesign()
{
    SceneNode::ApplyDesign();

    mShape.setPosition(mOutsidePosition);
    mShape.SetSize(mOutsideSize);
    mText.setPosition(mInsidePosition);
}

void Button::UpdateDesign()
{
    float text_height = mText.getCharacterSize() * 5 / 4;
    float text_width = mText.getGlobalBounds().width;

    if (mTextHAlignment == HAlignment::Left) {
        mInsidePosition.x = mOutsidePosition.x;
        mInsidePosition.x += mMargins.left;
    } else if (mTextHAlignment == HAlignment::Center) {
        mInsidePosition.x =
            mOutsidePosition.x + (mOutsideSize.x * 0.5) - text_width * 0.5;
    } else if (mTextHAlignment == HAlignment::Right) {
        mInsidePosition.x = mOutsidePosition.x + mOutsideSize.x - text_width;
        mInsidePosition.x -= mMargins.right;
    }

    if (mTextVAlignment == VAlignment::Top) {
        mInsidePosition.y = mOutsidePosition.y;
        mInsidePosition.y += mMargins.top;
    } else if (mTextVAlignment == VAlignment::Center) {
        mInsidePosition.y =
            mOutsidePosition.y + (mOutsideSize.y * 0.5) - text_height * 0.5;
    } else if (mTextVAlignment == VAlignment::Bottom) {
        mInsidePosition.y = mOutsidePosition.y + mOutsideSize.y - text_height;
        mInsidePosition.y -= mMargins.bottom;
    }

    ApplyDesign();
}

void Button::SetTextCharacterSize(unsigned int size)
{
    mText.setCharacterSize(size);
    UpdateDesign();
}

void Button::SetTextFontName(const std::string &fontname)
{
    mText.setFont(*FontManager::GetFont(fontname));
    UpdateDesign();
}

void Button::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    target.draw(mShape, states);
    target.draw(mText, states);
}

}; // namespace gg
