#include <gui/Label.hpp>

#include <resources/Attributes.hpp>
#include <resources/FontMenager.hpp>
#include <resources/XMLDocument.hpp>

namespace gg
{
Label::Label(const Attributes &attributes) : SceneNode(attributes), mText()
{
    SetText(attributes.Get("Text"));

    auto text_style = mStyle->GetChildren()[0];

    GG_ASSERT(text_style.GetName() == "Text", "Invalid label style!");

    SetFontName(text_style.GetAttributes().Get("FontName"));
    SetFontSize(text_style.GetAttributes().Get<unsigned int>("FontSize"));

    if (attributes.Has("ID")) {
        SetID(attributes.Get("ID"));
    }

    SetColor(text_style.GetAttributes().Get<sf::Color>("FillColor"));
}

void Label::SetText(const std::string &text)
{
    mText.setString(text);
    CalculateSize();
}

void Label::SetFontName(const std::string &fontname)
{
    mText.setFont(*FontManager::GetFont(fontname));
    CalculateSize();
}

void Label::SetFontSize(const unsigned characterSize)
{
    mText.setCharacterSize(characterSize);
    CalculateSize();
}

void Label::SetColor(const sf::Color &color)
{
    mColor = color;
    Label::ApplyStyle();
}

void Label::UpdateSize() { CalculateSize(); }

void Label::Render(sf::RenderTarget &target, sf::RenderStates states) const
{
    draw(target, states);
}

void Label::ApplyStyle()
{
    auto text_style = mStyle->GetChildren()[0];
    GG_ASSERT(text_style.GetName() == "Text", "Invalid label style!");

    mBackground.setFillColor(sf::Color::Red);

    mText.setStyle(text_style.GetAttributes().Get<unsigned int>("Style"));
    mText.setFillColor(mColor);
    if (text_style.GetAttributes().Has("OutlineThickness")) {
        mText.setOutlineThickness(
            text_style.GetAttributes().Get<float>("OutlineThickness"));
        mText.setOutlineColor(
            text_style.GetAttributes().Get<sf::Color>("OutlineColor"));
    }
}

void Label::ApplyDesign()
{
    mText.setPosition(mOutsidePosition);
    mBackground.setPosition(mOutsidePosition);
    mBackground.setSize(mOutsideSize);
}

void Label::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    // target.draw(m_background, states);
    target.draw(mText, states);
}

void Label::CalculateSize()
{
    SetOutsideSize(sf::Vector2f(
        mText.getGlobalBounds().width, mText.getCharacterSize() * 5 / 4));
    SetDirty();
}

} // namespace gg
