#include <core/InputEngine.hpp>
#include <events/MouseEvent.hpp>
#include <events/SceneEvent.hpp>
#include <gui/Scene.hpp>
#include <gui/ToggleButton.hpp>
#include <resources/FontMenager.hpp>
#include <resources/XMLDocument.hpp>

namespace gg
{

ToggleButton::ToggleButton(const Attributes &attributes)
    : Button(attributes), mToggleState(false)
{
    if (attributes.Has("ToggleState")) {
        mToggleState = attributes.Get<bool>("ToggleState");
    }
}

void ToggleButton::ToggleState()
{
    mToggleState = !mToggleState;
    if (mScene->IsActive()) {
        EventDispatcher::NotifySubscribers(
            new SceneToggleButtonEvent(mId, mToggleState));
    }

    this->ApplyStyle();
}

auto ToggleButton::GetToggleState() -> bool { return mToggleState; }

void ToggleButton::SetToggleGroup()
{
    if (mToggleGroup != "") {
        mScene->AddToggleButtonToToggleGroup(mToggleGroup, this);
    }
}

void ToggleButton::ApplyStyle()
{
    Button::ApplyStyle();

    if (mToggleState) {
        for (auto &child : mStyle->GetChildren()) {

            if (child.GetName() == "ToggleBackground") {
                mShape.setFillColor(
                    child.GetAttributes().Get<sf::Color>("NormalColor"));
                if (child.GetAttributes().Has("NormalBorderSize")) {
                    mShape.setOutlineThickness(
                        child.GetAttributes().Get<unsigned int>(
                            "NormalBorderSize"));
                    mShape.setOutlineColor(child.GetAttributes().Get<sf::Color>(
                        "NormalBorderColor"));
                }
                if (mState == ButtonState::Hovered) {
                    if (child.GetAttributes().Has("HoveredColor")) {
                        mShape.setFillColor(
                            child.GetAttributes().Get<sf::Color>(
                                "HoveredColor"));
                    }
                    if (child.GetAttributes().Has("HoveredBorderSize")) {
                        mShape.setOutlineThickness(
                            child.GetAttributes().Get<unsigned int>(
                                "HoveredBorderSize"));
                        mShape.setOutlineColor(
                            child.GetAttributes().Get<sf::Color>(
                                "HoveredBorderColor"));
                    }
                } else if (mState == ButtonState::Pressed) {
                    if (child.GetAttributes().Has("PressedColor")) {
                        mShape.setFillColor(
                            child.GetAttributes().Get<sf::Color>(
                                "PressedColor"));
                    }
                    if (child.GetAttributes().Has("PressedBorderSize")) {
                        mShape.setOutlineThickness(
                            child.GetAttributes().Get<unsigned int>(
                                "PressedBorderSize"));
                        mShape.setOutlineColor(
                            child.GetAttributes().Get<sf::Color>(
                                "PressedBorderColor"));
                    }
                }
            }

            if (child.GetName() == "ToggleText") {
                mText.setStyle(
                    child.GetAttributes().Get<unsigned int>("Style"));
                Button::SetTextCharacterSize(
                    child.GetAttributes().Get<unsigned int>("NormalFontSize"));
                mText.setFillColor(
                    child.GetAttributes().Get<sf::Color>("NormalColor"));
                if (mState == ButtonState::Hovered) {
                    if (child.GetAttributes().Has("HoveredColor")) {
                        mText.setFillColor(child.GetAttributes().Get<sf::Color>(
                            "HoveredColor"));
                    }
                    if (child.GetAttributes().Has("HoveredFontSize")) {
                        SetTextCharacterSize(
                            child.GetAttributes().Get<unsigned int>(
                                "HoveredFontSize"));
                    }
                } else if (mState == ButtonState::Pressed) {
                    if (child.GetAttributes().Has("PressedColor")) {
                        mText.setFillColor(child.GetAttributes().Get<sf::Color>(
                            "PressedColor"));
                    }
                    if (child.GetAttributes().Has("HoveredFontSize")) {
                        SetTextCharacterSize(
                            child.GetAttributes().Get<unsigned int>(
                                "PressedFontSize"));
                    }
                }
            }
        }
    }
}

void ToggleButton::OnMouseReleasedEvent(IEvent *e)
{
    auto *event = static_cast<MouseButtonEvent *>(e);
    if (event->GetKeycode() == sf::Mouse::Button::Left) {
        if (mState == ButtonState::Pressed) {
            if (mToggleGroup == "") {
                ToggleState();
            } else {
                mScene->GetToggleGroup(mToggleGroup)->SelectToggle(this);
            }
            if (this->ContainsPoint(InputEngine::GetMousePosition())) {
                SetState(ButtonState::Hovered);
            } else {
                SetState(ButtonState::Normal);
            }
        }
    }
}

} // namespace gg
