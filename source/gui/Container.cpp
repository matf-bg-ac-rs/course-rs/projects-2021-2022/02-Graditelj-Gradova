#include <gui/Container.hpp>
#include <gui/Scene.hpp>
#include <resources/XMLDocument.hpp>

namespace gg
{

Container::Container(const Attributes &attributes)
    : SceneNode(attributes), mShape()
{
    GG_ASSERT(
        attributes.Has("Size") ^ attributes.Has("RelativeSize"),
        "Container must have eighter Size or RelativeSize attribute!");

    if (attributes.Has("Size")) {
        mRelative = false;
        SetOutsideSize(attributes.Get<sf::Vector2f>("Size"));
    }

    if (attributes.Has("RelativeSize")) {
        mRelative = true;
    }

    SetMargins(attributes.Get<Margins>("Margins"));
}

void Container::SetMargins(Margins margins) { mMargins = margins; }

void Container::SetColor(sf::Color color) { mShape.setFillColor(color); }

void Container::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    target.draw(mShape, states);
}

void Container::UpdateSize()
{
    for (auto &child : mChildren) {
        child->UpdateSize();
    }

    sf::Vector2f margin_size = sf::Vector2f(
        mMargins.left + mMargins.right, mMargins.top + mMargins.bottom);
    if (mRelative) {
        sf::Vector2f child_size = mChildren[0]->GetOutsideSize();
        SetOutsideSize(child_size + margin_size);
    }

    mInsideSize = mOutsideSize - margin_size;
}

void Container::UpdateAlignment()
{
    mInsidePosition.x = mOutsidePosition.x + mMargins.left;
    mInsidePosition.y = mOutsidePosition.y + mMargins.top;

    auto &child = mChildren[0];
    child->SetOutsidePosition(mInsidePosition);
    child->UpdateAlignment();
}

void Container::ApplyDesign()
{
    mShape.setPosition(mOutsidePosition);
    mShape.SetSize(mOutsideSize);
}

void Container::ApplyStyle()
{
    SceneNode::ApplyStyle();

    auto container_style = mStyle->GetChildren()[0];

    GG_ASSERT(container_style.GetName() == "Shape", "Invalid label style!");

    SetColor(container_style.GetAttributes().Get<sf::Color>("Color"));
    if (container_style.GetAttributes().Has("Radius")) {
        mShape.SetRadius(container_style.GetAttributes().Get<float>("Radius"));
    }

    if (container_style.GetAttributes().Has("OutlineThickness")) {
        mShape.setOutlineThickness(
            container_style.GetAttributes().Get<float>("OutlineThickness"));
        mShape.setOutlineColor(
            container_style.GetAttributes().Get<sf::Color>("OutlineColor"));
    }
}

RoundedShape::RoundedShape(
    const sf::Vector2f &size, float radius, std::size_t pointCount)
    : sf::Shape(), mSize(size), mRadius(radius), mPointCount(pointCount)
{
    update();
}

void RoundedShape::SetSize(const sf::Vector2f &size)
{
    mSize = size;
    update();
}

void RoundedShape::SetRadius(float radius)
{
    mRadius = radius;
    update();
}

void RoundedShape::SetPointCount(std::size_t count)
{
    mPointCount = count;
    update();
}

auto RoundedShape::getPointCount() const -> std::size_t { return mPointCount; }

auto RoundedShape::getPoint(std::size_t index) const -> sf::Vector2f
{
    static const float sPI = 3.141592654f;

    // https://math.stackexchange.com/questions/1958939/parametric-equation-for-rectangular-tubing-with-corner-radius

    float angle = index * 2 * sPI / mPointCount;
    float t = 4 * angle / sPI;

    float a = mSize.x / 2;
    float b = mSize.y / 2;

    sf::Vector2f point;
    if (t >= 0 && t <= 1) {
        point = sf::Vector2f(a, -(b - mRadius) * (2 * t - 1));
    } else if (t > 1 && t <= 2) {
        point = sf::Vector2f(
            a - mRadius + mRadius * std::cos(sPI / 2 * (t - 1)),
            -b + mRadius - mRadius * std::sin(sPI / 2 * (t - 1)));
    } else if (t > 2 && t <= 3) {
        point = sf::Vector2f(-(a - mRadius) * (2 * t - 5), -b);
    } else if (t > 3 && t <= 4) {
        point = sf::Vector2f(
            -a + mRadius - mRadius * std::sin(sPI / 2 * (t - 3)),
            -b + mRadius - mRadius * std::cos(sPI / 2 * (t - 3)));
    } else if (t > 4 && t <= 5) {
        point = sf::Vector2f(-a, (b - mRadius) * (2 * t - 9));
    } else if (t > 5 && t <= 6) {
        point = sf::Vector2f(
            -a + mRadius - mRadius * std::cos(sPI / 2 * (t - 5)),
            b - mRadius + mRadius * std::sin(sPI / 2 * (t - 5)));
    } else if (t > 6 && t <= 7) {
        point = sf::Vector2f((a - mRadius) * (2 * t - 13), b);
    } else if (t > 7 && t <= 8) {
        point = sf::Vector2f(
            a - mRadius + mRadius * std::sin(sPI / 2 * (t - 7)),
            b - mRadius + mRadius * std::cos(sPI / 2 * (t - 7)));
    }

    return sf::Vector2f(point.x + a, point.y + b);
}

} // namespace gg
