#include <gui/Image.hpp>
#include <gui/Scene.hpp>
#include <gui/SceneNode.hpp>
#include <resources/Attributes.hpp>
#include <resources/TextureManager.hpp>

namespace gg
{

Image::Image(const Attributes &attributes) : SceneNode(attributes)
{
    GG_ASSERT(attributes.Has("Texture"), "Image must have Texture attribute!");

    if (attributes.Has("Texture")) {
        mImage.setTexture(
            *TextureManager::GetTexture(attributes.Get("Texture")));
    }

    if (attributes.Has("IntRect")) {
        mImage.setTextureRect(attributes.Get<sf::IntRect>("IntRect"));
    }

    if (attributes.Has("Size")) {
        SetOutsideSize(attributes.Get<sf::Vector2f>("Size"));
    } else {
        SetOutsideSize(sf::Vector2f(
            mImage.getTextureRect().width, mImage.getTextureRect().height));
    }
}

void Image::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    target.draw(mImage, states);
}

void Image::UpdateSize()
{
    if (IsRoot()) {
        mImage.setTextureRect(sf::IntRect(
            mScene->GetPosition().x,
            mScene->GetPosition().y,
            mScene->GetSize().x,
            mScene->GetSize().y));
        SetOutsideSize(sf::Vector2f(
            mImage.getTextureRect().width, mImage.getTextureRect().height));
        GG_DEBUG_TRACE("Size: ({}, {})", mOutsideSize.x, mOutsideSize.y);
    }
}

void Image::ApplyDesign()
{
    mImage.setPosition(mInsidePosition);

    sf::Vector2f scale_factor;
    scale_factor.x = mOutsideSize.x / mImage.getTextureRect().width;
    scale_factor.y = mOutsideSize.y / mImage.getTextureRect().height;
    mImage.setScale(scale_factor);
}

void Image::UpdateDesign() { SceneNode::UpdateDesign(); }

} // namespace gg
