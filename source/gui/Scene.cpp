#include <core/Application.hpp>
#include <events/WindowEvent.hpp>
#include <gui/Box.hpp>
#include <gui/Button.hpp>
#include <gui/Container.hpp>
#include <gui/Image.hpp>
#include <gui/Label.hpp>
#include <gui/Scene.hpp>
#include <gui/SceneNode.hpp>
#include <gui/ToggleButton.hpp>
#include <utility>

namespace gg
{

int Scene::mNodeID = 0;

Scene::Scene(std::string name, XMLDocument document)
    : mName(std::move(name))
    , mPosition(sf::Vector2f(0, 0))
    , mSize(sf::Vector2f(0, 0))
{
    EventDispatcher::Subscribe(this, EventType::WindowResize);

    for (auto &child : document.GetChildren()) {
        SceneNode *root_node = LoadSceneNode(child);
        if (child.GetAttributes().Has("name")) {
            AddRootNode(child.GetAttributes().Get("name"), root_node);
        } else {
            AddRootNodeDefaultName(root_node);
        }
    }
}

Scene::~Scene()
{
    while (mRootNodes.size() > 0) {
        RemoveNode(mRootNodes[0]);
    }

    while (mToggleGroups.size() > 0) {
        RemoveToggleGroup(mToggleGroups.begin()->first);
    }
}

void Scene::AddNode(const std::string &name, SceneNode *node)
{
    GG_ASSERT(
        mNodes.find(name) == mNodes.end(),
        "Node with given name already exists!");

    node->SetName(name);
    node->SetRoot(false);
    node->SetScene(this);
    node->SetToggleGroup();
    mNodes[name] = node;
    for (auto child : node->GetChildren()) {
        AddNodeDefaultName(child);
    }
}

void Scene::AddRootNode(const std::string &name, SceneNode *node)
{
    GG_DEBUG_ASSERT(
        std::find(mRootNodes.begin(), mRootNodes.end(), node) ==
            mRootNodes.end(),
        "Node is already root node exists!");
    GG_DEBUG_ASSERT(
        mNodes.find(name) == mNodes.end(),
        "Node with given name already exists!");

    node->SetName(name);
    node->SetRoot(true);
    node->SetScene(this);
    node->SetToggleGroup();
    mRootNodes.push_back(node);
    mNodes[name] = node;
    for (auto child : node->GetChildren()) {
        AddNodeDefaultName(child);
    }
}

void Scene::AddNodeDefaultName(SceneNode *node)
{
    AddNode(GenerateName(), node);
}

void Scene::AddRootNodeDefaultName(SceneNode *node)
{
    AddRootNode(GenerateName(), node);
}

void Scene::AddToggleButtonToToggleGroup(
    const std::string &group, ToggleButton *button)
{
    auto it = mToggleGroups.find(group);
    if (it == mToggleGroups.end()) {
        mToggleGroups[group] = new ToggleGroup();
    }
    mToggleGroups[group]->AddToggle(button);
}

auto Scene::GetNode(const std::string &name) -> SceneNode *
{
    auto it = mNodes.find(name);
    GG_ASSERT(it != mNodes.end(), "Node with given name does not exist!");
    return it->second;
}

auto Scene::GetNode(const std::string &name) const -> const SceneNode *
{
    auto it = mNodes.find(name);
    GG_ASSERT(it != mNodes.end(), "Node with given name does not exist!");
    return it->second;
}

auto Scene::GetToggleGroup(const std::string &group) -> ToggleGroup *
{
    auto it = mToggleGroups.find(group);
    GG_ASSERT(it != mToggleGroups.end(), "ToggleGroup does not exist!");
    return it->second;
}

auto Scene::GetToggleGroup(const std::string &group) const -> const ToggleGroup *
{
    auto it = mToggleGroups.find(group);
    GG_ASSERT(it != mToggleGroups.end(), "ToggleGroup does not exist!");
    return it->second;
}

auto Scene::GetLabelByID(const std::string &id) -> Label *
{
    for (auto &root : mRootNodes) {
        auto *label = GetLabelByID(root, id);
        if (label != nullptr) {
            return label;
        }
    }
    GG_DEBUG_ASSERT(false, "Label with given ID not found!");
    return nullptr;
}

auto Scene::GetLabelByID(SceneNode *node, const std::string &id) -> Label *
{
    if (node->GetID() == id) {
        return static_cast<Label *>(node);
    }

    for (auto &child : node->GetChildren()) {
        auto *label = GetLabelByID(child, id);
        if (label != nullptr) {
            return label;
        }
    }

    return nullptr;
}

void Scene::RemoveNode(const std::string &name)
{
    auto it = mNodes.find(name);
    GG_ASSERT(it != mNodes.end(), "Node with given name does not exist!");
    RemoveNode(it->second);
}

void Scene::RemoveNode(SceneNode *node)
{
    if (node->IsRoot()) {
        mRootNodes.erase(std::find(mRootNodes.begin(), mRootNodes.end(), node));
    } else {
        node->GetParent()->RemoveChild(node);
    }

    for (auto child : node->GetChildren()) {
        RemoveNode(child);
    }

    mNodes.erase(node->GetName());
    delete node;
}

void Scene::RemoveToggleGroup(const std::string &group)
{
    auto it = mToggleGroups.find(group);
    GG_ASSERT(
        it != mToggleGroups.end(),
        "ToggleGroup with given name does not exist!");
    delete it->second;
    mToggleGroups.erase(it);
}

void Scene::SetActive(bool active) { mActive = active; }

auto Scene::IsActive() -> bool { return mActive; }

void Scene::Update()
{
    for (auto node : mRootNodes) {
        if (node->IsDirty()) {
            node->Update();
        }
    }
}

auto Scene::GetName() const -> const std::string & { return mName; }

auto Scene::GetPosition() const -> const sf::Vector2f { return mPosition; }

auto Scene::GetSize() const -> const sf::Vector2f { return mSize; }

void Scene::OnEvent(IEvent *e)
{
    auto *event = static_cast<WindowResizeEvent *>(e);
    UpdateSize(event->GetWidth(), event->GetHeight());
}

void Scene::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    for (auto root : mRootNodes) {
        root->Render(target, states);
    }
}

auto Scene::LoadSceneNode(XMLDocument node) -> SceneNode *
{
    SceneNode *scene_node;

    if (node.GetName() == "VBox") {
        scene_node = new VBox(node.GetAttributes());
    } else if (node.GetName() == "HBox") {
        scene_node = new HBox(node.GetAttributes());
    } else if (node.GetName() == "Label") {
        scene_node = new Label(node.GetAttributes());
    } else if (node.GetName() == "Button") {
        scene_node = new Button(node.GetAttributes());
    } else if (node.GetName() == "ToggleButton") {
        scene_node = new ToggleButton(node.GetAttributes());
    } else if (node.GetName() == "Image") {
        scene_node = new Image(node.GetAttributes());
    } else if (node.GetName() == "Container") {
        scene_node = new Container(node.GetAttributes());
    } else {
        GG_DEBUG_ASSERT(false, "Invalid tag name: " + node.GetName());
        return nullptr;
    }

    for (auto &child : node.GetChildren()) {
        SceneNode *child_node = LoadSceneNode(child);
        scene_node->AppendChild(child_node);
    }

    return scene_node;
}

void Scene::UpdateSize(unsigned width, unsigned height)
{
    mSize = sf::Vector2f(width, height);
    for (auto *root : mRootNodes) {
        root->SetDirty();
    }
}

auto Scene::GenerateName() const -> std::string
{
    return "node" + std::to_string(mNodeID++);
}

} // namespace gg
