#include <events/MouseEvent.hpp>
#include <gui/Scene.hpp>
#include <gui/SceneNode.hpp>
#include <resources/FontMenager.hpp>
#include <resources/StyleManager.hpp>

namespace gg
{

SceneNode::SceneNode(const Attributes &attributes, const bool root)
    : mRoot(root), mParent(nullptr), mDirty(true)
{
    if (attributes.Has("Style")) {
        mStyle = StyleManager::GetStyle(attributes.Get("Style"));
    }

    if (attributes.Has("Position")) {
        SetOutsidePosition(attributes.Get<sf::Vector2f>("Position"));
    }

    if (attributes.Has("Size")) {
        SetOutsideSize(attributes.Get<sf::Vector2f>("Size"));
    }

    if (attributes.Has("ToggleGroup")) {
        mToggleGroup = attributes.Get("ToggleGroup");
    }
}

SceneNode::~SceneNode() = default;

void SceneNode::SetName(const std::string &name) { mName = name; }

auto SceneNode::GetName() const -> const std::string & { return mName; }

void SceneNode::SetRoot(bool root) { mRoot = root; }

auto SceneNode::IsRoot() const -> const bool { return mRoot; }

void SceneNode::SetParent(SceneNode *parent) { mParent = parent; }

auto SceneNode::GetParent() -> SceneNode * { return mParent; }

void SceneNode::SetScene(Scene *scene) { mScene = scene; }

auto SceneNode::GetScene() -> Scene * { return mScene; }

void SceneNode::SetToggleGroup() {}

void SceneNode::SetID(const std::string &id) { mId = id; }

auto SceneNode::GetID() const -> const std::string & { return mId; }

void SceneNode::SetInsidePosition(const sf::Vector2f &insidePosition)
{
    mInsidePosition = insidePosition;
}

void SceneNode::SetOutsidePosition(const sf::Vector2f &outsidePosition)
{
    if (mOutsidePosition != outsidePosition) {
        mOutsidePosition = outsidePosition;
        mInsidePosition = outsidePosition;
        SetDirty();
    }
}

auto SceneNode::GetInsidePosition() const -> const sf::Vector2f &
{
    return mInsidePosition;
}

auto SceneNode::GetOutsidePosition() const -> const sf::Vector2f &
{
    return mOutsidePosition;
}

void SceneNode::SetInsideSize(const sf::Vector2f &insideSize)
{
    mInsideSize = insideSize;
}

void SceneNode::SetOutsideSize(const sf::Vector2f &outsideSize)
{
    mOutsideSize = outsideSize;
}

auto SceneNode::GetInsideSize() const -> const sf::Vector2f &
{
    return mInsideSize;
}

auto SceneNode::GetOutsideSize() const -> const sf::Vector2f &
{
    return mOutsideSize;
}

void SceneNode::AppendChild(SceneNode *child)
{
    child->SetParent(this);
    mChildren.push_back(child);
    SetDirty();
}

void SceneNode::RemoveChild(SceneNode *child)
{
    auto it = std::find(mChildren.begin(), mChildren.end(), child);
    if (it != mChildren.end()) {
        mChildren.erase(it);
        SetDirty();
    }
}

auto SceneNode::GetChildren() -> std::vector<SceneNode *> { return mChildren; }

void SceneNode::Update()
{
    UpdateSize();
    UpdateAlignment();
    UpdateDesign();
    UpdateStyle();
    ResetDirty();

    for (auto child : mChildren) {
        child->Update();
    }
}

void SceneNode::Render(sf::RenderTarget &target, sf::RenderStates states) const
{
    draw(target, states);
    for (auto child : mChildren) {
        child->Render(target, states);
    }
}

void SceneNode::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    target.draw(mBackground, states);
}

void SceneNode::UpdateSize()
{
    for (auto *child : mChildren) {
        child->UpdateSize();
    }
}

void SceneNode::UpdateDesign()
{
    ApplyDesign();
    for (auto *child : mChildren) {
        child->UpdateDesign();
    }
}

void SceneNode::UpdateStyle()
{
    ApplyStyle();
    for (auto *child : mChildren) {
        child->UpdateSize();
    }
}

void SceneNode::UpdateAlignment()
{
    for (auto *child : mChildren) {
        child->UpdateAlignment();
    }
}

auto SceneNode::IsDirty() const -> bool { return mDirty; }

void SceneNode::SetDirty()
{
    mDirty = true;
    if (mParent && !mParent->IsDirty()) {
        mParent->SetDirty();
    }
}

void SceneNode::ResetDirty()
{
    mDirty = false;
    for (auto child : mChildren) {
        child->ResetDirty();
    }
}

auto SceneNode::ContainsPoint(const sf::Vector2f &point) -> bool
{
    return mBackground.getGlobalBounds().contains(point);
}

void SceneNode::ApplyDesign()
{
    mBackground.setPosition(mOutsidePosition);
    mBackground.setSize(mOutsideSize);
}

void SceneNode::ApplyStyle()
{
    mBackground.setFillColor(sf::Color::Transparent);
    mBackground.setOutlineThickness(0);
    mBackground.setOutlineColor(sf::Color::Transparent);
}

}; // namespace gg
