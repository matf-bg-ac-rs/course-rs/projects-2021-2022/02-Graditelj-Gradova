#include "../include/core/Application.hpp"

auto main() -> int
{
    gg::Logger::Init();
    GG_TRACE("spdlog initialized!");

    GG_INFO("Application initializing...");
    auto *app = new gg::Application();

    GG_INFO("Application running...");
    app->Run();

    GG_INFO("Application terminating...");
    delete app;

    exit(EXIT_SUCCESS);
}
