#include "gamestates/Gameplay.hpp"
#include "events/KeyboardEvent.hpp"
#include "events/WindowEvent.hpp"
#include "gamestates/PauseMenu.hpp"
#include "gui/Label.hpp"
#include <core/Application.hpp>
#include <events/TimeEvent.hpp>
#include <gamestates/ControlsMenu.hpp>
#include <utility>

namespace gg
{

Gameplay::Gameplay(std::string saveFilePath)
    : GameState(SceneType::Gameplay)
    , mSaveFilePath(std::move(saveFilePath))
    , mCityPopulation(0)
    , mCityBudget(400)
{
}

Gameplay::~Gameplay() { delete mMap; }

void Gameplay::Enter()
{
    GG_DEBUG_TRACE("Entered GameState: Gameplay");
    GameState::Enter();
    EventDispatcher::Subscribe(this, EventType::KeyboardPressed);
    EventDispatcher::Subscribe(this, EventType::ButtonClicked);
    EventDispatcher::Subscribe(this, EventType::TimePassed);
    // Subscribe on tiles
    if (mMap != nullptr) {
        mMap->SetActive(true);
    }
}

void Gameplay::Exit()
{
    GG_DEBUG_TRACE("Exited GameState: Gameplay");
    GameState::Exit();
    // Unsubcribe on tiles
    if (mMap != nullptr) {
        mMap->SetActive(false);
    }
}

void Gameplay::OnEvent(IEvent *e)
{
    if (e->GetEventType() == EventType::ButtonClicked) {
        auto *button_event = dynamic_cast<SceneButtonEvent *>(e);
        std::string button_id = button_event->GetButtonID();
        if (button_id == "PauseGame") {
            OnPauseEvent();
        }
        if (button_id == "ControlsMenu") {
            OnControlsMenuEvent();
        }
    }
    if (e->GetEventType() == EventType::KeyboardPressed) {
        auto *key_pressed_event = dynamic_cast<KeyboardEvent *>(e);
        // out of the switch because it does not equal to an sfml::Keycode
        if (GG_SAVE_GAME) {
            SaveGame();
        } else if (GG_MOVE_CAMERA_DIAGONALLY_UP_LEFT) {
            OnMoveDiagUpLeft();
        } else if (GG_MOVE_CAMERA_DIAGONALLY_UP_RIGHT) {
            OnMoveDiagUpRight();
        } else if (GG_MOVE_CAMERA_DIAGONALLY_DOWN_LEFT) {
            OnMoveDiagDownLeft();
        } else if (GG_MOVE_CAMERA_DIAGONALLY_DOWN_RIGHT) {
            OnMoveDiagDownRight();
        }

        switch (key_pressed_event->GetKeycode()) {
        case GG_MOVE_CAMERA_UP:
            OnMoveCameraUp();
            break;
        case GG_MOVE_CAMERA_DOWN:
            OnMoveCameraDown();
            break;
        case GG_MOVE_CAMERA_LEFT:
            OnMoveCameraLeft();
            break;
        case GG_MOVE_CAMERA_RIGHT:
            OnMoveCameraRight();
            break;
        case GG_PAUSE_GAME:
            OnPauseEvent();
            break;
        default:
            break;
        }
    }

    if (e->GetEventType() == EventType::TimePassed) {
        auto *time_event = dynamic_cast<TimePassed *>(e);
        if (time_event->GetCategory() == TimeCategory::MINUTE) {
            OnMinutePassedEvent();
        } else if (time_event->GetCategory() == TimeCategory::TEN_SECONDS) {
            OnTenSecondsPassedEvent();
        }
    }
}

void Gameplay::Update()
{
    if (mDirty) {
        mDirty = false;
        auto size =
            Application::GetInstance()->Application::GetWindow()->getSize();
        EventDispatcher::NotifySubscribers(
            new WindowResizeEvent(size.x, size.y));
    }
    mTimer.Update();
    SceneManager::GetScene(m_sceneType)->Update();
    mMap->Update();
}

void Gameplay::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    mMap->draw(dynamic_cast<sf::RenderWindow &>(target));
    if (GameEngine::CurrentState() == this) {
        GameState::draw(target, states);
    }
}

void Gameplay::NewGame()
{
    GG_DEBUG_TRACE("Setting up new game...");

    XMLDocument doc = XMLDocument::LoadFromFile(mSaveFilePath);
    for (auto &child : doc.GetChildren()) {
        if (child.GetName() == "CityName") {
            mCityName = child.GetContent();
        }
    }

    mMap = new Map();
    mMap->LoadFromXML(XMLDocument::LoadFromFile(
        GG_RESOURCES_PATH + GG_RESOURCES_MAP_DEFAULT));

    UpdatePopulationLabel();
    UpdateBudgetLabel();
    UpdatePotentialProfitLabel();
    UpdateInfo("Welcome!", sf::Color::Green);

    SaveGame();
}

void Gameplay::LoadGame()
{
    GG_DEBUG_TRACE("Loading game...");

    XMLDocument doc = XMLDocument::LoadFromFile(mSaveFilePath);

    GG_DEBUG_CRITICAL(doc.GetAttributes().Get<bool>("Init"));
    if (!doc.GetAttributes().Get<bool>("Init")) {
        GameEngine::PopState();
        return;
    }

    for (auto &child : doc.GetChildren()) {
        if (child.GetName() == "CityName") {
            mCityName = child.GetContent();
        } else if (child.GetName() == "CityMap") {
            GG_ASSERT(
                child.GetAttributes().Get("Path") != "", "Map path missing!");
            std::string map_path =
                GG_RESOURCES_PATH + child.GetAttributes().Get("Path");
            mMap = new Map();
            mMap->LoadFromXML(XMLDocument::LoadFromFile(map_path));
        } else if (child.GetName() == "CityPopulation") {
            mCityPopulation = std::stoul(child.GetContent());
            UpdatePopulationLabel();
        } else if (child.GetName() == "CityBudget") {
            mCityBudget = std::stoul(child.GetContent());
            UpdateBudgetLabel();
        } else if (child.GetName() == "PotentialProfit") {
            mPotentialProfit = std::stoul(child.GetContent());
            UpdateBudgetLabel();
        } else if (child.GetName() == "NumberOfIndustrial") {
            mNumberOfIndustrial = std::stoul(child.GetContent());
            UpdateBudgetLabel();
        } else if (child.GetName() == "TaxFactor") {
            mTaxFactor = std::stoul(child.GetContent());
            UpdateBudgetLabel();
        } else {
            GG_ASSERT(
                false,
                "Invalid city parameter in " + mSaveFilePath + " save file!");
        }
    }

    UpdateInfo("Hello there!", sf::Color::Green);
}

void Gameplay::SaveGame()
{
    GG_DEBUG_TRACE("Saving game...");

    Attributes game_attributes;

    game_attributes.Add("Init", "1");
    XMLDocument doc("City", game_attributes, "");

    XMLDocument city_name_doc =
        XMLDocument("CityName", Attributes(), mCityName);
    doc.AddChild(city_name_doc);

    Attributes city_map_attributes;
    city_map_attributes.Add("Path", "maps/" + mCityName + "Map.xml");
    mMap->SaveToFilename(GG_RESOURCES_PATH + city_map_attributes.Get("Path"));
    XMLDocument city_map_doc = XMLDocument("CityMap", city_map_attributes, "");
    doc.AddChild(city_map_doc);

    XMLDocument city_population_doc = XMLDocument(
        "CityPopulation", Attributes(), std::to_string(mCityPopulation));
    doc.AddChild(city_population_doc);

    XMLDocument city_budget_doc =
        XMLDocument("CityBudget", Attributes(), std::to_string(mCityBudget));
    doc.AddChild(city_budget_doc);

    XMLDocument potential_profit_doc = XMLDocument(
        "PotentialProfit", Attributes(), std::to_string(mPotentialProfit));
    doc.AddChild(potential_profit_doc);

    XMLDocument number_of_industrial_doc = XMLDocument(
        "NumberOfIndustrial",
        Attributes(),
        std::to_string(mNumberOfIndustrial));
    doc.AddChild(number_of_industrial_doc);

    XMLDocument tax_factor_doc =
        XMLDocument("TaxFactor", Attributes(), std::to_string(mTaxFactor));
    doc.AddChild(tax_factor_doc);

    XMLDocument::SaveToFile(doc, mSaveFilePath);
}

auto Gameplay::GetMap() -> Map * { return mMap; }

void Gameplay::OnPauseEvent() { GameEngine::PushState(new PauseMenu()); }

void Gameplay::OnControlsMenuEvent()
{
    GameEngine::PushState(new ControlsMenu());
}

void Gameplay::IncreasePopulation(unsigned immigrants)
{
    mCityPopulation += immigrants;
    UpdatePopulationLabel();
}

void Gameplay::DecreasePopulation(unsigned emigrants)
{
    GG_ASSERT(
        mCityPopulation >= emigrants,
        "City population cannot be negative number!");
    mCityPopulation -= emigrants;
    UpdatePopulationLabel();
}

void Gameplay::IncreasePotentialProfit(unsigned profit)
{
    mPotentialProfit += profit;
    UpdatePotentialProfitLabel();
}

void Gameplay::DecreasePotentialProfit(unsigned loss)
{
    GG_ASSERT(
        mPotentialProfit >= loss,
        "City population cannot be negative number!");
    mPotentialProfit -= loss;
    UpdatePotentialProfitLabel();
}

void Gameplay::DecreaseBudget(unsigned amount)
{
    GG_ASSERT(mCityBudget >= amount, "City budget cannot be negative... yet.");
    mCityBudget -= amount;
    GG_INFO("-${}", amount);
    GG_INFO("Current budget: {}", mCityBudget);
    UpdateBudgetLabel();
}

void Gameplay::IncreaseBudget(unsigned amount)
{
    mCityBudget += amount;
    GG_INFO("+${}", amount);
    GG_INFO("Current budget: {}", mCityBudget);
    UpdateBudgetLabel();
}

void Gameplay::CollectTaxes()
{
    GG_INFO("Collecting taxes...");
    IncreaseBudget(2 * mCityPopulation * mTaxFactor);
}

void Gameplay::CollectProfits()
{
    GG_INFO("Collecting profit...");
    if (mNumberOfIndustrial > 0) {
        float rate =
            float(mCityPopulation) / (20.0f * float(mNumberOfIndustrial));
        if (rate > 1.0f) {
            rate = 1.0f;
        }
        IncreaseBudget(unsigned(rate * float(mPotentialProfit)));
    }
}

void Gameplay::IncreaseTaxFactor()
{
    if (mTaxFactor < 3) {
        mTaxFactor++;
    }
}

void Gameplay::DecreaseTaxFactor()
{
    if (mTaxFactor > 0) {
        mTaxFactor--;
    }
}

void Gameplay::OnMinutePassedEvent()
{
    CollectTaxes();
    CollectProfits();
}

void Gameplay::OnTenSecondsPassedEvent() { UpdateInfo("", sf::Color::White); }

void Gameplay::Build(Tile *oldTile)
{

    auto gameplay_menu_toggle_group =
        SceneManager::GetScene(m_sceneType)->GetToggleGroup("GameplayMenu");
    auto selected_toggle_button =
        gameplay_menu_toggle_group->GetSelectedToggle();
    auto toggle_button_id = selected_toggle_button->GetID();

    if (toggle_button_id == "ToggleStraight") {
        BuildRoad(oldTile, RoadType::STRAIGHT);
    } else if (toggle_button_id == "ToggleCross") {
        BuildRoad(oldTile, RoadType::CROSS);
    } else if (toggle_button_id == "ToggleCrossroad") {
        BuildRoad(oldTile, RoadType::CROSSROAD);
    } else if (toggle_button_id == "ToggleDown") {
        BuildRoad(oldTile, RoadType::DOWN);
    } else if (toggle_button_id == "ToggleEnd") {
        BuildRoad(oldTile, RoadType::END);
    } else if (toggle_button_id == "ToggleBridge") {
        BuildBridge(oldTile, RoadType::BRIDGE);
    } else if (toggle_button_id == "ToggleBridge2") {
        BuildBridge(oldTile, RoadType::BRIDGE2);
    } else if (toggle_button_id == "ToggleT") {
        BuildRoad(oldTile, RoadType::T);
    } else if (toggle_button_id == "ToggleResidential") {
        BuildResidential(oldTile);
    } else if (toggle_button_id == "ToggleIndustrial") {
        BuildIndustrial(oldTile);
    } else if (toggle_button_id == "ToggleGrass") {
        BuildNature(oldTile, TerrainType::GRASS);
    } else if (toggle_button_id == "ToggleWaterDown") {
        BuildNature(oldTile, TerrainType::WATERDOWN);
    } else if (toggle_button_id == "ToggleCanal") {
        BuildNature(oldTile, TerrainType::CANAL);
    } else if (toggle_button_id == "ToggleWater") {
        BuildNature(oldTile, TerrainType::WATER);
    } else if (toggle_button_id == "ToggleTree") {
        BuildNature(oldTile, TerrainType::TREE);
    } else if (toggle_button_id == "ToggleDelete") {
        Demolish(oldTile);
    }
}

void Gameplay::BuildResidential(Tile *oldTile)
{
    if (oldTile->GetTileType() != TileType::Terrain) {
        GG_WARN("Cannot build over buildings and structures");
        UpdateInfo("Demolish that building first...", sf::Color::Red);
        return;
    }

    TerrainType terrain_type =
        dynamic_cast<TerrainTile *>(oldTile)->GetTerrainType();
    if (terrain_type == TerrainType::WATER ||
        terrain_type == TerrainType::CANAL ||
        terrain_type == TerrainType::WATERDOWN) {
        GG_WARN("Cannot build over the watery surface.");
        UpdateInfo(
            "Cannot build that over the watery tiles!", sf::Color::Yellow);
        return;
    }

    BuildingTile *new_residential =
        mMap->CreateResidentialTile(oldTile->GetCoordinates());

    if (mCityBudget >= new_residential->GetCost()) {
        DecreaseBudget(new_residential->GetCost());
        IncreasePopulation(new_residential->GetPopulation());
        mMap->ChangeTile(oldTile, new_residential);
    } else {
        delete new_residential;
        GG_INFO("Not enough money for building residential building! :/");
        UpdateInfo(
            "Insufficient founds to build a residential building...",
            sf::Color::Red);
    }
}

void Gameplay::BuildIndustrial(Tile *oldTile)
{
    if (oldTile->GetTileType() != TileType::Terrain) {
        GG_WARN("Cannot build over buildings and structures.");
        UpdateInfo("Demolish that building first...", sf::Color::Red);
        return;
    }

    TerrainType terrain_type =
        dynamic_cast<TerrainTile *>(oldTile)->GetTerrainType();
    if (terrain_type == TerrainType::WATER ||
        terrain_type == TerrainType::CANAL ||
        terrain_type == TerrainType::WATERDOWN) {
        GG_WARN("Cannot build over the watery surface.");
        UpdateInfo(
            "Cannot build that over the watery tiles!", sf::Color::Yellow);
        return;
    }

    if (mCityPopulation / (mNumberOfIndustrial + 1) < 20) {
        GG_WARN("Not enough workers for a factory. Build more houses.");
        UpdateInfo(
            "Not enough workers! Build more houses...", sf::Color::Yellow);
        return;
    }

    BuildingTile *new_industrial =
        mMap->CreateIndustrialTile(oldTile->GetCoordinates());

    if (mCityBudget >= new_industrial->GetCost()) {
        DecreaseBudget(new_industrial->GetCost());
        IncreasePopulation(new_industrial->GetPopulation());
        IncreasePotentialProfit(new_industrial->GetProfit());
        mNumberOfIndustrial++;
        mMap->ChangeTile(oldTile, new_industrial);
    } else {
        delete new_industrial;
        GG_INFO("Insufficient found to build the industrial building... :/");
        UpdateInfo(
            "Insufficient founds to build an industrial building...",
            sf::Color::Red);
    }
}

void Gameplay::BuildRoad(Tile *oldTile, RoadType roadType)
{

    if (oldTile->GetTileType() != TileType::Terrain) {
        GG_WARN("Cannot build over buildings and structures.");
        UpdateInfo("Demolish that building first...", sf::Color::Red);
        return;
    }

    TerrainType terrain_type =
        dynamic_cast<TerrainTile *>(oldTile)->GetTerrainType();
    if (terrain_type == TerrainType::WATER ||
        terrain_type == TerrainType::CANAL ||
        terrain_type == TerrainType::WATERDOWN) {
        GG_WARN("Cannot build over the watery surface.");
        UpdateInfo(
            "Cannot build that over the watery tiles!", sf::Color::Yellow);
        return;
    }

    RoadTile *new_road =
        mMap->CreateRoadTile(oldTile->GetCoordinates(), roadType);

    if (mCityBudget >= new_road->GetCost()) {
        DecreaseBudget(new_road->GetCost());
        mMap->ChangeTile(oldTile, new_road);
    } else {
        delete new_road;
        GG_INFO("Insufficient founds to build the road... :/");
        UpdateInfo("Insufficient founds to build the road...", sf::Color::Red);
    }
}

void Gameplay::BuildBridge(Tile *oldTile, RoadType bridgeType)
{
    if (oldTile->GetTileType() != TileType::Terrain) {
        GG_WARN("Bridge must be build over the water.");
        UpdateInfo("Water tile needed!", sf::Color::Yellow);
        return;
    }

    if (dynamic_cast<TerrainTile *>(oldTile)->GetTerrainType() !=
        TerrainType::WATER) {
        GG_WARN("Bridge must be build over the water.");
        UpdateInfo("Water tile needed!", sf::Color::Yellow);
        return;
    }

    if (bridgeType != RoadType::BRIDGE && bridgeType != RoadType::BRIDGE2) {
        GG_WARN("Wrong type for bridge");
        UpdateInfo("Wrong bridge type!", sf::Color::Red);
        return;
    }

    RoadTile *new_bridge =
        mMap->CreateRoadTile(oldTile->GetCoordinates(), bridgeType);

    if (mCityBudget >= new_bridge->GetCost()) {
        DecreaseBudget(new_bridge->GetCost());
        mMap->ChangeTile(oldTile, new_bridge);
    } else {
        delete new_bridge;
        GG_INFO("Not enough money for building bridge! :/");
        UpdateInfo("Insufficient founds to build a bridge...", sf::Color::Red);
    }
}

void Gameplay::UpdatePopulationLabel()
{
    Label *city_population_label =
        SceneManager::GetScene(m_sceneType)->GetLabelByID("CityPopulation");
    city_population_label->SetText(
        "Population: " + std::to_string(mCityPopulation));
}

void Gameplay::UpdateBudgetLabel()
{
    Label *city_budget_label =
        SceneManager::GetScene(m_sceneType)->GetLabelByID("CityBudget");
    city_budget_label->SetText("Budget: " + std::to_string(mCityBudget) + "$");
}

void Gameplay::UpdatePotentialProfitLabel()
{
    Label *city_budget_label =
        SceneManager::GetScene(m_sceneType)->GetLabelByID("CityProfit");
    city_budget_label->SetText(
        "Profit: " + std::to_string(mPotentialProfit) + "$");
}

void Gameplay::UpdateInfo(const std::string &infoMsg, sf::Color color)
{
    Label *info_label =
        SceneManager::GetScene(m_sceneType)->GetLabelByID("InfoLabel");
    info_label->SetText(infoMsg);
    info_label->SetColor(color);
}

void Gameplay::BuildNature(Tile *oldTile, TerrainType terrainType)
{

    if (oldTile->GetTileType() != TileType::Terrain) {
        GG_WARN("Cannot build over buildings and structures");
        UpdateInfo("Demolish that building first...", sf::Color::Red);
        return;
    }

    switch (terrainType) {
    case TerrainType::GRASS: {
        TerrainTile *new_grass_tile =
            mMap->CreateGrassTile(oldTile->GetCoordinates());
        mMap->ChangeTile(oldTile, new_grass_tile);
        break;
    }
    case TerrainType::WATER: {
        TerrainTile *new_water_tile =
            mMap->CreateWaterTile(oldTile->GetCoordinates());
        mMap->ChangeTile(oldTile, new_water_tile);
        break;
    }
    case TerrainType::WATERDOWN: {
        TerrainTile *new_water_down_tile =
            mMap->CreateWaterDownTile(oldTile->GetCoordinates());
        mMap->ChangeTile(oldTile, new_water_down_tile);
        break;
    }
    case TerrainType::CANAL: {
        TerrainTile *new_canal_tile =
            mMap->CreateCanalTile(oldTile->GetCoordinates());
        mMap->ChangeTile(oldTile, new_canal_tile);
        break;
    }
    case TerrainType::TREE: {
        TerrainTile *new_tree_tile =
            mMap->CreateTreeTile(oldTile->GetCoordinates());
        mMap->ChangeTile(oldTile, new_tree_tile);
        break;
    }
    }
}

void Gameplay::Demolish(Tile *tile)
{
    unsigned refund_factor = 2;

    if (tile->GetTileType() == TileType::Terrain) {
        return;
    }

    if (tile->GetTileType() == TileType::Building) {
        GG_INFO("Trying to demolish and object...");
        auto *building = dynamic_cast<BuildingTile *>(tile);
        DecreasePopulation(building->GetPopulation());
        IncreaseBudget(building->GetCost() / refund_factor);
        DecreasePotentialProfit(building->GetProfit());
        if (building->IsIndustrial()) {
            mNumberOfIndustrial--;
        }
        TerrainTile *new_grass_tile =
            mMap->CreateGrassTile(tile->GetCoordinates());
        mMap->ChangeTile(tile, new_grass_tile);
        GG_INFO("Object successfully demolished!");
        return;
    }

    if (tile->GetTileType() == TileType::Road) {

        auto *road = dynamic_cast<RoadTile *>(tile);

        if (road->GetType() == RoadType::BRIDGE ||
            road->GetType() == RoadType::BRIDGE2) {
            GG_INFO("Trying to demolish bridge...");
            IncreaseBudget(road->GetCost() / refund_factor);
            TerrainTile *new_water_tile =
                mMap->CreateWaterTile(tile->GetCoordinates());
            mMap->ChangeTile(tile, new_water_tile);
            GG_INFO("Bridge successfully demolished!");
        } else {
            GG_INFO("Trying to demolish road...");
            IncreaseBudget(road->GetCost() / refund_factor);
            TerrainTile *new_grass_tile =
                mMap->CreateGrassTile(tile->GetCoordinates());
            mMap->ChangeTile(tile, new_grass_tile);
            GG_INFO("Road successfully demolished!");
            return;
        }
    }
}

void Gameplay::MapNavigate()
{
    // TODO: Implement map navigation
}

void Gameplay::SetOrientation(Tile *oldTile)
{
    std::string texture_name = oldTile->GetTextureName();
    size_t texture_prefix_pos = texture_name.find("_");
    std::string texture_prefix = texture_name.substr(0, texture_prefix_pos);
    if (texture_name.find("front") != std::string::npos) {
        oldTile->SetTexture(texture_prefix.append("_left"));
    } else if (texture_name.find("left") != std::string::npos) {
        oldTile->SetTexture(texture_prefix.append("_back"));
    } else if (texture_name.find("back") != std::string::npos) {
        oldTile->SetTexture(texture_prefix.append("_right"));
    } else if (texture_name.find("right") != std::string::npos) {
        oldTile->SetTexture(texture_prefix.append("_front"));
    }
}

void Gameplay::OnMoveCameraUp()
{
    if (mMap->mBegin.y < 350) {
        mMap->MoveMap(0, 15.0f);
    }
}

void Gameplay::OnMoveCameraDown()
{
    if (mMap->mBegin.y > -700) {
        mMap->MoveMap(0, -15.0f);
    }
}

void Gameplay::OnMoveCameraLeft()
{
    GG_TRACE(mMap->mBegin.x);
    if (mMap->mBegin.x < 1700) {
        mMap->MoveMap(15.0f, 0);
    }
}

void Gameplay::OnMoveCameraRight()
{
    GG_TRACE(mMap->mBegin.x);
    if (mMap->mBegin.x > -150) {
        mMap->MoveMap(-15.0f, 0);
    }
}

void Gameplay::OnMoveDiagUpRight()
{
    // x^2 + y^2 = diag^2 => diag = sqrt(x^2 + y^2)
    if (mMap->mBegin.y < 350 && mMap->mBegin.x < 260) {
        mMap->MoveMap(-6.363f, 6.363f);
    }
}

void Gameplay::OnMoveDiagDownLeft()
{
    if (mMap->mBegin.y > -700 && mMap->mBegin.x > 960) {
        mMap->MoveMap(6.363f, -6.363f);
    }
}

void Gameplay::OnMoveDiagDownRight()
{
    if (mMap->mBegin.y > -450 && mMap->mBegin.x < 260) {
        mMap->MoveMap(-6.363f, -6.363f);
    }
}

void Gameplay::OnMoveDiagUpLeft()
{
    if (mMap->mBegin.y < 350 && mMap->mBegin.x > 960) {
        mMap->MoveMap(6.363f, 6.363f);
    }
}

} // namespace gg
