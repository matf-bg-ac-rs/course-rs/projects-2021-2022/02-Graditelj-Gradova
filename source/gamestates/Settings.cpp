#include <core/Application.hpp>
#include <events/SceneEvent.hpp>
#include <gamestates/Settings.hpp>

namespace gg
{

Settings::Settings() : GameState(SceneType::Settings) {}

Settings::~Settings() = default;

void Settings::Enter()
{
    GG_DEBUG_TRACE("Entered GameState: Settings");
    GameState::Enter();
    EventDispatcher::Subscribe(this, EventType::ToggleButtonClicked);
}

void Settings::Exit()
{
    GG_DEBUG_TRACE("Exited GameState: Settings");
    GameState::Exit();
}

void Settings::OnEvent(IEvent *e)
{
    if (e->GetEventType() == EventType::ButtonClicked) {
        auto *button_event = static_cast<SceneButtonEvent *>(e);
        std::string button_id = button_event->GetButtonID();
        if (button_id == "BackFromSettingsButton") {
            OnBackPressedEvent();
        }
    } else if (e->GetEventType() == EventType::ToggleButtonClicked) {
        auto *toggle_event =
            static_cast<SceneToggleButtonEvent *>(e);
        std::string button_id = toggle_event->GetButtonID();
        if (button_id == "FullscreenButton") {
            OnFullscreenEvent(toggle_event->GetToggleState());
        } else if (button_id == "MusicButton") {
            OnMusicSetEvent(toggle_event->GetToggleState());
        }
    }
}

void Settings::Update() { SceneManager::GetScene(m_sceneType)->Update(); }

void Settings::OnFullscreenEvent(const bool toggleState)
{
    Application::GetInstance()->ToggleFullscreen(toggleState);
}

void Settings::OnMusicSetEvent(const bool toggleState)
{
    Application::GetInstance()->ToggleMusic(toggleState);
}

void Settings::OnBackPressedEvent() { GameEngine::PopState(); }

} // namespace gg
