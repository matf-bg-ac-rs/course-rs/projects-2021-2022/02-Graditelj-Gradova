#include "events/KeyboardEvent.hpp"
#include "gamestates/Gameplay.hpp"
#include <events/SceneEvent.hpp>
#include <gamestates/ControlsMenu.hpp>

namespace gg
{

ControlsMenu::ControlsMenu() : GameState(SceneType::ControlsMenu) {}

ControlsMenu::~ControlsMenu() = default;

void ControlsMenu::Enter()
{
    GG_DEBUG_TRACE("Entered GameState: ControlsMenu");
    GameState::Enter();
    EventDispatcher::Subscribe(this, EventType::ButtonClicked);
}

void ControlsMenu::Exit()
{
    GG_DEBUG_TRACE("Exited GameState: ControlsMenu");
    GameState::Exit();
}

void ControlsMenu::OnEvent(IEvent *e)
{
    if (e->GetEventType() == EventType::ButtonClicked) {
        auto *button_event = static_cast<SceneButtonEvent *>(e);
        std::string button_id = button_event->GetButtonID();
        if (button_id == "ControlsResumeButton") {
            OnResumeEvent();
        }
    }
}

void ControlsMenu::Update() { SceneManager::GetScene(m_sceneType)->Update(); }

void ControlsMenu::OnResumeEvent() { GameEngine::PopState(); }

void ControlsMenu::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    dynamic_cast<Gameplay *>(GameEngine::PreviousState())->draw(target, states);
    GameState::draw(target, states);
}

} // namespace gg
