#include <events/SceneEvent.hpp>
#include <events/WindowEvent.hpp>
#include <gamestates/CustomGameState.hpp>
#include <gamestates/LoadGame.hpp>
#include <gamestates/MainMenu.hpp>
#include <gamestates/NewGame.hpp>
#include <gamestates/Settings.hpp>

namespace gg
{

MainMenu::MainMenu() : GameState(SceneType::MainMenu) {}

MainMenu::~MainMenu() = default;

void MainMenu::Enter()
{
    GG_DEBUG_TRACE("Entered GameState: MainMenu");
    GameState::Enter();
}

void MainMenu::Exit()
{
    GG_DEBUG_TRACE("Exited GameState: MainMenu");
    GameState::Exit();
}

void MainMenu::OnEvent(IEvent *e)
{
    if (e->GetEventType() == EventType::ButtonClicked) {
        auto *button_event = static_cast<SceneButtonEvent *>(e);
        std::string button_id = button_event->GetButtonID();
        if (button_id == "NewGameButton") {
            OnNewGamePressedEvent();
        } else if (button_id == "LoadGameButton") {
            OnLoadGamePressedEvent();
        } else if (button_id == "SettingsButton") {
            OnSettingsPressedEvent();
        } else if (button_id == "QuitButton") {
            OnExitPressedEvent();
        }
    }
}

void MainMenu::Update() { SceneManager::GetScene(m_sceneType)->Update(); }

void MainMenu::OnNewGamePressedEvent() { GameEngine::PushState(new NewGame()); }

void MainMenu::OnLoadGamePressedEvent()
{
    GameEngine::PushState(new LoadGame());
}

void MainMenu::OnSettingsPressedEvent()
{
    GameEngine::PushState(new Settings());
}

void MainMenu::OnExitPressedEvent()
{
    EventDispatcher::NotifySubscribers(new WindowCloseEvent());
}

} // namespace gg
