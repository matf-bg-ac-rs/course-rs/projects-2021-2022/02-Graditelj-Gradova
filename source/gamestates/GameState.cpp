#include <gamestates/GameState.hpp>

namespace gg
{

GameState::GameState(SceneType sceneType) : m_sceneType(sceneType) {}

void GameState::Enter()
{
    EventDispatcher::Subscribe(this, EventType::ButtonClicked);
    SceneManager::GetScene(m_sceneType)->SetActive(true);
}

void GameState::Exit()
{
    SceneManager::GetScene(m_sceneType)->SetActive(false);
    EventDispatcher::RemoveSubscriber(this);
}

void GameState::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    target.draw(*SceneManager::GetScene(m_sceneType), states);
}

auto GameState::GetSceneType() -> SceneType { return m_sceneType; }

} // namespace gg
