#include <events/SceneEvent.hpp>
#include <gamestates/CustomGameState.hpp>
#include <gamestates/Gameplay.hpp>
#include <gamestates/NewGame.hpp>

namespace gg
{

NewGame::NewGame() : GameState(SceneType::NewGame) {}

NewGame::~NewGame() = default;

void NewGame::Enter()
{
    GG_DEBUG_TRACE("Entered GameState: NewGame");
    GameState::Enter();
}

void NewGame::Exit()
{
    GG_DEBUG_TRACE("Exited GameState: NewGame");
    GameState::Exit();
}

void NewGame::OnEvent(IEvent *e)
{
    if (e->GetEventType() == EventType::ButtonClicked) {
        auto *button_event = static_cast<SceneButtonEvent *>(e);
        std::string button_id = button_event->GetButtonID();

        if (button_id == "BackFromNewGameButton") {
            OnBackPressedEvent();
        } else if (button_id == "City1Button") {
            OnSelectedGameEvent(GG_RESOURCES_SAVE_POZAREVAC);
        } else if (button_id == "City2Button") {
            OnSelectedGameEvent(GG_RESOURCES_SAVE_TRSTENIK);
        } else if (button_id == "City3Button") {
            OnSelectedGameEvent(GG_RESOURCES_SAVE_UB);
        } else if (button_id == "City4Button") {
            OnSelectedGameEvent(GG_RESOURCES_SAVE_BLACE);
        } else if (button_id == "City5Button") {
            OnSelectedGameEvent(GG_RESOURCES_SAVE_BEOGRAD);
        }
    }
}

void NewGame::Update() { SceneManager::GetScene(m_sceneType)->Update(); }

void NewGame::OnSelectedGameEvent(const std::string &saveFilePath)
{
    GameEngine::ChangeState(new Gameplay(GG_RESOURCES_PATH + saveFilePath));
    dynamic_cast<Gameplay *>(GameEngine::CurrentState())->NewGame();
}

void NewGame::OnBackPressedEvent() { GameEngine::PopState(); }

} // namespace gg
