#include <events/SceneEvent.hpp>
#include <gamestates/CustomGameState.hpp>
#include <gamestates/Gameplay.hpp>
#include <gamestates/LoadGame.hpp>

namespace gg
{

LoadGame::LoadGame() : GameState(SceneType::LoadGame) {}

LoadGame::~LoadGame() = default;

void LoadGame::Enter()
{
    GG_DEBUG_TRACE("Entered GameState: LoadGame");
    GameState::Enter();
}

void LoadGame::Exit()
{
    GG_DEBUG_TRACE("Exited GameState: LoadGame");
    GameState::Exit();
}

void LoadGame::OnEvent(IEvent *e)
{
    if (e->GetEventType() == EventType::ButtonClicked) {
        auto *button_event = static_cast<SceneButtonEvent *>(e);
        std::string button_id = button_event->GetButtonID();
        //            TODO: implement different handlers based on button id
        if (button_id == "BackFromLoadGameButton") {
            OnBackPressedEvent();
        } else if (button_id == "City1Button") {
            OnSelectedGameEvent(GG_RESOURCES_SAVE_POZAREVAC);
        } else if (button_id == "City2Button") {
            OnSelectedGameEvent(GG_RESOURCES_SAVE_TRSTENIK);
        } else if (button_id == "City3Button") {
            OnSelectedGameEvent(GG_RESOURCES_SAVE_UB);
        } else if (button_id == "City4Button") {
            OnSelectedGameEvent(GG_RESOURCES_SAVE_BLACE);
        } else if (button_id == "City5Button") {
            OnSelectedGameEvent(GG_RESOURCES_SAVE_BEOGRAD);
        }
    }
}

void LoadGame::Update() { SceneManager::GetScene(m_sceneType)->Update(); }

void LoadGame::OnSelectedGameEvent(const std::string &saveFilePath)
{
    GameEngine::ChangeState(new Gameplay(GG_RESOURCES_PATH + saveFilePath));
    dynamic_cast<Gameplay *>(GameEngine::CurrentState())->LoadGame();
}

void LoadGame::OnBackPressedEvent() { GameEngine::PopState(); }

} // namespace gg
