#include "events/KeyboardEvent.hpp"
#include "gamestates/Gameplay.hpp"
#include "gamestates/MainMenu.hpp"
#include <events/SceneEvent.hpp>
#include <events/WindowEvent.hpp>
#include <gamestates/PauseMenu.hpp>

namespace gg
{

PauseMenu::PauseMenu() : GameState(SceneType::PauseMenu) {}

PauseMenu::~PauseMenu() = default;

void PauseMenu::Enter()
{
    GG_DEBUG_TRACE("Entered GameState: PauseMenu");
    GameState::Enter();
    EventDispatcher::Subscribe(this, EventType::KeyboardPressed);
    EventDispatcher::Subscribe(this, EventType::ButtonClicked);
}

void PauseMenu::Exit()
{
    GG_DEBUG_TRACE("Exited GameState: PauseMenu");
    GameState::Exit();
}

void PauseMenu::OnEvent(IEvent *e)
{
    if (e->GetEventType() == EventType::ButtonClicked) {
        auto *button_event = static_cast<SceneButtonEvent *>(e);
        std::string button_id = button_event->GetButtonID();
        if (button_id == "QuitToMainMenuButton") {
            OnQuitEvent();
        } else if (button_id == "ResumeButton") {
            OnResumeEvent();
        } else if (button_id == "SaveButton") {
            OnSaveEvent();
        }
    }

    if (e->GetEventType() == EventType::KeyboardPressed) {
        auto *keyboard_event = static_cast<KeyboardEvent *>(e);
        if (keyboard_event->GetKeycode() == sf::Keyboard::Escape) {
            GG_DEBUG_CRITICAL("ESCAPE");
            OnResumeEvent();
        }
    }
}

void PauseMenu::Update() { SceneManager::GetScene(m_sceneType)->Update(); }

void PauseMenu::OnResumeEvent() { GameEngine::PopState(); }

void PauseMenu::OnSaveEvent()
{
    dynamic_cast<Gameplay *>(GameEngine::PreviousState())->SaveGame();
}

void PauseMenu::OnQuitEvent()
{
    // Getting back to main menu
    GameEngine::PopState();
    GameEngine::PopState();
}

void PauseMenu::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    dynamic_cast<Gameplay *>(GameEngine::PreviousState())->draw(target, states);
    GameState::draw(target, states);
}

} // namespace gg
