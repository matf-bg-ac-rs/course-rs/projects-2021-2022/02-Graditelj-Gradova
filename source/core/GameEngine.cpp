#include "core/GameEngine.hpp"
#include <chrono>
#include <gamestates/MainMenu.hpp>
#include <thread>

namespace gg
{

std::stack<GameState *> GameEngine::mStates;

void GameEngine::Init()
{
    GG_DEBUG_INFO("Initialzing game...");
    PushState(new MainMenu());
}

void GameEngine::Destroy()
{
    GG_DEBUG_INFO("Destroying game...");
    while (!mStates.empty()) {
        PopState();
    }
}

void GameEngine::ChangeState(GameState *state)
{
    GG_ASSERT(
        !mStates.empty(),
        "Stack is empty, main menu state should be there at least");
    PopState();
    PushState(state);
}

void GameEngine::PushState(GameState *state)
{
    if (!mStates.empty()) {
        CurrentState()->Exit();
    }

    state->Enter();
    mStates.push(state);
}

void GameEngine::PopState()
{
    GG_ASSERT(!mStates.empty(), "Stack empty. Cannot pop!");
    auto state = CurrentState();
    mStates.pop();
    state->Exit();
    delete state;

    if (!mStates.empty()) {
        CurrentState()->Enter();
    }
}

auto GameEngine::CurrentState() -> GameState *
{
    GG_ASSERT(!mStates.empty(), "Stack empty. Cannot return current state!");
    return mStates.top();
}

void GameEngine::Update() { CurrentState()->Update(); }

void GameEngine::Render(sf::RenderTarget &target, sf::RenderStates states)
{
    target.draw(*CurrentState(), states);
}

auto GameEngine::PreviousState() -> GameState *
{
    GG_ASSERT(mStates.size() >= 2, "Stack must have at least 2 elements");
    auto *current_state = CurrentState();
    mStates.pop();
    auto *previous_state = mStates.top();
    mStates.push(current_state);
    return previous_state;
}

} // namespace gg
