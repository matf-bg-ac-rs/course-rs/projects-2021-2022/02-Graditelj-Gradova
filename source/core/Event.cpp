#include <core/Event.hpp>

namespace gg
{

auto &dispatcher = EventDispatcher::GetInstance();

void EventDispatcher::Subscribe(IEventSubscriber *s, const EventType e)
{
    // check to see if this subscriber already monitors this event
    auto subscriber = dispatcher.mSubscribers.find(s);
    // subscriber already exists - just try to add another event for that
    // subscriber
    if (subscriber != dispatcher.mSubscribers.end()) {
        std::vector<EventType> &events = subscriber->second;
        // that subscriber already monitors that event - skip adding it again
        if (std::find(events.begin(), events.end(), e) != events.end()) {
            return;
        } else {
            subscriber->second.push_back(e);
        }
    } else {
        dispatcher.mSubscribers.insert(
            std::make_pair(s, std::vector<EventType>(1, e)));
    }
    s->IncrementNumberOfSubscribedEvents();
}

void EventDispatcher::Unsubscribe(IEventSubscriber *s, const EventType e)
{
    // check if subscriber and this event exist
    auto subscriber = dispatcher.mSubscribers.find(s);
    if (subscriber != dispatcher.mSubscribers.end()) {
        std::vector<EventType> &events = subscriber->second;
        auto event_position = std::find(events.begin(), events.end(), e);
        if (event_position != events.end()) {
            events.erase(event_position);
            s->DecrementNumberOfSubscribedEvents();

            if (s->GetNumberOfSubscribedEvents() == 0) {
                RemoveSubscriber(s);
            }
        }
    }
}

void EventDispatcher::RemoveSubscriber(IEventSubscriber *s)
{
    auto it = dispatcher.mSubscribers.find(s);
    if (it == dispatcher.mSubscribers.end()) {
        return;
    }
    s->ResetNumberOfSubscribedEvents();
    dispatcher.mSubscribers.erase(it);
}

void EventDispatcher::NotifySubscribers(IEvent *e)
{
    std::vector<IEventSubscriber *> subscribers;
    for (auto &it : dispatcher.mSubscribers) {
        auto event =
            std::find(it.second.cbegin(), it.second.cend(), e->GetEventType());
        if (event != it.second.cend()) {
            subscribers.push_back(it.first);
        }
    }

    for (auto it : subscribers) {
        it->OnEvent(e);
    }

    // removes event so there is no memory leakage (subscriber stays subscribed
    // to that event type)
    delete e;
}

auto EventDispatcher::NumberOfSubscribers() -> int
{
    return dispatcher.mSubscribers.size();
}

bool CheckSubscriberExistence(IEventSubscriber* e)
{
    if (EventDispatcher::GetInstance().mSubscribers.find(e) != EventDispatcher::GetInstance().mSubscribers.end()) {
        return  true;
    }
    return false;
}

auto IEventSubscriber::GetNumberOfSubscribedEvents() const -> size_t
{
    return mNumOfSubscribedEvents;
}

void IEventSubscriber::IncrementNumberOfSubscribedEvents()
{
    mNumOfSubscribedEvents++;
}

void IEventSubscriber::DecrementNumberOfSubscribedEvents()
{
    mNumOfSubscribedEvents--;
}

void IEventSubscriber::ResetNumberOfSubscribedEvents()
{
    mNumOfSubscribedEvents = 0;
}

auto IEventSubscriber::operator==(const IEventSubscriber &other) -> bool
{
    return this == &other;
}

} // namespace gg
