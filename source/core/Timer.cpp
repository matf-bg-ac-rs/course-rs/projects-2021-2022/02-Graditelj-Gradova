#include "core/Event.hpp"
#include "events/TimeEvent.hpp"
#include <core/Timer.hpp>

namespace gg
{

Timer::Timer() { mCurrentTimeInSeconds = 0.0f; }

Timer::~Timer() = default;

void Timer::Update()
{
    auto elapsed_time = mClock.getElapsedTime();
    mCurrentTimeInSeconds = unsigned(elapsed_time.asSeconds());

    bool condition_min =
        mCurrentTimeInSeconds != 0 && mCurrentTimeInSeconds % 60 == 0;
    bool condition_ten =
        mCurrentTimeInSeconds != 0 && mCurrentTimeInSeconds % 10 == 0;

    if (condition_min) {
        EventDispatcher::NotifySubscribers(
            new TimePassed(TimeCategory::MINUTE));
        mClock.restart();
    }

    if (condition_ten) {
        EventDispatcher::NotifySubscribers(
            new TimePassed(TimeCategory::TEN_SECONDS));
    }
}
} // namespace gg
