#include <core/Util.hpp>

namespace gg
{

auto Util::TriangleArea(
    const sf::Vector2f &A, const sf::Vector2f &B, const sf::Vector2f &C) -> double
{
    return 0.5 * std::fabs(
                     A.x * B.y - A.x * C.y + B.x * C.y - B.x * A.y + C.x * A.y -
                     C.x * B.y);
}

auto Util::RectangleArea(
    const sf::Vector2f &A,
    const sf::Vector2f &B,
    const sf::Vector2f &C,
    const sf::Vector2f &D) -> double
{
    return TriangleArea(A, B, C) + TriangleArea(A, D, C);
}

} // namespace gg
