#include <core/Map.hpp>
#include <tile/BuildingTile.hpp>
#include <tile/RoadTile.hpp>
#include <tile/TerrainTile.hpp>

#include <SFML/Graphics.hpp>

#include "core/GameEngine.hpp"
#include "gamestates/Gameplay.hpp"
#include <cmath>

namespace gg
{

Map::~Map()
{
    for (auto tile : mTiles) {
            delete tile;
    }
}

void Map::LoadFromXML(const XMLDocument &map)
{
    mWidth = map.GetAttributes().Get<unsigned int>("width");
    mHeight = map.GetAttributes().Get<unsigned int>("height");
    mTileSize = map.GetAttributes().Get<sf::Vector2f>("tile_size");
    mBegin = map.GetAttributes().Get<sf::Vector2f>("begin");

    mResidentialAttributes.Add("building_type", "residential_front");
    mResidentialAttributes.Add("cost", "50");
    mResidentialAttributes.Add("population", "6");
    mResidentialAttributes.Add("profit", "0");

    mIndustrialAttributes.Add("building_type", "industrial_front");
    mIndustrialAttributes.Add("cost", "150");
    mIndustrialAttributes.Add("population", "0");
    mIndustrialAttributes.Add("profit", "80");

    mRoadAttributes.Add("cost", "20");

    // TODO: define attributes for canal, tree and waterDown
    mTreeAttributes.Add("terrain_type", "tree_front");
    mCanalAttributes.Add("terrain_type", "canal_front");
    mWaterDownAttributes.Add("terrain_type", "waterDown_front");
    mGrassAttributes.Add("terrain_type", "grass");
    mWaterAttributes.Add("terrain_type", "water");

    std::vector<XMLDocument> tiles = map.GetChildren();

    float half_size = mTileSize.x / 2.0f;
    // halfSize.y = m_tileSize.y / 2.0f;
    long double sq3_ratio = 1.0 / sqrt(3.0) - 0.077;

    for (unsigned i = 0; i < mWidth; ++i) {
        float x_offset = mBegin.x + i * half_size;
        float y_offset = mBegin.y + i * half_size * sq3_ratio;

        for (unsigned j = 0; j < mHeight; ++j) {
            Attributes attributes = (tiles[i * mWidth + j]).GetAttributes();
            attributes.Add(
                "coordinates",
                std::to_string(x_offset) + " " + std::to_string(y_offset));

            std::string tile_type = attributes.Get("type");
            if (tile_type == "terrain") {
                auto *terrain_tile = new TerrainTile(attributes);
                mTiles.push_back(terrain_tile);
            } else if (tile_type == "building") {
                auto *building_tile = new BuildingTile(attributes);
                mTiles.push_back(building_tile);
            } else if (tile_type == "road") {
                auto *road_tile = new RoadTile(attributes);
                mTiles.push_back(road_tile);
            } else {
                GG_ASSERT(false, "tileType not valid");
            }

            x_offset -= half_size;
            y_offset += half_size * sq3_ratio;
        }
    }
}

void Map::SaveToFilename(const std::string &filename)
{
    Attributes map_attributes;
    map_attributes.Add("width", std::to_string(mWidth));
    map_attributes.Add("height", std::to_string(mHeight));
    map_attributes.Add(
        "tile_size",
        std::to_string(mTileSize.x) + " " + std::to_string(mTileSize.y));
    map_attributes.Add(
        "begin", std::to_string(mBegin.x) + " " + std::to_string(mBegin.y));

    std::vector<XMLDocument> tiles;

    for (auto & m_tile : mTiles) {
        Attributes tile_attributes = m_tile->GetAttributes();
        XMLDocument tile(
            "Tile", tile_attributes, "", std::vector<XMLDocument>());
        tiles.push_back(tile);
    }

    XMLDocument map("Map", map_attributes, "", tiles);
    XMLDocument::SaveToFile(map, filename);
}

void Map::draw(sf::RenderWindow &window)
{
    for (int i = 0; i < mHeight; ++i) {
        for (int j = 0; j < mWidth; ++j) {
            window.draw(*(mTiles[i * mWidth + j]));
        }
    }
}

void Map::Update()
{
    for (auto &tile : mTiles) {
        tile->Update();
    }
}

void Map::SetBegin(sf::Vector2f begin) { mBegin = begin; }

void Map::SetWidth(unsigned int width) { mWidth = width; }

void Map::SetHeight(unsigned int height) { mHeight = height; }

void Map::SetTileSize(sf::Vector2f tileSize) { mTileSize = tileSize; }

void Map::ChangeTile(Tile *oldTile, Tile *newTile)
{
    for (int i = 0; i < mHeight; i++) {
        for (int j = 0; j < mWidth; j++) {
            if (mTiles[i * mWidth + j] == oldTile) {
                delete oldTile;
                mTiles[i * mWidth + j] = newTile;
            }
        }
    }
}

auto Map::CreateGrassTile(sf::Vector2f coordinates) -> TerrainTile *
{
    Attributes grass_attributes = this->mGrassAttributes;
    grass_attributes.Add(
        "coordinates",
        std::to_string(coordinates.x) + " " + std::to_string(coordinates.y));
    grass_attributes.Add("rotation", "0");
    return new TerrainTile(grass_attributes);
}

auto Map::CreateWaterTile(sf::Vector2f coordinates) -> TerrainTile *
{
    Attributes water_attributes = this->mWaterAttributes;
    water_attributes.Add(
        "coordinates",
        std::to_string(coordinates.x) + " " + std::to_string(coordinates.y));
    water_attributes.Add("rotation", "0");
    return new TerrainTile(water_attributes);
}

auto Map::CreateResidentialTile(sf::Vector2f coordinates) -> BuildingTile *
{
    Attributes residential_attributes = this->mResidentialAttributes;
    residential_attributes.Add(
        "coordinates",
        std::to_string(coordinates.x) + " " + std::to_string(coordinates.y));
    residential_attributes.Add("rotation", "0");
    return new BuildingTile(residential_attributes);
}

auto Map::CreateIndustrialTile(sf::Vector2f coordinates) -> BuildingTile *
{
    Attributes industrial_attributes = this->mIndustrialAttributes;
    industrial_attributes.Add(
        "coordinates",
        std::to_string(coordinates.x) + " " + std::to_string(coordinates.y));
    industrial_attributes.Add("rotation", "0");
    return new BuildingTile(industrial_attributes);
}

auto Map::CreateRoadTile(sf::Vector2f coordinates, RoadType roadType) -> RoadTile *
{
    Attributes road_attributes = this->mRoadAttributes;
    road_attributes.Add(
        "coordinates",
        std::to_string(coordinates.x) + " " + std::to_string(coordinates.y));
    road_attributes.Add("road_type", RoadTile::RoadTypeToString(roadType));
    road_attributes.Add("rotation", "0");
    return new RoadTile(road_attributes);
}

void Map::ChangeLocation()
{
    float half_size = mTileSize.x / 2.0f;
    // halfSize.y = m_tileSize.y / 2.0f;
    long double sq3_ratio = 1.0 / sqrt(3.0) - 0.077;

    for (unsigned i = 0; i < mWidth; ++i) {
        float x_offset = mBegin.x + i * half_size;
        float y_offset = mBegin.y + i * half_size * sq3_ratio;

        for (unsigned j = 0; j < mHeight; ++j) {
            Attributes attributes = (mTiles[i * mWidth + j])->GetAttributes();
            attributes.Add(
                "coordinates",
                std::to_string(x_offset) + " " + std::to_string(y_offset));

            mTiles[i * mWidth + j]->SetCoordinates(
                sf::Vector2f(x_offset, y_offset));

            x_offset -= half_size;
            y_offset += half_size * sq3_ratio;
        }
    }
}

void Map::MoveMap(float x, float y)
{
    mBegin.x += x;
    mBegin.y += y;
    this->ChangeLocation();
}

void Map::SetActive(bool isActive)
{
    if (isActive) {
        for (int i = 0; i < mHeight; i++) {
            for (int j = 0; j < mWidth; j++) {
                EventDispatcher::Subscribe(
                    mTiles[i * mWidth + j], EventType::MouseMoved);
                EventDispatcher::Subscribe(
                    mTiles[i * mWidth + j], EventType::MousePressed);
            }
        }
    } else {
        for (int i = 0; i < mHeight; i++) {
            for (int j = 0; j < mWidth; j++) {
                EventDispatcher::RemoveSubscriber(mTiles[i * mWidth + j]);
            }
        }
    }
}

auto Map::CreateWaterDownTile(sf::Vector2f coordinates) -> TerrainTile *
{
    Attributes water_down_attributes = mWaterDownAttributes;
    water_down_attributes.Add(
        "coordinates",
        std::to_string(coordinates.x) + " " + std::to_string(coordinates.y));
    water_down_attributes.Add("rotation", "0");
    return new TerrainTile(water_down_attributes);
}

auto Map::CreateCanalTile(sf::Vector2f coordinates) -> TerrainTile *
{
    Attributes canal_attributes = mCanalAttributes;
    canal_attributes.Add(
        "coordinates",
        std::to_string(coordinates.x) + " " + std::to_string(coordinates.y));
    canal_attributes.Add("rotation", "0");
    return new TerrainTile(canal_attributes);
}

auto Map::CreateTreeTile(sf::Vector2f coordinates) -> TerrainTile *
{
    Attributes tree_attributes = mTreeAttributes;
    tree_attributes.Add(
        "coordinates",
        std::to_string(coordinates.x) + " " + std::to_string(coordinates.y));
    tree_attributes.Add("rotation", "0");
    return new TerrainTile(tree_attributes);
}

} // namespace gg

// namespace gg
