#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/sinks/stdout_color_sinks.h"

namespace gg
{

std::shared_ptr<spdlog::logger> Logger::s_coreLogger;
std::shared_ptr<spdlog::logger> Logger::s_debugLogger;

void Logger::Init()
{
    std::vector<spdlog::sink_ptr> core_sinks;
    std::vector<spdlog::sink_ptr> debug_sinks;

    core_sinks.push_back(
        std::make_shared<spdlog::sinks::stdout_color_sink_mt>());
    core_sinks.push_back(
        std::make_shared<spdlog::sinks::basic_file_sink_mt>("gg.log"));

    debug_sinks.push_back(
        std::make_shared<spdlog::sinks::stdout_color_sink_mt>());
    debug_sinks.push_back(
        std::make_shared<spdlog::sinks::basic_file_sink_mt>("gg_debug.log"));

    core_sinks[0]->set_pattern("[%T] [%n: %^%l%$] %v");
    core_sinks[1]->set_pattern("[%T] [%n: %l] %v");

    // TODO: find some better way for debug pattern
    debug_sinks[0]->set_pattern("[%T] [%^%n: %l%$] %v");
    debug_sinks[1]->set_pattern("[%T] [%n: %l] %v");

    s_coreLogger = std::make_shared<spdlog::logger>(
        "GG", core_sinks.begin(), core_sinks.end());
    s_debugLogger = std::make_shared<spdlog::logger>(
        "DEBUG_GG", debug_sinks.begin(), debug_sinks.end());

    spdlog::register_logger(s_coreLogger);
    spdlog::register_logger(s_debugLogger);

    s_coreLogger->set_level(spdlog::level::trace);
    s_coreLogger->flush_on(spdlog::level::trace);

    s_debugLogger->set_level(spdlog::level::trace);
    s_debugLogger->flush_on(spdlog::level::trace);
}

}; // namespace gg
