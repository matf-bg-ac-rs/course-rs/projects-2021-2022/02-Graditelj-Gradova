#include <core/Application.hpp>
#include <core/InputEngine.hpp>
#include <events/KeyboardEvent.hpp>
#include <events/MouseEvent.hpp>
#include <events/SceneEvent.hpp>
#include <events/WindowEvent.hpp>

namespace gg
{
auto InputEngine::IsSpriteClicked(
    sf::Sprite object, sf::Mouse::Button button, sf::RenderWindow &window) -> bool
{
    if (sf::Mouse::isButtonPressed(button)) {
        // obuhvata ceo sprajt u pravougaonik i potom proverava da li je mis
        // unutar njega
        sf::IntRect temp_rect(
            object.getPosition().x,
            object.getPosition().y,
            object.getGlobalBounds().width,
            object.getGlobalBounds().height);

        if (temp_rect.contains(sf::Mouse::getPosition(window))) {
            return true;
        }
    }

    return false;
}

void InputEngine::IsSpriteHovered(sf::Sprite object, sf::RenderWindow &window)
{

    sf::IntRect temp_rect(
        object.getPosition().x,
        object.getPosition().y,
        object.getGlobalBounds().width,
        object.getGlobalBounds().height);
    if (temp_rect.contains(sf::Mouse::getPosition(window))) {
        GG_TRACE("Hovered over button!");
    }
}

auto InputEngine::IsKeyPressed(sf::Keyboard::Key key) -> bool
{
    if (sf::Keyboard::isKeyPressed(key)) {
        return true;
    }
    return false;
}

auto InputEngine::GetMousePosition() -> sf::Vector2f
{
    std::shared_ptr<sf::Window> window =
        Application::GetInstance()->GetWindow();
    return sf::Vector2f(sf::Mouse::getPosition(*window.get()));
}

void InputEngine::PullEvents()
{
    sf::Event event;
    std::shared_ptr<sf::Window> window =
        Application::GetInstance()->GetWindow();
    while (window->pollEvent(event)) {
        if (event.type == sf::Event::Closed) {
            auto *e = new WindowCloseEvent();
            EventDispatcher::NotifySubscribers(e);
        }
        if (event.type == sf::Event::Resized) {
            auto *e =
                new WindowResizeEvent(event.size.width, event.size.height);
            EventDispatcher::NotifySubscribers(e);
        }
        if (event.type == sf::Event::MouseMoved) {
            auto *e =
                new MouseMovedEvent(event.mouseMove.x, event.mouseMove.y);
            EventDispatcher::NotifySubscribers(e);
        }
        if (event.type == sf::Event::MouseButtonPressed) {
            auto *e =
                new MouseButtonPressedEvent(event.mouseButton.button);
            EventDispatcher::NotifySubscribers(e);
        }
        if (event.type == sf::Event::MouseButtonReleased) {
            auto *e =
                new MouseButtonReleasedEvent(event.mouseButton.button);
            EventDispatcher::NotifySubscribers(e);
        }
        if (event.type == sf::Event::KeyPressed) {
            auto *e = new KeyPressedEvent(event.key.code);
            EventDispatcher::NotifySubscribers(e);
        }
        if (event.type == sf::Event::KeyReleased) {
            auto *e = new KeyReleasedEvent(event.key.code);
            EventDispatcher::NotifySubscribers(e);
        }
    }
}

}; // namespace gg
