#include <core/Application.hpp>
#include <core/InputEngine.hpp>
#include <events/KeyboardEvent.hpp>
#include <events/MouseEvent.hpp>
#include <events/WindowEvent.hpp>
#include <gui/SceneNode.hpp>

#include "../../libs/tinyxml2/tinyxml2.h"
#include <core/GameEngine.hpp>
#include <core/Map.hpp>
#include <gamestates/CustomGameState.hpp>
#include <gamestates/MainMenu.hpp>
#include <resources/Attributes.hpp>
#include <resources/FontMenager.hpp>
#include <resources/SceneManager.hpp>
#include <resources/StyleManager.hpp>
#include <resources/TextureManager.hpp>
#include <resources/XMLDocument.hpp>
#include <tile/BuildingTile.hpp>
#include <tile/RoadTile.hpp>
#include <tile/TerrainTile.hpp>

#include <cmath>

namespace gg
{

Application *Application::sInstance = nullptr;

Application::Application()
{
    GG_ASSERT(!sInstance, "Application already initialized!");
    sInstance = this;
    mRunning = true;

    mWindow = std::make_shared<sf::RenderWindow>(
        sf::VideoMode(GG_WINDOW_WIDTH, GG_WINDOW_HEIGHT), GG_WINDOW_TITLE);
#ifdef GG_WINDOW_VERTICAL_SYNC
    mWindow->setVerticalSyncEnabled(true);
#endif // GG_WINDOW_VERTICAL_SYNC

    ToggleFullscreen(true);
    mMusic = std::make_shared<sf::Music>();
    if (!mMusic->openFromFile(GG_RESOURCES_PATH + GG_RESOURCES_MUSIC)) {
        GG_WARN("Music not loaded");
    }
    // m_music->play();
    mMusic->setLoop(true);

    StyleManager::Init();
    FontManager::Init();
    TextureManager::Init();
    SceneManager::Init();
    GameEngine::Init();

    EventDispatcher::Subscribe(sInstance, EventType::WindowClose);
    EventDispatcher::Subscribe(sInstance, EventType::WindowResize);
    EventDispatcher::Subscribe(sInstance, EventType::KeyboardReleased);
    EventDispatcher::Subscribe(sInstance, EventType::KeyboardPressed);
    EventDispatcher::Subscribe(sInstance, EventType::ButtonClicked);

    GG_TRACE("Application started!");
};

Application::~Application()
{
    EventDispatcher::RemoveSubscriber(sInstance);

    GameEngine::Destroy();
    SceneManager::Destroy();
    TextureManager::Destroy();
    FontManager::Destroy();
    StyleManager::Destroy();

    mWindow->close();

    GG_TRACE("Application terminated!");
};

void Application::Run()
{
#ifdef GG_SHOW_FPS
    sf::Clock clock;
    sf::Time previous_time = clock.getElapsedTime();
#endif // GG_SHOW_FPS

    while (mRunning) {

#ifdef GG_SHOW_FPS
        sf::Time current_time = clock.getElapsedTime();
        float fps =
            1.0f / (current_time.asSeconds() - previous_time.asSeconds());
        previous_time = current_time;
        GG_TRACE("FPS: {}", fps);
#endif // GG_SHOW_FPS

        // Handle events
        InputEngine::PullEvents();

        // Handle game logic
        GameEngine::Update();

        mWindow->clear(sf::Color(193, 213, 245));

        // Handle rendering
        GameEngine::Render(*mWindow, sf::RenderStates::Default);
        // m_window->draw(*SceneManager::GetScene(SceneType::Custom));

        mWindow->display();
    }
}

void Application::ToggleFullscreen(bool fullscreen)
{

    if (fullscreen) {
        mWindow->create(
            sf::VideoMode(
                GG_FULLSCREEN_WINDOW_WIDTH, GG_FULLSCREEN_WINDOW_HEIGHT),
            GG_WINDOW_TITLE,
            sf::Style::Fullscreen);
    } else {
        mWindow->create(
            sf::VideoMode(GG_WINDOW_WIDTH, GG_WINDOW_HEIGHT), GG_WINDOW_TITLE);
    }
}

void Application::ToggleMusic(bool music)
{
    if (music) {
        mMusic->play();
    } else {
        mMusic->pause();
    }
}

void Application::OnEvent(IEvent *e)
{
    switch (e->GetEventType()) {
    case EventType::WindowClose:
        OnWindowCloseEvent(e);
        break;
    case EventType::WindowResize:
        OnWindowResizeEvent(e);
        break;
    case EventType::KeyboardPressed:
        OnKeyPressedEvent(e);
        break;
    case EventType::KeyboardReleased:
        OnKeyReleasedEvent(e);
        break;
    default:
        break;
    }
}

void Application::OnKeyReleasedEvent(IEvent *e)
{
    auto *key_released_event = static_cast<KeyReleasedEvent *>(e);
    // GG_TRACE(key_released_event->to_string());
}

void Application::OnKeyPressedEvent(IEvent *e)
{
    auto *key_pressed_event = static_cast<KeyPressedEvent *>(e);
}

void Application::OnWindowResizeEvent(IEvent *e)
{
    auto *window_resize_event =
        static_cast<WindowResizeEvent *>(e);

    sf::FloatRect visible_area(
        0,
        0,
        window_resize_event->GetWidth(),
        window_resize_event->GetHeight());

    if (GameEngine::CurrentState()->GetSceneType() == SceneType::Gameplay) {
        // Update map location when we resize window
        GG_INFO("Updating map begin location.");
        auto *gp = dynamic_cast<Gameplay *>(GameEngine::CurrentState());
        mWindow->setView(sf::View(visible_area));
        sf::View view = mWindow->getView();
        float width = view.getSize().x;
        float height = view.getSize().y;
        gp->GetMap()->SetBegin(sf::Vector2f(width / 2.2f, height / 20.0f));
        gp->GetMap()->ChangeLocation();
    }

    GG_TRACE(
        "Window resize event: ({}, {})",
        mWindow->getView().getSize().x,
        mWindow->getView().getSize().y);
}

void Application::OnWindowCloseEvent(IEvent *e)
{
    mRunning = false;
    GG_TRACE("Window close event");
}

}; // namespace gg
