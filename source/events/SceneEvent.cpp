#include <events/SceneEvent.hpp>

namespace gg
{
SceneButtonEvent::SceneButtonEvent(std::string &id) : IEvent(), mButtonID(id)
{
}

auto SceneButtonEvent::GetEventType() const -> EventType
{
    return EventType::ButtonClicked;
}

auto SceneButtonEvent::GetButtonID() const -> std::string
{
    ;
    return mButtonID;
}

SceneInputEvent::SceneInputEvent(std::string &id, std::string &content)
    : IEvent(), mInputID(id), mInputContent(content)
{
}

auto SceneInputEvent::GetEventType() const -> EventType
{
    return EventType::TextInserted;
}

auto SceneInputEvent::GetInputID() const -> std::string { return mInputID; }

auto SceneInputEvent::GetInputContent() const -> std::string { return mInputContent; }

SceneToggleButtonEvent::SceneToggleButtonEvent(
    std::string &id, bool toggleState)
    : SceneButtonEvent(id), mToggleState(toggleState)
{
}

auto SceneToggleButtonEvent::GetEventType() const -> EventType
{
    return EventType::ToggleButtonClicked;
}

auto SceneToggleButtonEvent::GetToggleState() const -> const bool
{
    return mToggleState;
}

}; // namespace gg
