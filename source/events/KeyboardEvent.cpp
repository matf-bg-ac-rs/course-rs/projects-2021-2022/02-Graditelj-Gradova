#include <events/KeyboardEvent.hpp>

namespace gg
{
KeyboardEvent::KeyboardEvent() : IEvent() {}

auto KeyboardEvent::GetKeycode() -> sf::Keyboard::Key { return mKeycode; }

auto KeyboardEvent::KeycodeToString() -> char
{
    if (this->mKeycode >= sf::Keyboard::A &&
        this->mKeycode <= sf::Keyboard::Z) {
        this->mKeycodeString =
            static_cast<char>(mKeycode - sf::Keyboard::A + 'a');
    } else {
        this->mKeycodeString = '~';
    }

    return mKeycodeString;
}

KeyPressedEvent::KeyPressedEvent(sf::Keyboard::Key keycode) : KeyboardEvent()
{
    mKeycode = keycode;
}

auto KeyPressedEvent::GetEventType() const -> EventType
{
    return EventType::KeyboardPressed;
}

auto KeyPressedEvent::to_string() -> std::string
{
    std::stringstream ss;
    ss << "Key " << KeycodeToString() << " pressed";
    return ss.str();
}

KeyReleasedEvent::KeyReleasedEvent(sf::Keyboard::Key keycode) : KeyboardEvent()
{
    mKeycode = keycode;
}

auto KeyReleasedEvent::GetEventType() const -> EventType
{
    return EventType::KeyboardReleased;
}

auto KeyReleasedEvent::to_string() -> std::string
{
    std::stringstream ss;
    ss << "Key " << KeycodeToString() << " released";
    return ss.str();
}

}; // namespace gg
