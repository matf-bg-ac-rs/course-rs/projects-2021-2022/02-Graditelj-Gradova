#include "../../include/events/WindowEvent.hpp"

namespace gg
{

WindowResizeEvent::WindowResizeEvent(unsigned width, unsigned height)
    : IEvent(), mWidth(width), mHeight(height)
{
}

auto WindowResizeEvent::GetEventType() const -> EventType
{
    return EventType::WindowResize;
}

auto WindowResizeEvent::GetWidth() const -> unsigned { return mWidth; }

auto WindowResizeEvent::GetHeight() const -> unsigned { return mHeight; }

WindowCloseEvent::WindowCloseEvent() : IEvent() {}

auto WindowCloseEvent::GetEventType() const -> EventType
{
    return EventType::WindowClose;
}

} // namespace gg
