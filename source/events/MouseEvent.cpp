#include <events/MouseEvent.hpp>

namespace gg
{

MouseMovedEvent::MouseMovedEvent(const float x, const float y)
    : IEvent(), mMouseX(x), mMouseY(y)
{
}

auto MouseMovedEvent::GetEventType() const -> EventType
{
    return EventType::MouseMoved;
}

auto MouseMovedEvent::GetMouseX() -> float { return mMouseX; }

auto MouseMovedEvent::GetMouseY() -> float { return mMouseY; }

auto MouseMovedEvent::to_string() const -> std::string
{
    std::stringstream ss;
    ss << "MousePosition(" << mMouseX << ", " << mMouseY << ")";
    return ss.str();
}

MouseButtonEvent::MouseButtonEvent(sf::Mouse::Button keycode)
    : IEvent(), mKeycode(keycode)
{
}

auto MouseButtonEvent::GetKeycode() -> sf::Mouse::Button { return mKeycode; }

auto MouseButtonEvent::keycode_to_string() const -> std::string
{
    switch (mKeycode) {
    case sf::Mouse::Left:
        return "Left";
    case sf::Mouse::Right:
        return "Right";
    case sf::Mouse::Middle:
        return "Middle";
    case sf::Mouse::XButton1:
        return "XButton1";
    case sf::Mouse::XButton2:
        return "XButton2";
    case sf::Mouse::ButtonCount:
        return "ButtonCount";
    }
    return "";
}

auto MouseButtonEvent::to_string() const -> std::string
{
    std::stringstream ss;
    ss << "MouseButton: " << keycode_to_string();
    return ss.str();
}

MouseButtonPressedEvent::MouseButtonPressedEvent(sf::Mouse::Button keycode)
    : MouseButtonEvent(keycode)
{
}

auto MouseButtonPressedEvent::GetEventType() const -> EventType
{
    return EventType::MousePressed;
}

MouseButtonReleasedEvent::MouseButtonReleasedEvent(sf::Mouse::Button keycode)
    : MouseButtonEvent(keycode)
{
}

auto MouseButtonReleasedEvent::GetEventType() const -> EventType
{
    return EventType::MouseReleased;
}

} // namespace gg
