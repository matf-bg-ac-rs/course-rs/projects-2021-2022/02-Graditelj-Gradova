#include <events/TimeEvent.hpp>

namespace gg
{

auto TimePassed::GetEventType() const -> EventType { return EventType::TimePassed; }

auto TimePassed::GetCategory() -> TimeCategory { return mCategory; }

TimePassed::TimePassed(TimeCategory category) : mCategory(category) {}
} // namespace gg
