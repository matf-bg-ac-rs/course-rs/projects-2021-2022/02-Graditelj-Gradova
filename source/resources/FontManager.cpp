#include <resources/FontMenager.hpp>
#include <resources/XMLDocument.hpp>

namespace gg
{

bool FontManager::sInitialized = false;
std::map<std::string, sf::Font *> FontManager::sFontLibrary = {};

void FontManager::Init()
{
    GG_ASSERT(!sInitialized, "FontManager already initialized!");
    GG_DEBUG_INFO("Initializing fonts...");
    sInitialized = true;

    XMLDocument doc = XMLDocument::LoadFromFile(
        GG_RESOURCES_PATH + GG_RESOURCES_FONTS_FILENAME);

    for (auto &child : doc.GetChildren()) {
        std::string name = child.GetAttributes().Get("Name");
        std::string path = child.GetAttributes().Get("Path");
        GG_DEBUG_TRACE(
            "Loading {} font from: {}", name, GG_RESOURCES_PATH + path);
        AddFont(name, GG_RESOURCES_PATH + path);
    }
}

void FontManager::Destroy()
{
    GG_DEBUG_TRACE("Destroying fonts...");
    for (auto &font : sFontLibrary) {
        delete font.second;
    }
}

auto FontManager::GetFont(const std::string &fontname) -> sf::Font *
{
    auto it = sFontLibrary.find(fontname);
    GG_ASSERT(
        it != sFontLibrary.end(), "Font does not exists in font library!");
    return it->second;
}

void FontManager::AddFont(
    const std::string &fontname, const std::string &filename)
{
    auto it = sFontLibrary.find(fontname);
    GG_ASSERT(it == sFontLibrary.end(), "Font already exists in font library!");
    sFontLibrary[fontname] = new sf::Font();
    sFontLibrary[fontname]->loadFromFile(filename);
}

auto FontManager::Size() -> std::size_t { return sFontLibrary.size(); }

} // namespace gg
