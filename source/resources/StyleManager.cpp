#include <resources/StyleManager.hpp>

namespace gg
{

bool StyleManager::sInitialized = false;
std::map<std::string, XMLDocument *> StyleManager::sStyleLibrary;

void StyleManager::Init()
{
    GG_DEBUG_ASSERT(!sInitialized, "StyleManager already initialized!");
    GG_DEBUG_INFO("Initializing styles...");
    sInitialized = true;

    XMLDocument doc = XMLDocument::LoadFromFile(
        GG_RESOURCES_PATH + GG_RESOURCES_STYLE_FILENAME);

    for (auto &style : doc.GetChildren()) {
        std::string name = style.GetName();
        GG_DEBUG_TRACE("Loading style: {}", name);
        AddStyle(name, style);
    }
}

void StyleManager::Destroy()
{
    GG_DEBUG_TRACE("Destroying styles...");
    for (auto &style : sStyleLibrary) {
        delete style.second;
    }

    sInitialized = false;
}

auto StyleManager::GetStyle(const std::string &styleName) -> XMLDocument *
{
    auto it = sStyleLibrary.find(styleName);

    GG_ASSERT(
        it != sStyleLibrary.end(), "Style does not exists in style library!");
    return it->second;
}

void StyleManager::AddStyle(const std::string &styleName, XMLDocument style)
{
    auto it = sStyleLibrary.find(styleName);

    GG_ASSERT(
        it == sStyleLibrary.end(), "Style already exists in style library!");
    sStyleLibrary[styleName] = new XMLDocument(
        style.GetName(),
        style.GetAttributes(),
        style.GetContent(),
        style.GetChildren());
}

auto StyleManager::GetInitialized() -> bool { return sInitialized; }

auto StyleManager::GetNumberOfStyles() -> unsigned
{
    return sStyleLibrary.size();
    }


}
