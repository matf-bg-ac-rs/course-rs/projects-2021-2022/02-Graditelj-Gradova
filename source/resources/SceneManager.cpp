#include <gui/Label.hpp>
#include <gui/Scene.hpp>
#include <gui/SceneNode.hpp>
#include <resources/SceneManager.hpp>

#include <resources/XMLDocument.hpp>

namespace gg
{

bool SceneManager::sInitialized = false;
std::vector<Scene *>
    SceneManager::sSceneLibrary(SceneManager::NUM_OF_SCENES, nullptr);

void SceneManager::Init()
{
    GG_ASSERT(!sInitialized, "SceneManager already initialized!");
    GG_DEBUG_INFO("Initializing scenes...");
    sInitialized = true;

    XMLDocument doc = XMLDocument::LoadFromFile(
        GG_RESOURCES_PATH + GG_RESOURCES_SCENES_FILENAME);

    for (auto &child : doc.GetChildren()) {
        std::string name = child.GetAttributes().Get("Name");
        std::string path = child.GetAttributes().Get("Path");
        GG_DEBUG_TRACE(
            "Loading {} scene from: {}", name, GG_RESOURCES_PATH + path);
        XMLDocument scene_doc =
            XMLDocument::LoadFromFile(GG_RESOURCES_PATH + path);
        auto *scene = new Scene(name, scene_doc);
        AddScene(StringToSceneType(name), scene);
    }
}

void SceneManager::Destroy()
{
    GG_DEBUG_TRACE("Destroying scenes...");
    for (auto scene : sSceneLibrary) {
        if (scene) {
            delete scene;
        }
    }
}

auto SceneManager::GetScene(const SceneType &sceneType) -> Scene *
{
    Scene *scene = sSceneLibrary[static_cast<int>(sceneType)];
    GG_ASSERT(scene, "Scene does not exists!");
    return scene;
}

void SceneManager::AddScene(const SceneType &sceneType, Scene *scene)
{
    sSceneLibrary[static_cast<int>(sceneType)] = scene;
}

auto SceneManager::SceneTypeToString(SceneType sceneType) -> const std::string
{
    switch (sceneType) {
    case gg::SceneType::Custom:
        return "Custom";
    case gg::SceneType::MainMenu:
        return "MainMenu";
    case gg::SceneType::NewGame:
        return "NewGame";
    case gg::SceneType::LoadGame:
        return "LoadGame";
    case gg::SceneType::Settings:
        return "Settings";
    case gg::SceneType::Gameplay:
        return "Gameplay";
    case gg::SceneType::PauseMenu:
        return "PauseMenu";
    case gg::SceneType::ControlsMenu:
        return "ControlsMenu";
    }

    GG_ASSERT(false, "Invalid scene type!");
}

auto SceneManager::StringToSceneType(const std::string &sceneType) -> SceneType
{
    if (sceneType == "Custom") {
        return SceneType::Custom;
    } else if (sceneType == "MainMenu") {
        return SceneType::MainMenu;
    } else if (sceneType == "NewGame") {
        return SceneType::NewGame;
    } else if (sceneType == "LoadGame") {
        return SceneType::LoadGame;
    } else if (sceneType == "Settings") {
        return SceneType::Settings;
    } else if (sceneType == "Gameplay") {
        return SceneType::Gameplay;
    } else if (sceneType == "PauseMenu") {
        return SceneType::PauseMenu;
    } else if (sceneType == "ControlsMenu") {
        return SceneType::ControlsMenu;
    }

    GG_ASSERT(false, "Invalide scene type: " + sceneType);
}

} // namespace gg
