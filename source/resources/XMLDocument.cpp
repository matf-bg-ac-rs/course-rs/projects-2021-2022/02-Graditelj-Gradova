#include <resources/XMLDocument.hpp>

namespace gg
{

XMLDocument::XMLDocument(
    std::string name,
    Attributes attributes,
    std::string content,
    std::vector<XMLDocument> children)
    : mName(std::move(name))
    , mAttributes(attributes)
    , mContent(std::move(content))
    , mChildren(std::move(children))
{
}

XMLDocument::XMLDocument(
    std::string name, Attributes attributes, std::string content)
    : mName(std::move(name))
    , mAttributes(attributes)
    , mContent(std::move(content))
{
}

void XMLDocument::AddChild(XMLDocument child)
{
    mChildren.emplace_back(std::move(child));
}

void XMLDocument::RemoveChild(int i) { mChildren.erase(mChildren.begin() + i); }

auto XMLDocument::GetName() const -> const std::string & { return mName; }

auto XMLDocument::GetAttributes() const -> const Attributes &
{
    return mAttributes;
}

auto XMLDocument::GetContent() const -> const std::string & { return mContent; }

auto XMLDocument::GetChildren() const -> const std::vector<XMLDocument> &
{
    return mChildren;
}

auto XMLDocument::GetChildren() -> std::vector<XMLDocument> &
{
    return mChildren;
}

auto XMLDocument::LoadFromFile(const std::string &filename) -> XMLDocument
{
    tinyxml2::XMLDocument doc;
    doc.LoadFile(filename.c_str());
    GG_ASSERT(
        !doc.Error(),
        "Cannot load filename: " + filename + ":\n" + doc.ErrorStr());

    auto *root = doc.FirstChild();
    return LoadFromElement(root->ToElement());
}

void XMLDocument::SaveToFile(
    const XMLDocument &document, const std::string &filename)
{
    tinyxml2::XMLDocument doc;
    doc.InsertEndChild(CreateElement(doc, document));
    doc.SaveFile(filename.c_str());
}

auto XMLDocument::LoadFromElement(tinyxml2::XMLElement *element) -> XMLDocument
{
    std::string name = element->Name();

    Attributes attributes = LoadAttributesFromElement(element);

    std::string content = "";
    if (element->GetText()) {
        content = element->GetText();
    }

    std::vector<XMLDocument> children;
    for (auto *child = element->FirstChildElement(); child != nullptr;
         child = child->NextSiblingElement()) {
        children.push_back(LoadFromElement(child));
    }

    return XMLDocument(name, attributes, content, children);
}

auto XMLDocument::LoadAttributesFromElement(tinyxml2::XMLElement *element) -> Attributes
{
    Attributes attributes;

    for (auto *attribute = element->FirstAttribute(); attribute != nullptr;
         attribute = attribute->Next()) {
        attributes.Add(attribute->Name(), attribute->Value());
    }

    return attributes;
}

auto XMLDocument::CreateElement(
    tinyxml2::XMLDocument &doc, const XMLDocument &document) -> tinyxml2::XMLElement *
{
    tinyxml2::XMLElement *element = doc.NewElement(document.GetName().c_str());

    for (auto &attribute : document.GetAttributes().GetLibrary()) {
        element->SetAttribute(
            attribute.first.c_str(), attribute.second.c_str());
    }

    if (!document.GetContent().empty()) {
        element->SetText(document.GetContent().c_str());
    }

    for (auto &child : document.GetChildren()) {
        element->InsertEndChild(CreateElement(doc, child));
    }

    return element;
}

} // namespace gg
