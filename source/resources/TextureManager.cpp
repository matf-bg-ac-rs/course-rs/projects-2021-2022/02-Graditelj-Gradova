#include <SFML/Graphics.hpp>

#include <resources/TextureManager.hpp>
#include <resources/XMLDocument.hpp>

namespace gg
{

bool TextureManager::sInitialized = false;
std::map<std::string, sf::Texture *> TextureManager::sTextureLibrary = {};

void TextureManager::Init()
{
    GG_DEBUG_ASSERT(!sInitialized, "TextureManager already initialized!");
    GG_DEBUG_INFO("Initializing textures...");
    sInitialized = true;

    XMLDocument doc = XMLDocument::LoadFromFile(
        GG_RESOURCES_PATH + GG_RESOURCES_TEXTURES_FILENAME);
    for (auto &child : doc.GetChildren()) {
        std::string name = child.GetAttributes().Get("Name");
        std::string path = child.GetAttributes().Get("Path");
        GG_DEBUG_TRACE(
            "Loading {} texture from: {}", name, GG_RESOURCES_PATH + path);
        AddTexture(name, GG_RESOURCES_PATH + path);
    }
}

void TextureManager::Destroy()
{
    GG_DEBUG_TRACE("Destroying textures...");
    for (auto &texture : sTextureLibrary) {
        delete texture.second;
    }

    sInitialized = false;
}

void TextureManager::AddTexture(
    const std::string &name, const std::string &filename)
{
    GG_DEBUG_ASSERT(
        sTextureLibrary.find(name) == sTextureLibrary.end(),
        "Texture already exists in texture library!");
    sTextureLibrary[name] = new sf::Texture();
    sTextureLibrary[name]->loadFromFile(filename);
}

auto TextureManager::GetTexture(const std::string &texture) -> sf::Texture *
{
    auto it = sTextureLibrary.find(texture);
    GG_DEBUG_ASSERT(
        it != sTextureLibrary.end(),
        "Texture does not exists in texture library!");

    return it->second;
}

auto TextureManager::GetInitialized() -> bool { return sInitialized; }

auto TextureManager::GetNumberOfTexures() -> unsigned
{
    return sTextureLibrary.size();
}

} // namespace gg
