#include <gui/Box.hpp>
#include <resources/Attributes.hpp>
#include <resources/FontMenager.hpp>
#include <resources/XMLDocument.hpp>

namespace gg
{

void Attributes::Add(const std::string &key, const std::string &value)
{
    GG_DEBUG_ASSERT(
        !Has(key), "There already exists attribute with given key!");
    mAttributeLibrary[key] = value;
}

auto Attributes::Has(const std::string &key) const -> bool
{
    auto it = mAttributeLibrary.find(key);
    return it != mAttributeLibrary.end();
}

auto Attributes::Get(const std::string &key) const -> std::string
{
    GG_DEBUG_ASSERT(Has(key), "There is not attribute with given key: " + key);
    return mAttributeLibrary.find(key)->second;
}

auto Attributes::GetLibrary() const -> const std::map<std::string, std::string>
{
    return mAttributeLibrary;
}

template <typename T> auto Attributes::Get(const std::string &key) const -> T
{
    GG_DEBUG_ASSERT(Has(key), "There is not attribute with given key: " + key);
    std::istringstream stream(mAttributeLibrary.at(key));
    T value;
    stream >> value;
    return value;
}

template <> auto Attributes::Get(const std::string &key) const -> bool
{
    GG_DEBUG_ASSERT(Has(key), "There is not attribute with given key: " + key);
    return std::stoi(mAttributeLibrary.at(key));
}

template <> auto Attributes::Get(const std::string &key) const -> int
{
    GG_DEBUG_ASSERT(Has(key), "There is not attribute with given key: " + key);
    return std::stoi(mAttributeLibrary.at(key));
}

template <> auto Attributes::Get(const std::string &key) const -> unsigned int
{
    GG_DEBUG_ASSERT(Has(key), "There is not attribute with given key: " + key);
    return std::stoul(mAttributeLibrary.at(key));
}

template <> auto Attributes::Get(const std::string &key) const -> float
{
    GG_DEBUG_ASSERT(Has(key), "There is not attribute with given key: " + key);
    return std::stof(mAttributeLibrary.at(key));
}

template <> auto Attributes::Get(const std::string &key) const -> double
{
    GG_DEBUG_ASSERT(Has(key), "There is not attribute with given key: " + key);
    return std::stod(mAttributeLibrary.at(key));
}

template <> auto Attributes::Get(const std::string &key) const -> sf::Vector2i
{
    GG_DEBUG_ASSERT(Has(key), "There is not attribute with given key: " + key);

    std::istringstream stream(mAttributeLibrary.at(key));
    std::string x, y;
    stream >> x >> y;

    return sf::Vector2i(std::stoi(x), std::stoi(y));
}

template <> auto Attributes::Get(const std::string &key) const -> sf::Vector2f
{
    GG_DEBUG_ASSERT(Has(key), "There is not attribute with given key: " + key);

    std::istringstream stream(mAttributeLibrary.at(key));
    std::string x, y;
    stream >> x >> y;

    return sf::Vector2f(std::stof(x), std::stof(y));
}

template <> auto Attributes::Get(const std::string &key) const -> sf::Color
{
    GG_DEBUG_ASSERT(Has(key), "There is not attribute with given key: " + key);

    std::istringstream stream(mAttributeLibrary.at(key));
    std::string r, g, b, a;
    stream >> r >> g >> b;
    if (!(stream >> a)) {
        a = "0xff";
    }

    return sf::Color(
        std::stoi(r, nullptr, 16),
        std::stoi(g, nullptr, 16),
        std::stoi(b, nullptr, 16),
        std::stoi(a, nullptr, 16));
}

template <> auto Attributes::Get(const std::string &key) const -> sf::IntRect
{
    GG_DEBUG_ASSERT(Has(key), "There is not attribute with given key: " + key);

    std::istringstream stream(mAttributeLibrary.at(key));
    int x, y, width, height;
    stream >> x >> y >> width >> height;

    return sf::IntRect(x, y, width, height);
}

template <> auto Attributes::Get(const std::string &key) const -> const sf::Font &
{
    GG_DEBUG_ASSERT(Has(key), "There is not attribute with given key: " + key);
    return *FontManager::GetFont(mAttributeLibrary.at(key));
}

template <> auto Attributes::Get(const std::string &key) const -> VAlignment
{
    GG_DEBUG_ASSERT(Has(key), "There is not attribute with given key: " + key);

    std::string alignment = mAttributeLibrary.at(key);
    if (alignment == "Top") {
        return VAlignment::Top;
    } else if (alignment == "Bottom") {
        return VAlignment::Bottom;
    } else if (alignment == "Center") {
        return VAlignment::Center;
    }

    GG_ASSERT(false, "Incorrectly set VAlignment");
}

template <> auto Attributes::Get(const std::string &key) const -> HAlignment
{
    GG_DEBUG_ASSERT(Has(key), "There is not attribute with given key: " + key);

    std::string alignment = mAttributeLibrary.at(key);
    if (alignment == "Left") {
        return HAlignment::Left;
    } else if (alignment == "Right") {
        return HAlignment::Right;
    } else if (alignment == "Center") {
        return HAlignment::Center;
    }

    GG_ASSERT(false, "Incorrectly set HAlignment");
}

template <> auto Attributes::Get(const std::string &key) const -> Margins
{
    GG_DEBUG_ASSERT(Has(key), "There is not attribute with given key: " + key);

    std::istringstream stream(mAttributeLibrary.at(key));
    std::string top, bottom, left, right;
    stream >> top >> bottom >> left >> right;

    return Margins{
        std::stoi(top), std::stoi(bottom), std::stoi(left), std::stoi(right)};
}

} // namespace gg
