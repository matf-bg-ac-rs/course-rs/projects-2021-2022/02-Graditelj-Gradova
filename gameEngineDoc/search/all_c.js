var searchData=
[
  ['newgame_0',['NewGame',['../classgg_1_1NewGame.html',1,'gg::NewGame'],['../classgg_1_1Gameplay.html#aa7c208dd9325562dd9203b4db25a6020',1,'gg::Gameplay::NewGame()'],['../classgg_1_1NewGame.html#a58474a0d48013bf2bad1241ab0cc13eb',1,'gg::NewGame::NewGame()'],['../namespacegg.html#a185920557ea920066338d5dec2aa9cf9a31947c4eece1b08b70e9ba88dae24416',1,'gg::NewGame()']]],
  ['newgame_2ehpp_1',['NewGame.hpp',['../NewGame_8hpp.html',1,'']]],
  ['none_2',['None',['../namespacegg.html#ada69b9f844678d00cd5c2b2c30a20122a6adf97f83acf6453d4a6a4b1070f3754',1,'gg::None()'],['../namespacegg.html#a98db2e09803bf503c02d952e6a8840f4a6adf97f83acf6453d4a6a4b1070f3754',1,'gg::None()']]],
  ['normal_3',['Normal',['../namespacegg.html#a2cacabcdf9a5efbaa4b5e29609049e6ba960b44c579bc2f6818d2daaf9e4c16f0',1,'gg']]],
  ['notifysubscribers_4',['NotifySubscribers',['../classgg_1_1EventDispatcher.html#ac0c4379683c748e569a9fa6be55bab55',1,'gg::EventDispatcher']]],
  ['numberofsubscribers_5',['NumberOfSubscribers',['../classgg_1_1EventDispatcher.html#a7ad81d66b2f595f42de4e7841ef11044',1,'gg::EventDispatcher']]]
];
