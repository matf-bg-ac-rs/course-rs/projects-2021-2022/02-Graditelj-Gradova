var searchData=
[
  ['s_5fcorelogger_0',['s_coreLogger',['../classgg_1_1Logger.html#a7c882224e94115f579600263a36a4eca',1,'gg::Logger']]],
  ['s_5fdebuglogger_1',['s_debugLogger',['../classgg_1_1Logger.html#a5adc5af77e864f14ed8ae2ec338d8b58',1,'gg::Logger']]],
  ['sfontlibrary_2',['sFontLibrary',['../classgg_1_1FontManager.html#a5da42340e8dd211509331012181bfe08',1,'gg::FontManager']]],
  ['sinitialized_3',['sInitialized',['../classgg_1_1FontManager.html#a8d2985661b185c14c0cc23c3f964dbde',1,'gg::FontManager::sInitialized()'],['../classgg_1_1SceneManager.html#adbe594d7553c88a25a70371ff1249566',1,'gg::SceneManager::sInitialized()'],['../classgg_1_1StyleManager.html#a972590f110a85b1393e0d09e353089e7',1,'gg::StyleManager::sInitialized()'],['../classgg_1_1TextureManager.html#a11c5724ade416a230945f05dc3f591e9',1,'gg::TextureManager::sInitialized()']]],
  ['sinstance_4',['sInstance',['../classgg_1_1Application.html#ae8ce58c1ab69793faa116e4a6a0dd867',1,'gg::Application']]],
  ['sscenelibrary_5',['sSceneLibrary',['../classgg_1_1SceneManager.html#abddb625c1dec38b3fe9a3261a1236a86',1,'gg::SceneManager']]],
  ['sstylelibrary_6',['sStyleLibrary',['../classgg_1_1StyleManager.html#a107d3d5341e81994eab575a69e4157e7',1,'gg::StyleManager']]],
  ['stexturelibrary_7',['sTextureLibrary',['../classgg_1_1TextureManager.html#aaaa49e216b66eb1d44fe164ed375af22',1,'gg::TextureManager']]]
];
