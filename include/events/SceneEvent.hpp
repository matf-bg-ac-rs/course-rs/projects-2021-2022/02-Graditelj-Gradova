#pragma once

#include <core/Event.hpp>

namespace gg
{

class SceneButtonEvent : public IEvent
{
  public:
    SceneButtonEvent(std::string &id);

    ~SceneButtonEvent() override = default;

    [[nodiscard]] inline auto GetEventType() const -> EventType override;

    [[nodiscard]] auto GetButtonID() const -> std::string;

  private:
    std::string mButtonID;
};

class SceneToggleButtonEvent : public SceneButtonEvent
{
  public:
    SceneToggleButtonEvent(std::string &, bool);

    ~SceneToggleButtonEvent() override = default;

    [[nodiscard]] inline auto GetEventType() const -> EventType override;

    [[nodiscard]] auto GetToggleState() const -> const bool;

  private:
    bool mToggleState;
};

class SceneInputEvent : public IEvent
{
  public:
    SceneInputEvent(std::string &id, std::string &content);

    ~SceneInputEvent() override = default;

    [[nodiscard]] inline auto GetEventType() const -> EventType override;

    [[nodiscard]] inline auto GetInputID() const -> std::string;

    [[nodiscard]] inline auto GetInputContent() const -> std::string;

  private:
    std::string mInputID;
    std::string mInputContent;
};

}; // namespace gg
