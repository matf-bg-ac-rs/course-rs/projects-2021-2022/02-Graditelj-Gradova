#pragma once

#include <SFML/Graphics.hpp>
#include <core/Event.hpp>

namespace gg
{

class MouseMovedEvent : public IEvent
{
  public:
    MouseMovedEvent(const float, const float);
    ~MouseMovedEvent() override = default;

    [[nodiscard]] auto GetEventType() const -> EventType override;

    auto GetMouseX() -> float;
    auto GetMouseY() -> float;

    [[nodiscard]] auto to_string() const -> std::string;

  private:
    float mMouseX;
    float mMouseY;
};

class MouseButtonEvent : public IEvent
{
  public:
    MouseButtonEvent(sf::Mouse::Button);
    ~MouseButtonEvent() override = default;

    [[nodiscard]] auto GetEventType() const -> EventType override = 0;

    auto GetKeycode() -> sf::Mouse::Button;

    [[nodiscard]] auto to_string() const -> std::string;

  private:
    [[nodiscard]] auto keycode_to_string() const -> std::string;

  private:
    sf::Mouse::Button mKeycode;
};

class MouseButtonPressedEvent : public MouseButtonEvent
{
  public:
    MouseButtonPressedEvent(sf::Mouse::Button);
    ~MouseButtonPressedEvent() override = default;

    [[nodiscard]] auto GetEventType() const -> EventType override;
};

class MouseButtonReleasedEvent : public MouseButtonEvent
{
  public:
    MouseButtonReleasedEvent(sf::Mouse::Button);
    ~MouseButtonReleasedEvent() override = default;

    [[nodiscard]] auto GetEventType() const -> EventType override;
};
}; // namespace gg
