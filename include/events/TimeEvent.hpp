#pragma once
#include <core/Event.hpp>

namespace gg
{
enum class TimeCategory {
    SECUND,
    TEN_SECONDS,
    MINUTE,
};

class TimePassed : public IEvent
{
  public:
    TimePassed(TimeCategory);
    ~TimePassed() override = default;

    [[nodiscard]] auto GetEventType() const -> EventType override;

    auto GetCategory() -> TimeCategory;

  private:
    TimeCategory mCategory;
};
}; // namespace gg
