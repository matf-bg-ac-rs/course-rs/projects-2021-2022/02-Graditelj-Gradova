#include "../core/Event.hpp"

#pragma once

namespace gg
{

class WindowResizeEvent : public IEvent
{
  public:
    WindowResizeEvent(unsigned int width, unsigned int height);
    ~WindowResizeEvent() override = default;

    [[nodiscard]] inline auto GetEventType() const -> EventType override;

    [[nodiscard]] auto GetWidth() const -> unsigned;
    [[nodiscard]] auto GetHeight() const -> unsigned;

  private:
    unsigned mWidth, mHeight;
};

class WindowCloseEvent : public IEvent
{
  public:
    WindowCloseEvent();
    ~WindowCloseEvent() override = default;

    [[nodiscard]] inline auto GetEventType() const -> EventType override;
};

} // namespace gg
