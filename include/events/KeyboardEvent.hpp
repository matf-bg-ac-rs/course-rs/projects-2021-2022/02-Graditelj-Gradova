#pragma once

#include <SFML/Graphics.hpp>
#include <core/Event.hpp>
#include <sstream>

namespace gg
{

class KeyboardEvent : public IEvent
{
  public:
    KeyboardEvent();
    ~KeyboardEvent() override = default;

    [[nodiscard]] inline auto GetEventType() const -> EventType override = 0;

    auto GetKeycode() -> sf::Keyboard::Key;
    auto KeycodeToString() -> char;
    virtual auto to_string() -> std::string = 0;

  protected:
    // properly detects A-Z; if some other key is pressed becomes '~' to
    // indicate that
    char mKeycodeString;
    sf::Keyboard::Key mKeycode;
};

class KeyPressedEvent : public KeyboardEvent
{
  public:
    KeyPressedEvent(sf::Keyboard::Key);
    ~KeyPressedEvent() override = default;

    [[nodiscard]] inline auto GetEventType() const -> EventType override;

    auto to_string() -> std::string override;

  private:
};

class KeyReleasedEvent : public KeyboardEvent
{
  public:
    KeyReleasedEvent(sf::Keyboard::Key keycode);
    ~KeyReleasedEvent() override = default;

    [[nodiscard]] inline auto GetEventType() const -> EventType override;

    auto to_string() -> std::string override;

  private:
};

}; // namespace gg
