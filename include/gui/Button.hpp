#pragma once

#include "Box.hpp"
#include "Container.hpp"
#include "Image.hpp"
#include "SceneNode.hpp"

namespace gg
{

enum class ButtonState { Normal = 0, Hovered, Pressed };

class Button : public SceneNode, public IEventSubscriber
{
  public:
    Button(const Attributes &);
    ~Button() override;

    void SetText(const std::string &);
    void SetSize(const float, const float);

    void SetHAlignment(HAlignment);
    void SetVAlignment(VAlignment);

    void SetMargins(Margins);

    void OnEvent(IEvent *e) override;

    void SetState(ButtonState);

  protected:
    void ApplyStyle() override;
    void ApplyDesign() override;

    virtual void OnMouseMovedEvent(IEvent *e);
    virtual void OnMousePressedEvent(IEvent *e);
    virtual void OnMouseReleasedEvent(IEvent *e);

  protected:
    void UpdateDesign() override;

    void SetTextCharacterSize(unsigned int);
    void SetTextFontName(const std::string &);

    void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

  protected:
    sf::Text mText;
    RoundedShape mShape;

    ButtonState mState;

  private:
    HAlignment mTextHAlignment;
    VAlignment mTextVAlignment;

    Margins mMargins;
};

}; // namespace gg
