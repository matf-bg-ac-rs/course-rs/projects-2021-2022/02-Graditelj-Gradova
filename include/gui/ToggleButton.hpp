#pragma once

#include "Button.hpp"

namespace gg
{

class ToggleButton : public Button
{

  public:
    ToggleButton(const Attributes &);
    ~ToggleButton() override = default;

    void ToggleState();
    auto GetToggleState() -> bool;

    void SetToggleGroup() override;

  protected:
    void ApplyStyle() override;

    void OnMouseReleasedEvent(IEvent *e) override;

  private:
    bool mToggleState;
};

}; // namespace gg
