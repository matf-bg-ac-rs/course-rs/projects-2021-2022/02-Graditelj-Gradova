#pragma once

#include "SceneNode.hpp"

#include <resources/Attributes.hpp>

namespace gg
{

enum class HAlignment { Left = 0, Center, Right };

enum class VAlignment { Top = 0, Center, Bottom };

struct Margins {
    int top = 0;
    int bottom = 0;
    int left = 0;
    int right = 0;
};

class Attributes;

class Box : public SceneNode
{
  public:
    Box(const Attributes &);
    ~Box() override = default;

    void SetSpacing(float);

    void SetHAlignment(const HAlignment &);
    void SetVAlignment(const VAlignment &);

    void SetMargins(const struct Margins &);

  protected:
    void UpdateSize() override;
    void UpdateAlignment() override;

    virtual void Align() = 0;
    virtual void CalculateSize() = 0;

    void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

  protected:
    struct Margins mMargins;

    float mSpacing;

    HAlignment mHorizontalAlignment;
    VAlignment mVerticalAlignment;
};

class HBox : public Box
{
  public:
    HBox(const Attributes &attributes);
    ~HBox() override = default;

  protected:
    void Align() override;
    void CalculateSize() override;
};

class VBox : public Box
{
  public:
    VBox(const Attributes &attributes);
    ~VBox() override = default;

  protected:
    void Align() override;
    void CalculateSize() override;
};

}; // namespace gg
