
#include "ToggleGroup.hpp"
#pragma once

#include <SFML/Graphics/Drawable.hpp>

#include <core/Event.hpp>

#include <resources/XMLDocument.hpp>

namespace gg
{

class SceneNode;
class Label;

class Scene : public sf::Drawable, public IEventSubscriber
{
  public:
    Scene(std::string , XMLDocument);
    ~Scene() override;

  private:
    void AddNode(const std::string &, SceneNode *);
    void AddNodeDefaultName(SceneNode *);

  public:
    void AddRootNode(const std::string &, SceneNode *);
    void AddRootNodeDefaultName(SceneNode *);
    void AddToggleButtonToToggleGroup(const std::string &, ToggleButton *);

    auto GetNode(const std::string &) -> SceneNode *;
    [[nodiscard]] auto GetNode(const std::string &) const -> const SceneNode *;
    auto GetToggleGroup(const std::string &) -> ToggleGroup *;
    [[nodiscard]] auto GetToggleGroup(const std::string &) const -> const ToggleGroup *;

    auto GetLabelByID(const std::string &) -> Label *;
    auto GetLabelByID(SceneNode *, const std::string &) -> Label *;

    void RemoveNode(const std::string &);
    void RemoveNode(SceneNode *);
    void RemoveToggleGroup(const std::string &);

    void SetActive(bool);
    auto IsActive() -> bool;

    void Update();

    [[nodiscard]] auto GetName() const -> const std::string &;
    [[nodiscard]] auto GetPosition() const -> const sf::Vector2f;
    [[nodiscard]] auto GetSize() const -> const sf::Vector2f;

  public:
    void OnEvent(IEvent *e) override;

  protected:
    void
    draw(sf::RenderTarget &target, sf::RenderStates states) const override;

  private:
    auto LoadSceneNode(XMLDocument) -> SceneNode *;

    void UpdateSize(unsigned, unsigned);
    [[nodiscard]] auto GenerateName() const -> std::string;

  private:
    std::string mName;

    sf::Vector2f mPosition;
    sf::Vector2f mSize;

    std::map<std::string, SceneNode *> mNodes;
    std::vector<SceneNode *> mRootNodes;

    std::map<std::string, ToggleGroup *> mToggleGroups;

    static int mNodeID;

    bool mActive = false;
};

}; // namespace gg
