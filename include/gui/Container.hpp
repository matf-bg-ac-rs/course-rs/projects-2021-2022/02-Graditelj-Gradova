#pragma once

#include <SFML/Graphics.hpp>

#include "Box.hpp"
#include "SceneNode.hpp"

namespace gg
{

class RoundedShape : public sf::Shape
{
  public:
    RoundedShape(
        const sf::Vector2f &size = sf::Vector2f(100, 100),
        float radius = 0,
        std::size_t pointCount = 100);
    ~RoundedShape() override = default;

    void SetSize(const sf::Vector2f &);
    void SetRadius(float);
    void SetPointCount(std::size_t);

  public:
    auto getPointCount() const -> std::size_t override;
    auto getPoint(std::size_t index) const -> sf::Vector2f override;

  private:
    sf::Vector2f mSize;
    float mRadius;
    std::size_t mPointCount;
};

class Container : public SceneNode
{
  public:
    Container(const Attributes &);
    ~Container() override = default;

    void SetMargins(Margins);
    void SetColor(sf::Color);

  protected:
    void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

    void UpdateSize() override;
    void UpdateAlignment() override;
    void ApplyDesign() override;
    void ApplyStyle() override;

  private:
    bool mRelative;

    Margins mMargins;

    RoundedShape mShape;
};

}; // namespace gg
