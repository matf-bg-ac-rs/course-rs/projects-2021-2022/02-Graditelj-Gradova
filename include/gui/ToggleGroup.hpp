#pragma once

#include "ToggleButton.hpp"

namespace gg
{

class ToggleGroup
{
  public:
    ToggleGroup();
    virtual ~ToggleGroup() = default;

    void AddToggle(ToggleButton *);
    void RemoveToggle(ToggleButton *);
    void ClearToggle();

    void SelectToggle(ToggleButton *);
    auto GetSelectedToggle() -> ToggleButton *;

  public:
    std::size_t mActiveToggleIndex;
    std::vector<ToggleButton *> mToggles;
};

}; // namespace gg
