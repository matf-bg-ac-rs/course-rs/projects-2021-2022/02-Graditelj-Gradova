#pragma once

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Text.hpp>

#include <core/Event.hpp>

namespace gg
{

class Scene;
class Attributes;
class XMLDocument;

class SceneNode : public sf::Drawable
{
  public:
    SceneNode(const Attributes &attributes, const bool root = false);
    ~SceneNode() override;

    void SetName(const std::string &);
    auto GetName() const -> const std::string &;

    void SetRoot(bool);
    auto IsRoot() const -> const bool;

    void SetParent(SceneNode *);
    auto GetParent() -> SceneNode *;

    void SetScene(Scene *);
    auto GetScene() -> Scene *;

    virtual void SetToggleGroup();

    void SetID(const std::string &);
    auto GetID() const -> const std::string &;

    void SetInsidePosition(const sf::Vector2f &);
    void SetOutsidePosition(const sf::Vector2f &);

    auto GetInsidePosition() const -> const sf::Vector2f &;
    auto GetOutsidePosition() const -> const sf::Vector2f &;

    void SetInsideSize(const sf::Vector2f &);
    void SetOutsideSize(const sf::Vector2f &);

    auto GetInsideSize() const -> const sf::Vector2f &;
    auto GetOutsideSize() const -> const sf::Vector2f &;

    void AppendChild(SceneNode *);
    void RemoveChild(SceneNode *);
    auto GetChildren() -> std::vector<SceneNode *>;

    virtual void Update();
    virtual void Render(sf::RenderTarget &, sf::RenderStates) const;

    auto IsDirty() const -> bool;
    void SetDirty();
    void ResetDirty();

    auto ContainsPoint(const sf::Vector2f &) -> bool;

    virtual void UpdateSize();
    virtual void UpdateAlignment();

  protected:
    virtual void UpdateDesign();
    virtual void UpdateStyle();
    virtual void ApplyDesign();
    virtual void ApplyStyle();

  private:
    void draw(sf::RenderTarget &, sf::RenderStates) const override;

  protected:
    std::string mName = "";
    std::string mId = "";
    bool mRoot;

    sf::Text mText;
    sf::RectangleShape mBackground;
    sf::RectangleShape mInsideArea;
    sf::RectangleShape mOutsideArea;

    sf::Vector2f mInsidePosition;
    sf::Vector2f mOutsidePosition;
    sf::Vector2f mInsideSize;
    sf::Vector2f mOutsideSize;

    float mPadding = 3.0f;

    XMLDocument *mStyle;

    SceneNode *mParent;
    std::vector<SceneNode *> mChildren;

    Scene *mScene;
    std::string mToggleGroup = "";

  private:
    bool mDirty;
};

}; // namespace gg
