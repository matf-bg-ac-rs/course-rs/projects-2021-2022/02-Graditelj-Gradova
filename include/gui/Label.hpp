#pragma once

#include "SceneNode.hpp"

#include <SFML/Graphics/Text.hpp>

namespace gg
{
class Label : public SceneNode
{
  public:
    Label(const Attributes &);
    ~Label() override = default;

    void SetText(const std::string &);
    void SetFontName(const std::string &);
    void SetFontSize(const unsigned);
    void SetColor(const sf::Color &);

    void UpdateSize() override;
    void Render(sf::RenderTarget &, sf::RenderStates) const override;

  protected:
    void ApplyStyle() override;
    void ApplyDesign() override;

  private:
    void draw(sf::RenderTarget &, sf::RenderStates) const override;

    void CalculateSize();

  private:
    sf::Text mText;
    std::string mFontname;
    sf::Color mColor;
};

}; // namespace gg
