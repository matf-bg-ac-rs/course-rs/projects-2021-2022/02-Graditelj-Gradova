#pragma once

#include "SceneNode.hpp"

#include <SFML/Graphics.hpp>

namespace gg
{

class Image : public SceneNode
{
  public:
    Image(const Attributes &);
    ~Image() override = default;

  protected:
    void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

    void UpdateSize() override;
    void ApplyDesign() override;
    void UpdateDesign() override;

  private:
    sf::Sprite mImage;
};

}; // namespace gg
