#pragma once

#include "Tile.hpp"
#include <SFML/Graphics.hpp>
#include <resources/Attributes.hpp>

#include <string>

namespace gg
{
enum class RoadType {
    BRIDGE,
    BRIDGE2,
    STRAIGHT,
    END,
    CROSS,
    DOWN,
    T,
    CROSSROAD
};

class RoadTile : public Tile
{
  public:
    RoadTile(Attributes &attributes);
    ~RoadTile() override = default;

    void Update() override;
    void SetType(const RoadType &type);
    auto GetType() -> RoadType;
    auto GetCost() -> unsigned;

    auto GetAttributes() -> Attributes override;
    auto GetTileType() -> TileType override;

    static auto RoadTypeToString(const RoadType type) -> std::string;
    static auto StringToRoadType(const std::string &str) -> RoadType;

    void Align() override;

  private:
    RoadType mType;
    unsigned mCost;
};
}; // namespace gg
