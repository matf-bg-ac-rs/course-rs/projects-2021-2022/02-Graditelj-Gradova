#pragma once

#include "Tile.hpp"

#include <SFML/Graphics.hpp>
#include <resources/Attributes.hpp>

#include <string>

namespace gg
{
enum class BuildingType { RESIDENTIAL, INDUSTRIAL };

class BuildingTile : public Tile
{
  public:
    BuildingTile(Attributes &attributes);
    ~BuildingTile() override = default;

    auto IsResidential() -> bool;
    auto IsIndustrial() -> bool;

    auto GetAttributes() -> Attributes override;
    auto GetTileType() -> TileType override;
    auto GetCost() -> unsigned int;
    auto GetPopulation() -> unsigned int;
    auto GetProfit() -> unsigned int;

    static auto BuildingTypeToString(BuildingType type) -> std::string;
    static auto StringToBuildingType(const std::string &str) -> BuildingType;

    void Align() override;

  private:
    BuildingType mType;
    unsigned int mCost;
    unsigned int mPopulation;
    int mProfit;
};

}; // namespace gg
