#pragma once

#include "Tile.hpp"
#include <SFML/Graphics.hpp>
#include <resources/Attributes.hpp>

#include <string>

namespace gg
{
enum class TerrainType { GRASS, WATER, WATERDOWN, CANAL, TREE };

class TerrainTile : public Tile
{
  public:
    TerrainTile(const Attributes &attributes);
    ~TerrainTile() override = default;

    auto IsWater() -> bool;
    auto IsGrass() -> bool;

    auto GetAttributes() -> Attributes override;
    auto GetTileType() -> TileType override;

    static auto TerrainTypeToString(const TerrainType terrainType) -> std::string;
    static auto StringToTerrainType(const std::string &str) -> TerrainType;

    auto GetTerrainType() -> TerrainType;

    void Align() override;

  private:
    TerrainType mTerrainType;
};
}; // namespace gg
