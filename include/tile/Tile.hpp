#pragma once

#include <SFML/Graphics.hpp>
#include <core/Event.hpp>
#include <resources/Attributes.hpp>
#include <string>

namespace gg
{

enum class TileType { Terrain, Road, Building };

struct TileVertices {
    sf::Vector2f left;
    sf::Vector2f top;
    sf::Vector2f right;
    sf::Vector2f bottom;
};

class Tile : public sf::Drawable, public IEventSubscriber
{
  public:
    void draw(sf::RenderTarget &target, sf::RenderStates states) const override;
    virtual void Update();

    Tile(
        sf::Vector2f coordinates,
        sf::Texture &texture,
        std::string texture_name,
        unsigned rotation);
    ~Tile() override = 0;

    virtual auto GetAttributes() -> Attributes = 0;
    virtual auto GetTileType() -> TileType = 0;

    // Returns vertices in the following order:
    // Left, Up, Right, Down
    auto GetVertices() -> TileVertices;
    auto GetCoordinates() -> sf::Vector2f;
    auto GetTexture() -> sf::Texture *;
    inline auto GetTextureName() const -> const std::string &
    {
        return mTextureName;
    }

    void SetTexture(std::string &);
    void SetCoordinates(sf::Vector2f);
    auto GetOrientation() -> unsigned;

    void OnEvent(IEvent *e) override;

  protected:
    virtual void OnMouseMoved(IEvent *e);
    virtual void OnMousePressed(IEvent *e);
    virtual void OnKeyboardReleased(IEvent *e);

    virtual void Align() = 0;

  private:
    void SetHover(bool);
    auto IsContained(const sf::Vector2f &) -> bool;
    void ApplyGlow();

  protected:
    sf::Texture *mTexture;
    sf::Vector2f mCoordinates;
    sf::Sprite mSprite;
    std::string mTextureName;
    unsigned mRotation = 0;

  private:
    bool mDirty = false;
    bool mHovered = false;
};
}; // namespace gg
