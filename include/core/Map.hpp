#pragma once

#include <SFML/Graphics.hpp>
#include <resources/Attributes.hpp>
#include <resources/XMLDocument.hpp>
#include <tile/BuildingTile.hpp>
#include <tile/RoadTile.hpp>
#include <tile/TerrainTile.hpp>
#include <tile/Tile.hpp>

namespace gg
{

class Map
{
  private:
    // void DepthFirstSearch(std::vector<> &whiteList, sf::Vector2i pos, int
    // label, int type);

  public:
    Map() = default;
    ~Map();

    void LoadFromXML(const XMLDocument &);
    void SaveToFilename(const std::string &);

    void draw(sf::RenderWindow &window);
    void Update();

    void SetWidth(unsigned int width);
    void SetHeight(unsigned int height);
    void SetTileSize(sf::Vector2f tileSize);
    void SetBegin(sf::Vector2f begin);
    void ChangeLocation();
    void MoveMap(float x, float y);

    void ChangeTile(Tile *, Tile *);

    auto CreateGrassTile(sf::Vector2f coordinates) -> TerrainTile *;
    auto CreateWaterTile(sf::Vector2f coordinates) -> TerrainTile *;
    auto CreateWaterDownTile(sf::Vector2f coordinates) -> TerrainTile *;
    auto CreateCanalTile(sf::Vector2f coordinates) -> TerrainTile *;
    auto CreateTreeTile(sf::Vector2f coordinates) -> TerrainTile *;
    auto CreateResidentialTile(sf::Vector2f coordinates) -> BuildingTile *;
    auto CreateIndustrialTile(sf::Vector2f coordinates) -> BuildingTile *;
    auto CreateRoadTile(sf::Vector2f coordinates, RoadType roadType) -> RoadTile *;

  public:
    unsigned int mWidth;
    unsigned int mHeight;

    std::vector<Tile *> mTiles;

    sf::Vector2f mBegin;
    sf::Vector2f mTileSize;

    Attributes mResidentialAttributes;
    Attributes mIndustrialAttributes;
    Attributes mTreeAttributes;
    Attributes mCanalAttributes;
    Attributes mWaterDownAttributes;
    Attributes mGrassAttributes;
    Attributes mWaterAttributes;
    Attributes mRoadAttributes;

    void SetActive(bool b);
};
}; // namespace gg
