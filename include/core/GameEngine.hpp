#pragma once

#include "Event.hpp"
#include "GameEngine.hpp"
#include "gamestates/GameState.hpp"
#include <SFML/Graphics/Drawable.hpp>

namespace gg
{

class GameState;

class GameEngine
{
  private:
    static std::stack<GameState *> mStates;

  public:
    /**
     * @brief Initializes game engine
     * 
     */
    static void Init();
    /**
     * @brief Deinitializes game engine
     * 
     */
    static void Destroy();

    /**
     * @brief Remove current state and change it to state provided as the argument
     * 
     * @param state 
     */
    static void ChangeState(GameState *state);
    /**
     * @brief Pushes state on top of the stack of states and makes it an active state
     * 
     * @param state 
     */
    static void PushState(GameState *state);
    /**
     * @brief Removes current state
     * 
     */
    static void PopState();
  
    /**
     * @brief Returns state that is currently active
     * 
     * @return GameState* 
     */
    static auto CurrentState() -> GameState *;

    /**
     * @brief Returns state that was active before currently active state
     * 
     * @return GameState* 
     */
    static auto PreviousState() -> GameState *;

    static void Update();
    static void Render(sf::RenderTarget &target, sf::RenderStates states);
};

} // namespace gg