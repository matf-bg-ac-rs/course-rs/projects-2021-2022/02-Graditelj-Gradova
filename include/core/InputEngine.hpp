#pragma once

#include <SFML/Graphics.hpp>

namespace gg
{

class InputEngine
{

  public:
    static auto IsSpriteClicked(
        sf::Sprite object, sf::Mouse::Button button, sf::RenderWindow &window) -> bool;

    static void IsSpriteHovered(sf::Sprite object, sf::RenderWindow &window);

    static auto IsKeyPressed(sf::Keyboard::Key key) -> bool;

    static auto GetMousePosition() -> sf::Vector2f;

    static void PullEvents();

  private:
    InputEngine();
    ~InputEngine();
};
}; // namespace gg
