#pragma once

#define GG_WINDOW_WIDTH (1200)
#define GG_WINDOW_HEIGHT (800)
#define GG_FULLSCREEN_WINDOW_WIDTH (sf::VideoMode::getDesktopMode().width)
#define GG_FULLSCREEN_WINDOW_HEIGHT (sf::VideoMode::getDesktopMode().height)
#define GG_WINDOW_TITLE (std::string("Graditelj Gradova"))
#define GG_WINDOW_VERTICAL_SYNC

#define GG_RESOURCES_PATH (std::string("resources/"))
#define GG_RESOURCES_FONTS_FILENAME (std::string("Fonts.xml"))
#define GG_RESOURCES_SCENES_FILENAME (std::string("Scenes.xml"))
#define GG_RESOURCES_STYLE_FILENAME (std::string("Style.xml"))
#define GG_RESOURCES_TEXTURES_FILENAME (std::string("Textures.xml"))
#define GG_RESOURCES_IMAGES_FILENAME (std::string("Images.xml"))
#define GG_RESOURCES_MUSIC (std::string("music/music1.wav"))
#define GG_RESOURCES_MAP_DEFAULT (std::string("maps/DefaultMap.xml"))
#define GG_RESOURCES_SAVE_POZAREVAC (std::string("saves/Pozarevac.xml"))
#define GG_RESOURCES_SAVE_TRSTENIK (std::string("saves/Trstenik.xml"))
#define GG_RESOURCES_SAVE_UB (std::string("saves/Ub.xml"))
#define GG_RESOURCES_SAVE_BLACE (std::string("saves/Blace.xml"))
#define GG_RESOURCES_SAVE_BEOGRAD (std::string("saves/Beograd.xml"))

// controls
#define GG_SAVE_GAME                                                           \
    ((sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) &&                    \
      sf::Keyboard::isKeyPressed(sf::Keyboard::S)))
#define GG_ROTATE_TILE (sf::Keyboard::R)
#define GG_DEMOLISH_TILE (sf::Keyboard::Space)
#define GG_PLACE_TILE (sf::Mouse::Right)
#define GG_PAUSE_GAME (sf::Keyboard::Escape)

#define GG_MOVE_CAMERA_UP (sf::Keyboard::W)
#define GG_MOVE_CAMERA_LEFT (sf::Keyboard::A)
#define GG_MOVE_CAMERA_DOWN (sf::Keyboard::S)
#define GG_MOVE_CAMERA_RIGHT (sf::Keyboard::D)

#define GG_MOVE_CAMERA_DIAGONALLY_DOWN_LEFT                                    \
    ((sf::Keyboard::isKeyPressed(GG_MOVE_CAMERA_DOWN) &&                       \
      sf::Keyboard::isKeyPressed(GG_MOVE_CAMERA_LEFT)))
#define GG_MOVE_CAMERA_DIAGONALLY_DOWN_RIGHT                                   \
    ((sf::Keyboard::isKeyPressed(GG_MOVE_CAMERA_DOWN) &&                       \
      sf::Keyboard::isKeyPressed(GG_MOVE_CAMERA_RIGHT)))
#define GG_MOVE_CAMERA_DIAGONALLY_UP_LEFT                                      \
    ((sf::Keyboard::isKeyPressed(GG_MOVE_CAMERA_UP) &&                         \
      sf::Keyboard::isKeyPressed(GG_MOVE_CAMERA_LEFT)))
#define GG_MOVE_CAMERA_DIAGONALLY_UP_RIGHT                                     \
    ((sf::Keyboard::isKeyPressed(GG_MOVE_CAMERA_UP) &&                         \
      sf::Keyboard::isKeyPressed(GG_MOVE_CAMERA_RIGHT)))

//#define GG_SHOW_FPS

#define GG_TEST_SCENE_NODE

//#define GG_DEBUG
