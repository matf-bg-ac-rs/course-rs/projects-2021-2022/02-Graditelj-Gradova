#pragma once

#include "../events/WindowEvent.hpp"
#include "Event.hpp"
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <gamestates/Gameplay.hpp>
#include <gui/Scene.hpp>

namespace gg
{

class Application : public IEventSubscriber
{

  private:
    void OnEvent(IEvent *e) override;
    void OnWindowResizeEvent(IEvent *);
    void OnWindowCloseEvent(IEvent *);
    void OnKeyPressedEvent(IEvent *);
    void OnKeyReleasedEvent(IEvent *);

  public:
    Application();
    ~Application() override;

  public:
    Application(Application &) = delete;
    void operator=(const Application &) = delete;

  public:
    void Run();

    auto GetView() -> sf::View { return mWindow->getView(); };
    auto GetWindow() -> std::shared_ptr<sf::RenderWindow> { return mWindow; }

    void ToggleFullscreen(bool);
    void ToggleMusic(bool);

    static auto GetInstance() -> Application * { return sInstance; }

  private:
    static Application *sInstance;
    std::shared_ptr<sf::RenderWindow> mWindow;
    std::shared_ptr<sf::Music> mMusic;
    bool mRunning;
};

}; // namespace gg
