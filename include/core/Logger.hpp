#pragma once

#include "spdlog/fmt/ostr.h"
#include "spdlog/spdlog.h"

namespace gg
{

class Logger
{
  public:
    static void Init();

    static std::shared_ptr<spdlog::logger> &GetCoreLogger()
    {
        return s_coreLogger;
    };
    static std::shared_ptr<spdlog::logger> &GetDebugLogger()
    {
        return s_debugLogger;
    };

  private:
    static std::shared_ptr<spdlog::logger> s_coreLogger;
    static std::shared_ptr<spdlog::logger> s_debugLogger;
};

#define GG_BREAK __builtin_trap(); // TODO: Make this platform specific

#define GG_TRACE(...) ::gg::Logger::GetCoreLogger()->trace(__VA_ARGS__)
#define GG_INFO(...) ::gg::Logger::GetCoreLogger()->info(__VA_ARGS__)
#define GG_ERROR(...) ::gg::Logger::GetCoreLogger()->error(__VA_ARGS__)
#define GG_WARN(...) ::gg::Logger::GetCoreLogger()->warn(__VA_ARGS__)
#define GG_CRITICAL(...) ::gg::Logger::GetCoreLogger()->critical(__VA_ARGS__)

#define GG_ASSERT(x, msg)                                                      \
    if ((x)) {                                                                 \
    } else {                                                                   \
        GG_CRITICAL(                                                           \
            "Asset: {}\n\tMessage: {}\n\tFile: {}: {}",                        \
            #x,                                                                \
            msg,                                                               \
            __FILE__,                                                          \
            __LINE__);                                                         \
        exit(EXIT_FAILURE);                                                    \
    }

#ifdef GG_DEBUG

#define GG_DEBUG_TRACE(...) ::gg::Logger::GetDebugLogger()->trace(__VA_ARGS__)
#define GG_DEBUG_INFO(...) ::gg::Logger::GetDebugLogger()->info(__VA_ARGS__)
#define GG_DEBUG_ERROR(...) ::gg::Logger::GetDebugLogger()->error(__VA_ARGS__)
#define GG_DEBUG_WARN(...) ::gg::Logger::GetDebugLogger()->warn(__VA_ARGS__)
#define GG_DEBUG_CRITICAL(...)                                                 \
    ::gg::Logger::GetDebugLogger()->critical(__VA_ARGS__)

#define GG_DEBUG_ASSERT(x, msg)                                                \
    if ((x)) {                                                                 \
    } else {                                                                   \
        GG_DEBUG_CRITICAL(                                                     \
            "Asset: {}\n\tMessage: {}\n\tFile: {}: {}",                        \
            #x,                                                                \
            msg,                                                               \
            __FILE__,                                                          \
            __LINE__);                                                         \
        GG_BREAK;                                                              \
    }

#else

#define GG_DEBUG_TRACE(...)
#define GG_DEBUG_INFO(...)
#define GG_DEBUG_ERROR(...)
#define GG_DEBUG_WARN(...)
#define GG_DEBUG_CRITICAL(...)

#define GG_DEBUG_ASSERT(x, msg)

#endif // GG_DEBUG

}; // namespace gg
