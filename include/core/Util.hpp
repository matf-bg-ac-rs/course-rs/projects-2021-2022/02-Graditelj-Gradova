#pragma once

#include <SFML/Graphics.hpp>

namespace gg
{

class Util
{
  public:
    Util() = delete;
    ~Util() = delete;

    static auto TriangleArea(
        const sf::Vector2f &A, const sf::Vector2f &B, const sf::Vector2f &C) -> double;
    static auto RectangleArea(
        const sf::Vector2f &A,
        const sf::Vector2f &B,
        const sf::Vector2f &C,
        const sf::Vector2f &D) -> double;
};
}; // namespace gg
