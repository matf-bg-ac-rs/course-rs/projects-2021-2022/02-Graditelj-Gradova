#pragma once
#include <unordered_map>
#include <vector>

#define BIT_FIELD(x) (1 << x)

namespace gg
{
class EventDispatcher;

enum class EventType {
    None = 0,
    // Window Events
    WindowResize,
    WindowClose,
    // Input Events
    MouseMoved,
    MousePressed,
    MouseReleased,
    KeyboardPressed,
    KeyboardReleased,
    // Scene events
    ButtonClicked,
    ToggleButtonClicked,
    TextInserted,
    // Time events
    TimePassed,
    TestEvent
};

enum class EventCategory {
    None = 0,
    EventBuilding = BIT_FIELD(0),   // we use bit field to specify events that
    EventDestroying = BIT_FIELD(1), // can belong to multiple categories
    EventTerraforming = BIT_FIELD(2),
    Window = BIT_FIELD(3)

};

class IEvent
{
  public:
    virtual ~IEvent() = default;

    [[nodiscard]] inline virtual auto GetEventType() const -> EventType = 0;
    // virtual EventCategory GetEventCategoryFlags() const = 0;

    // inline bool IsInCategory(EventCategory category) const
    //{
    //	// operator '&' is not defined for EventCategory class so this
    // conversion is needed 	return
    // static_cast<long>(GetEventCategoryFlags()) & static_cast<long>(category);
    //}
  protected:
    IEvent() = default;
};

class IEventSubscriber
{
  public:
    IEventSubscriber() = default;

    virtual ~IEventSubscriber() = default;
    auto operator==(const IEventSubscriber &) -> bool;

    virtual void OnEvent(IEvent *e)
    {
        GG_INFO("Implement OnEvent in your derived class!");
    }

    [[nodiscard]] auto GetNumberOfSubscribedEvents() const -> size_t;

    void IncrementNumberOfSubscribedEvents();

    void DecrementNumberOfSubscribedEvents();

    void ResetNumberOfSubscribedEvents();

  private:
    size_t mNumOfSubscribedEvents = 0;
};
} // namespace gg
namespace std
{
}

namespace gg
{
class EventDispatcher
{
  public:
    static auto GetInstance() -> EventDispatcher &
    {
        static EventDispatcher sInstance;
        return sInstance;
    }

    // should not need destructor as it will be alive during whole runtime

    // better to keep deleted operators public (see Effective Modern C++)
    EventDispatcher(EventDispatcher &&) = delete;
    EventDispatcher(const EventDispatcher &) = delete;
    auto operator=(const EventDispatcher &) -> EventDispatcher & = delete;
    auto operator=(EventDispatcher &&) -> EventDispatcher & = delete;

    static void Subscribe(IEventSubscriber *s, const EventType e);
    static void Unsubscribe(IEventSubscriber *s, const EventType e);
    // could be a private method, but we want to allow subscriber to remove
    // itself in one go, instead only when it does not have any monitoring
    // events
    static void RemoveSubscriber(IEventSubscriber *s);

    static void NotifySubscribers(IEvent *e);

    static auto NumberOfSubscribers() -> int;
    friend bool CheckSubscriberExistence(IEventSubscriber* e);

  private:
    EventDispatcher() = default;

    // Pointer on the first is required because we are dealing with an abstract
    // class Without it, map would assume that all our derived classes are this
    // abstract class
    std::unordered_map<IEventSubscriber *, std::vector<EventType>>
        mSubscribers;
};

    bool CheckSubscriberExistence(IEventSubscriber* e);

} // namespace gg
