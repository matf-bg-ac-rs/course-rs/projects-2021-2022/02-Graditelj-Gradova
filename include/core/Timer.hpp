#include <SFML/System/Clock.hpp>
#include <core/GameEngine.hpp>

namespace gg
{
class Timer
{

  public:
    Timer();
    ~Timer();

    void Update();

  private:
    unsigned mCurrentTimeInSeconds;
    sf::Clock mClock;
};
} // namespace gg
