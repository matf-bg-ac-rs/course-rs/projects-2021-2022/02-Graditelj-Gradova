#pragma once

#include <core/Event.hpp>
#include <core/GameEngine.hpp>
#include <gui/Scene.hpp>
#include <resources/SceneManager.hpp>

namespace gg
{

class GameState : public IEventSubscriber, public sf::Drawable
{
  public:
    GameState(SceneType);
    ~GameState() override = default;

    virtual void Enter();
    virtual void Exit();

    virtual void Update() = 0;

    auto GetSceneType() -> SceneType;

  protected:
    void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

  protected:
    SceneType m_sceneType = SceneType::Custom;
};

}; // namespace gg
