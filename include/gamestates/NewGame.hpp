#pragma once

#include "GameState.hpp"
#include <events/SceneEvent.hpp>

namespace gg
{

class NewGame : public GameState
{

  public:
    NewGame();
    ~NewGame() override;

    void Enter() override;
    void Exit() override;

    void OnEvent(IEvent *e) override;
    void Update() override;

    void OnSelectedGameEvent(const std::string &);
    void OnBackPressedEvent();

  private:
};

}; // namespace gg
