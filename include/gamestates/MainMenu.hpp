#pragma once

#include "GameState.hpp"
#include <events/SceneEvent.hpp>

namespace gg
{

class MainMenu : public GameState
{

  public:
    MainMenu();
    ~MainMenu() override;

    void Enter() override;
    void Exit() override;

    void OnEvent(IEvent *e) override;
    void Update() override;

    void OnNewGamePressedEvent();
    void OnLoadGamePressedEvent();
    void OnSettingsPressedEvent();
    void OnExitPressedEvent();

  private:
};

}; // namespace gg
