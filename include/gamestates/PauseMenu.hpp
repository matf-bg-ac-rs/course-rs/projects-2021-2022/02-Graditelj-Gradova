#pragma once
#include "GameState.hpp"

namespace gg
{

class PauseMenu : public GameState
{
  public:
    PauseMenu();
    ~PauseMenu() override;

    void Enter() override;
    void Exit() override;

    void OnEvent(IEvent *e) override;
    void Update() override;

  protected:
    void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

  private:
    void OnResumeEvent();
    void OnSaveEvent();
    void OnQuitEvent();
};
}; // namespace gg
