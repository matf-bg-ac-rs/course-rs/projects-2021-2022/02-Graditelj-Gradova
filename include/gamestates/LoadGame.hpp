#pragma once

#include "GameState.hpp"
#include <events/SceneEvent.hpp>

namespace gg
{

class LoadGame : public GameState
{
  public:
    LoadGame();
    ~LoadGame() override;

    void Enter() override;
    void Exit() override;

    void OnEvent(IEvent *e) override;
    void Update() override;

    void OnSelectedGameEvent(const std::string &);
    void OnBackPressedEvent();
};

}; // namespace gg
