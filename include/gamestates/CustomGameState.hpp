#pragma once

#include "gamestates/GameState.hpp"

namespace gg
{

class CustomGameState : public GameState
{

  public:
    CustomGameState();
    ~CustomGameState() override;

    void Enter() override;

    void OnEvent(IEvent *e) override;
    void Update() override;

  private:
};

}; // namespace gg
