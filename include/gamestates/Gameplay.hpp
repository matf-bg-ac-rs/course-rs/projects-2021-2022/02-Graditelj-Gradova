#pragma once
#include "GameState.hpp"
#include "core/Timer.hpp"
#include <core/Application.hpp>
#include <core/Map.hpp>
#include <events/SceneEvent.hpp>

namespace gg
{

class Gameplay : public GameState
{
  public:
    Gameplay(std::string );
    ~Gameplay() override;

    void Enter() override;
    void Exit() override;

    void OnEvent(IEvent *e) override;
    void Update() override;

    void NewGame();
    void LoadGame();
    void SaveGame();

    auto GetMap() -> Map *;
    void Build(Tile *);
    void Demolish(Tile *tile);
    void SetOrientation(Tile *);

    void IncreaseTaxFactor();
    void DecreaseTaxFactor();

    void MapNavigate();

    void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

  private:
    void OnPauseEvent();
    void OnMinutePassedEvent();
    void OnTenSecondsPassedEvent();
    void OnControlsMenuEvent();

    void OnMoveCameraUp();
    void OnMoveCameraDown();
    void OnMoveCameraLeft();
    void OnMoveCameraRight();
    void OnMoveDiagDownLeft();
    void OnMoveDiagDownRight();
    void OnMoveDiagUpLeft();
    void OnMoveDiagUpRight();

    void DecreaseBudget(unsigned);
    void IncreaseBudget(unsigned);
    void IncreasePopulation(unsigned);
    void DecreasePopulation(unsigned);
    void DecreasePotentialProfit(unsigned);
    void IncreasePotentialProfit(unsigned);
    void CollectTaxes();
    void CollectProfits();

    void BuildResidential(Tile *);
    void BuildIndustrial(Tile *);
    void BuildNature(Tile *, TerrainType);
    void BuildRoad(Tile *, RoadType);
    void BuildBridge(Tile *, RoadType);

    void UpdatePopulationLabel();
    void UpdateBudgetLabel();
    void UpdatePotentialProfitLabel();
    void UpdateInfo(const std::string &, sf::Color);

  private:
    bool mZoom = false;
    Map *mMap = nullptr;
    std::string mCityName;
    std::string mSaveFilePath;
    unsigned mCityPopulation;
    unsigned mCityBudget;
    unsigned mTaxFactor = 1;
    unsigned mPotentialProfit = 0;
    unsigned int mNumberOfIndustrial = 0;
    Timer mTimer;
    bool mDirty = true;
};

}; // namespace gg
