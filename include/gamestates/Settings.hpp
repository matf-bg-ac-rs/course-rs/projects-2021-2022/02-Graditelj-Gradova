#pragma once

#include "GameState.hpp"

namespace gg
{

class Settings : public GameState
{
  public:
    Settings();
    ~Settings() override;

    void Enter() override;
    void Exit() override;

    void OnEvent(IEvent *e) override;
    void Update() override;

    void OnFullscreenEvent(const bool);
    void OnMusicSetEvent(const bool);
    void OnBackPressedEvent();

  private:
};
}; // namespace gg
