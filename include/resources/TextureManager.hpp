#pragma once

#include <SFML/Graphics.hpp>
#include <map>
#include <string>
#include <tile/BuildingTile.hpp>
#include <tile/RoadTile.hpp>
#include <tile/TerrainTile.hpp>

namespace gg
{

class TextureManager
{
  private:
    static std::map<std::string, sf::Texture *> sTextureLibrary;
    static bool sInitialized;

  public:
    static void Init();
    static void Destroy();
    static auto GetInitialized() -> bool;
    static auto GetNumberOfTexures() -> unsigned;

    static void
    AddTexture(const std::string &name, const std::string &filename);
    static auto GetTexture(const std::string &texture) -> sf::Texture *;

    TextureManager() = delete;
    ~TextureManager() = delete;
};

}; // namespace gg
