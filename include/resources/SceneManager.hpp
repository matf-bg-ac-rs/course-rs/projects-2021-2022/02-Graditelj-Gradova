#pragma once

#include <gui/Scene.hpp>

namespace gg
{

enum class SceneType {
    Custom = 0,
    MainMenu,
    NewGame,
    LoadGame,
    Settings,
    Gameplay,
    PauseMenu,
    ControlsMenu
};

class SceneManager
{
  public:
    static void Init();
    static void Destroy();

    static auto GetScene(const SceneType &) -> Scene *;
    static void AddScene(const SceneType &, Scene *scene);

    static auto SceneTypeToString(SceneType) -> const std::string;
    static auto StringToSceneType(const std::string &) -> SceneType;

    SceneManager() = delete;
    ~SceneManager() = delete;

  private:
    static bool sInitialized;
    static std::vector<Scene *> sSceneLibrary;

    static const int NUM_OF_SCENES = 8; // Change if new SceneType added/deleted
};

}; // namespace gg
