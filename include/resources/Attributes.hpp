#pragma once

#include <SFML/Graphics.hpp>

namespace gg
{

enum class HAlignment;
enum class VAlignment;
struct Margins;
class XMLDocument;

class Attributes
{
  public:
    Attributes() = default;
    virtual ~Attributes() = default;

    void Add(const std::string &, const std::string &);
    [[nodiscard]] auto Has(const std::string &) const -> bool;
    [[nodiscard]] auto Get(const std::string &) const -> std::string;

    template <typename T> auto Get(const std::string &) const -> T;

    [[nodiscard]] auto GetLibrary() const -> const std::map<std::string, std::string>;
    inline void SetLibrary(std::map<std::string, std::string> &new_attr_library)
    {
        mAttributeLibrary = new_attr_library;
    }

  private:
    std::map<std::string, std::string> mAttributeLibrary;
};

template <> [[nodiscard]] auto Attributes::Get(const std::string &) const -> bool;

template <> [[nodiscard]] auto Attributes::Get(const std::string &) const -> int;

template <> [[nodiscard]] auto Attributes::Get(const std::string &) const -> unsigned int;

template <> [[nodiscard]] auto Attributes::Get(const std::string &) const -> float;

template <> [[nodiscard]] auto Attributes::Get(const std::string &) const -> double;

template <> [[nodiscard]] auto Attributes::Get(const std::string &) const -> sf::Vector2i;

template <> [[nodiscard]] auto Attributes::Get(const std::string &) const -> sf::Vector2f;

template <> [[nodiscard]] auto Attributes::Get(const std::string &) const -> sf::Color;

template <> [[nodiscard]] auto Attributes::Get(const std::string &) const -> const sf::Font &;

template <> [[nodiscard]] auto Attributes::Get(const std::string &) const -> VAlignment;

template <> [[nodiscard]] auto Attributes::Get(const std::string &) const -> HAlignment;

template <> [[nodiscard]] auto Attributes::Get(const std::string &) const -> Margins;

template <> [[nodiscard]] auto Attributes::Get(const std::string &) const -> XMLDocument *;

}; // namespace gg
