#pragma once

#include "../../libs/tinyxml2/tinyxml2.h"
#include "Attributes.hpp"

namespace gg
{

class XMLDocument
{
  public:
    XMLDocument() = default;
    XMLDocument(std::string name, Attributes attributes, std::string content);
    XMLDocument(
        std::string name,
        Attributes attributes,
        std::string content,
        std::vector<XMLDocument> children);
    virtual ~XMLDocument() = default;

    void AddChild(XMLDocument);
    void RemoveChild(int i);

    [[nodiscard]] auto GetName() const -> const std::string &;
    [[nodiscard]] auto GetAttributes() const -> const Attributes &;
    [[nodiscard]] auto GetContent() const -> const std::string &;
    [[nodiscard]] auto GetChildren() const -> const std::vector<XMLDocument> &;

    auto GetChildren() -> std::vector<XMLDocument> &;
    static auto LoadFromFile(const std::string &) -> XMLDocument;
    static void SaveToFile(const XMLDocument &, const std::string &);

  private:
    static auto LoadFromElement(tinyxml2::XMLElement *) -> XMLDocument;
    static auto LoadAttributesFromElement(tinyxml2::XMLElement *) -> Attributes;
    static auto
    CreateElement(tinyxml2::XMLDocument &, const XMLDocument &) -> tinyxml2::XMLElement *;

  private:
    std::string mName;
    Attributes mAttributes;
    std::string mContent;
    std::vector<XMLDocument> mChildren;
};

}; // namespace gg
