#pragma once

#include "XMLDocument.hpp"

namespace gg
{

class StyleManager
{
  public:
    static void Init();
    static void Destroy();


    static auto GetInitialized() -> bool;
    static auto GetNumberOfStyles() -> unsigned;

    static auto GetStyle(const std::string&) -> XMLDocument *;
    static void AddStyle(const std::string&, XMLDocument);

    StyleManager() = delete;
    virtual ~StyleManager() = delete;

  private:
    static bool sInitialized;
    static std::map<std::string, XMLDocument *> sStyleLibrary;
};

}; // namespace gg
