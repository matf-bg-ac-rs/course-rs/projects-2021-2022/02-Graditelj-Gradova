#pragma once

#include <SFML/Graphics/Font.hpp>

namespace gg
{

class FontManager
{
  public:
    static void Init();
    static void Destroy();

    static auto GetFont(const std::string &fontname) -> sf::Font *;
    static void
    AddFont(const std::string &fontname, const std::string &filename);
    static auto Size() -> std::size_t;

    FontManager() = delete;
    ~FontManager() = delete;

  private:
    static bool sInitialized;
    static std::map<std::string, sf::Font *> sFontLibrary;
};

}; // namespace gg
