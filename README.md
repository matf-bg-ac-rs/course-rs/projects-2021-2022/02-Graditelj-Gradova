# Project Graditelj-Gradova

Graditelj Gradova is an interactive video game which simulates building of bussines and residential structures and also road infrastructure of city. It is sandbox which means it has no final goal and user has complete freedom to design and build his own city. It's currently supported on linux only.

## Developers

- [Andrija Urosevic, 83/2018](https://gitlab.com/AndrijaAda99)
- [Stefan Jancic, 219/2018](https://gitlab.com/Voldil)
- [Bojan Bardzic, 300/2018](https://gitlab.com/BojanBardzic)
- [Igor Paunovic, 185/2018](https://gitlab.com/Idzii)
- [Dimitrije Stankov, 307/2017](https://gitlab.com/ginger-with-a-soul)

## Demo video
- [Demo video(Serbian)](https://drive.google.com/file/d/16deIhXKMNg49i1lMhWvcI1t-jbq5_yAw/view?usp=sharing)

## Libraries used

- [SFML](https://www.sfml-dev.org/)
- [spdlog](https://github.com/gabime/spdlog)
- [tinyxml2](https://github.com/leethomason/tinyxml2)

## How to run

- Clone with option `--recursive`
```
git clone --recursive https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/02-Graditelj-Gradova.git
```

- Install SFML

```
sudo apt-get install libsfml-dev (ubuntu)
or
sudo pacman -S sfml (arch)
```

- Install spdlog

```
sudo apt-get install libspdlog-dev (ubuntu)
or
sudo pacman -S spdlog (arch)
```

- Install tinyxml2

```
sudo pacman -S tinyxml (ubuntu)
or 
sudo apt-get install libtinyxml2-dev (arch)
```

- Install clang
```
sudo apt -y install clang (ubuntu)
or
sudo pacman -S clang (arch)
```

- Build with CMake and run
```
cd 02-GraditeljGradova
mkdir build && cd build
cmake ..
make
./GraditeljGradova`
```
