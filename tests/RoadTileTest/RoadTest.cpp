#include <catch2/catch_test_macros.hpp>

#include<tile/RoadTile.hpp>
#include<resources/Attributes.hpp>

#include<SFML/Graphics.hpp>

gg::RoadTile makeTestTile(gg::RoadType type) {
	std::string typeStr = gg::RoadTile::RoadTypeToString(type);

	gg::Attributes attributes;
	attributes.Add("coordinates", "1.0 1.0");
	attributes.Add("road_type", typeStr);
	attributes.Add("cost", "20");
	attributes.Add("rotation", "0");

	return new gg::RoadTile(attributes);
}

TEST_CASE("Road tile testing!", "[class]") {

	SECTION("SetType changes the road type") {
		gg::RoadTile tile = makeTestTile(gg::RoadType::STRAIGHT);

		gg::RoadType oldType = tile.GetType();
		tile.SetType(gg::RoadType::CROSS);
		gg::RoadType newType = tile.GetType();

		REQUIRE_FALSE(oldType == newType);
	}

	SECTION("GetAttributes returns following attributes: type, road_type, cost and rotation") {
		gg::RoadTile tile = makeTestTile(gg::RoadType::STRAIGHT);

		gg::Attributes result = tile.GetAttributes();

		bool hasType = result.Has("type");
		bool hasRoadType = result.Has("road_type");
		bool hasCost = result.Has("cost");
		bool hasRotation = result.Has("rotation");

		REQUIRE(hasType == true);
		REQUIRE(hasRoadType == true);
		REQUIRE(hasCost == true);
		REQUIRE(hasRotation == true);
	}

	SECTION("GetTileType returns Road type") {
		gg::RoadTile tile = makeTestTile(gg::RoadType::STRAIGHT);

		gg::TileType expected = gg::TileType::Road;
		gg::TileType result = tile.GetTileType();

		REQUIRE(result == expected);
	}

	SECTION("RoadTypeToString returns \"cross\" for road type cross") {
		gg::RoadType input = gg::RoadType::CROSS;

		std::string expected = "cross";
		std::string result = gg::RoadTile::RoadTypeToString(input);

		REQUIRE(result == expected);
	}

	SECTION("StringToRoadType returns road type straight for \"straight\" ") {
		std::string input = "straight";

		gg::RoadType expected = gg::RoadType::STRAIGHT;
		gg::RoadType result = gg::RoadTile::StringToRoadType(input);

		REQUIRE(result == expected);
	}

	SECTION("Align actually moves a RoadTile") {
		gg::RoadTile tile = makeTestTile(gg::RoadType::CROSS);

		sf::Vector2f oldCoordinates = tile.GetCoordinates();
		tile.Align();
		sf::Vector2f newCoordinates = tile.GetCoordinates();

		REQUIRE_FALSE(oldCoordinates == newCoordinates);
	}
}