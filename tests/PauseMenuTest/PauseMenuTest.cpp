#include <catch2/catch_test_macros.hpp>

#include "../../include/gamestates/PauseMenu.hpp"

TEST_CASE("Pause menu state testing!", "[class]")
{
    SECTION("Enter test"){
        //Arrange
        auto pauseMenu = new gg::PauseMenu();
        gg::XMLDocument pause_scene_doc = gg::XMLDocument::LoadFromFile("resources/scenes/PauseMenuScene.xml");
        auto pauseScene = new gg::Scene("PauseMenu", pause_scene_doc);
//        gg::SceneManager::AddScene(gg::SceneType::PauseMenu, pauseScene);
//        pauseMenu->Enter();

        //Act

        //Assert
        REQUIRE(pauseMenu != nullptr);
    }
}
