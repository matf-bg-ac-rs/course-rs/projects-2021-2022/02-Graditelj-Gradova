#include <catch2/catch_test_macros.hpp>

#include "../../include/resources/FontMenager.hpp"

TEST_CASE("FontManager testing!", "[class]")
{
    SECTION("Empty font library") { REQUIRE(gg::FontManager::Size() == 0); }

    SECTION("Adding fonts to font library")
    {
        auto size_before = gg::FontManager::Size();
        gg::FontManager::AddFont("font1", "resources/fonts/Anton-Regular.ttf");
        auto size_after = gg::FontManager::Size();

        REQUIRE(size_before == size_after - 1);
    }

    SECTION("Getting fonts from font library")
    {
        auto size_before = gg::FontManager::Size();
        gg::FontManager::GetFont("font1");
        auto size_after = gg::FontManager::Size();

        REQUIRE(size_before == size_after);
    }
}
