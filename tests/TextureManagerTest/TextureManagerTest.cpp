#include <catch2/catch_test_macros.hpp>

#include "../../include/resources/TextureManager.hpp"

TEST_CASE("Texture manager testing!", "[class]")
{
    SECTION("Init test")
    {
        // Arrange
        gg::TextureManager::Init();

        // Act
        bool initialized = gg::TextureManager::GetInitialized();

        // Assert
        REQUIRE(initialized == true);
    }

    SECTION("Adding and getting texture test")
    {
        // Arrange
        gg::TextureManager::Destroy();
        unsigned numberOfTexturesBefore =
            gg::TextureManager::GetNumberOfTexures();

        // Act
        gg::TextureManager::AddTexture(
            "water.png", "resources/textures/water.png");
        auto texture = gg::TextureManager::GetTexture("water.png");
        unsigned numberOfTexturesAfter =
            gg::TextureManager::GetNumberOfTexures();

        // Assert
        REQUIRE(texture != nullptr);
        REQUIRE(numberOfTexturesBefore + 1 == numberOfTexturesAfter);
    }

    SECTION("Texture manager Destroy test")
    {
        // Arrange
        gg::TextureManager::Init();

        // Act
        gg::TextureManager::Destroy();
        bool initialized = gg::TextureManager::GetInitialized();

        // Assert
        REQUIRE(initialized == false);
    }
}
