#include <catch2/catch_test_macros.hpp>

#include <gui/Box.hpp>
#include <resources/Attributes.hpp>

#include <SFML/Graphics.hpp>

TEST_CASE("Attribute testing!", "[class]")
{
    SECTION("Adding attributes [std::string]")
    {
        gg::Attributes attributes;
        std::string key = "key";
        std::string value = "value";

        attributes.Add(key, value);

        REQUIRE(attributes.Has(key));
    }

    SECTION("Adding and getting attributes [std::string]")
    {
        gg::Attributes attributes;
        std::string key = "key";
        std::string value = "value";

        attributes.Add(key, value);

        REQUIRE(attributes.Get(key) == value);
    }

    SECTION("Adding and getting attributes [unsigned int]")
    {
        gg::Attributes attributes;
        std::string key = "key";
        unsigned int value = 1u;

        attributes.Add(key, std::to_string(value));

        REQUIRE(attributes.Get<unsigned int>(key) == value);
    }

    SECTION("Adding and getting attributes [int]")
    {
        gg::Attributes attributes;
        std::string key = "key";
        int value = 1;

        attributes.Add(key, std::to_string(value));

        REQUIRE(attributes.Get<int>(key) == value);
    }

    SECTION("Adding and getting attributes [bool]")
    {
        gg::Attributes attributes;
        std::string key = "key";
        bool value = true;

        attributes.Add(key, std::to_string(value));

        REQUIRE(attributes.Get<bool>(key) == value);
    }

    SECTION("Adding and getting attributes [float]")
    {
        gg::Attributes attributes;
        std::string key = "key";
        float value = 1.0f;

        attributes.Add(key, std::to_string(value));

        REQUIRE(attributes.Get<float>(key) == value);
    }

    SECTION("Adding and getting attributes [double]")
    {
        gg::Attributes attributes;
        std::string key = "key";
        double value = 1.0;

        attributes.Add(key, std::to_string(value));

        REQUIRE(attributes.Get<double>(key) == value);
    }

    SECTION("Adding and getting attributes [sf::Vector2i]")
    {
        gg::Attributes attributes;
        std::string key = "key";
        sf::Vector2i value(1, 1);

        attributes.Add(
            key, std::to_string(value.x) + " " + std::to_string(value.y));

        REQUIRE(attributes.Get<sf::Vector2i>(key).x == value.x);
        REQUIRE(attributes.Get<sf::Vector2i>(key).y == value.y);
    }

    SECTION("Adding and getting attributes [sf::Vector2f]")
    {
        gg::Attributes attributes;
        std::string key = "key";
        sf::Vector2f value(1.0f, 1.0f);

        attributes.Add(
            key, std::to_string(value.x) + " " + std::to_string(value.y));

        REQUIRE(attributes.Get<sf::Vector2f>(key).x == value.x);
        REQUIRE(attributes.Get<sf::Vector2f>(key).y == value.y);
    }

    SECTION("Adding and getting attributes [sf::Color]")
    {
        gg::Attributes attributes;
        std::string key1 = "key1";
        std::string key2 = "key2";
        sf::Color value = sf::Color::Red;

        attributes.Add(key1, "0xff 0x00 0x00 0xff");
        attributes.Add(key2, "0xff 0x00 0x00");

        REQUIRE(attributes.Get<sf::Color>(key1).r == value.r);
        REQUIRE(attributes.Get<sf::Color>(key1).g == value.g);
        REQUIRE(attributes.Get<sf::Color>(key1).b == value.b);
        REQUIRE(attributes.Get<sf::Color>(key1).a == value.a);

        REQUIRE(attributes.Get<sf::Color>(key2).r == value.r);
        REQUIRE(attributes.Get<sf::Color>(key2).g == value.g);
        REQUIRE(attributes.Get<sf::Color>(key2).b == value.b);
        REQUIRE(attributes.Get<sf::Color>(key2).a == value.a);
    }

    SECTION("Adding and getting attributes [HAlignment]")
    {
        gg::Attributes attributes;
        std::string key_left = "key1";
        std::string key_center = "key2";
        std::string key_right = "key3";

        gg::HAlignment value_left = gg::HAlignment::Left;
        gg::HAlignment value_center = gg::HAlignment::Center;
        gg::HAlignment value_right = gg::HAlignment::Right;

        attributes.Add(key_left, "Left");
        attributes.Add(key_center, "Center");
        attributes.Add(key_right, "Right");

        REQUIRE(attributes.Get<gg::HAlignment>(key_left) == value_left);
        REQUIRE(attributes.Get<gg::HAlignment>(key_center) == value_center);
        REQUIRE(attributes.Get<gg::HAlignment>(key_right) == value_right);
    }

    SECTION("Adding and getting attributes [VAlignment]")
    {
        gg::Attributes attributes;
        std::string key_top = "key1";
        std::string key_center = "key2";
        std::string key_bottom = "key3";

        gg::VAlignment value_top = gg::VAlignment::Top;
        gg::VAlignment value_center = gg::VAlignment::Center;
        gg::VAlignment value_bottom = gg::VAlignment::Bottom;

        attributes.Add(key_top, "Top");
        attributes.Add(key_center, "Center");
        attributes.Add(key_bottom, "Bottom");

        REQUIRE(attributes.Get<gg::VAlignment>(key_top) == value_top);
        REQUIRE(attributes.Get<gg::VAlignment>(key_center) == value_center);
        REQUIRE(attributes.Get<gg::VAlignment>(key_bottom) == value_bottom);
    }

    SECTION("Adding and getting attributes [Margins]")
    {
        gg::Attributes attributes;
        std::string key = "key";
        gg::Margins value{10, 10, 10, 10};

        attributes.Add(
            key,
            std::to_string(value.top) + " " + std::to_string(value.bottom) +
                " " + std::to_string(value.left) + " " +
                std::to_string(value.right));

        REQUIRE(attributes.Get<gg::Margins>(key).top == value.top);
        REQUIRE(attributes.Get<gg::Margins>(key).bottom == value.bottom);
        REQUIRE(attributes.Get<gg::Margins>(key).left == value.left);
        REQUIRE(attributes.Get<gg::Margins>(key).right == value.right);
    }

    SECTION("Adding and getting attributes [Margins]")
    {
        gg::Attributes attributes;
        std::string key = "key";
        gg::Margins value{10, 10, 10, 10};

        attributes.Add(
            key,
            std::to_string(value.top) + " " + std::to_string(value.bottom) +
                " " + std::to_string(value.left) + " " +
                std::to_string(value.right));

        REQUIRE(attributes.Get<gg::Margins>(key).top == value.top);
        REQUIRE(attributes.Get<gg::Margins>(key).bottom == value.bottom);
        REQUIRE(attributes.Get<gg::Margins>(key).left == value.left);
        REQUIRE(attributes.Get<gg::Margins>(key).right == value.right);
    }
}
