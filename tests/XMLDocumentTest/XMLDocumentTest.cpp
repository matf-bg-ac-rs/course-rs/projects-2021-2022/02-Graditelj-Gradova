#include <catch2/catch_test_macros.hpp>

#include <resources/Attributes.hpp>
#include <resources/XMLDocument.hpp>

TEST_CASE("XMLDocument test!", "[class]")
{
    SECTION("Empty XMLDocument")
    {
        gg::XMLDocument doc;

        REQUIRE(doc.GetName() == "");
        REQUIRE(doc.GetContent() == "");
        REQUIRE(doc.GetChildren().size() == 0);
    }

    SECTION("Simple XMLDocument")
    {
        std::string name = "name";
        gg::Attributes attributes;
        attributes.Add("key", "value");
        std::string content = "content";
        std::vector<gg::XMLDocument> children;
        children.push_back(gg::XMLDocument());
        children.push_back(gg::XMLDocument());

        gg::XMLDocument doc(name, attributes, "content", children);

        REQUIRE(doc.GetName() == name);
        REQUIRE(doc.GetAttributes().Get("key") == "value");
        REQUIRE(doc.GetContent() == content);
        REQUIRE(doc.GetChildren().size() == children.size());
    }

    SECTION("Adding child to XMLDocument")
    {
        gg::XMLDocument doc;
        int num_of_children = 10;

        for (int i = 0; i < num_of_children; i++) {
            doc.AddChild(gg::XMLDocument());
        }

        REQUIRE(doc.GetChildren().size() == num_of_children);
    }

    SECTION("Adding child to XMLDocument")
    {
        gg::XMLDocument doc;

        int num_of_added_children = 10;
        for (int i = 0; i < num_of_added_children; i++) {
            doc.AddChild(gg::XMLDocument());
        }

        int num_of_removed_children = 0;
        for (int i = 0; i < num_of_added_children; i++) {
            doc.RemoveChild(0);
            num_of_removed_children++;
        }

        REQUIRE(num_of_added_children == num_of_removed_children);
        REQUIRE(doc.GetChildren().size() == 0);
    }
}
