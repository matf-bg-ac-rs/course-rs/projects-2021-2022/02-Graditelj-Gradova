#include <catch2/catch_test_macros.hpp>

#include "../../include/resources/StyleManager.hpp"

TEST_CASE("Style manager testing!", "[class]")
{

    SECTION("Adding and getting style test"){
        //Arrange
        unsigned NumberOfStylesBefore = gg::StyleManager::GetNumberOfStyles();

        //Act
        gg::XMLDocument doc = gg::XMLDocument::LoadFromFile("resources/Style.xml");
        for (auto &style : doc.GetChildren()) {
            if(style.GetName() == "GameplayMenuButton")
                gg::StyleManager::AddStyle("GameplayMenuButton", style);
        }
        auto style = gg::StyleManager::GetStyle("GameplayMenuButton");
        unsigned NumberOfStylesAfter = gg::StyleManager::GetNumberOfStyles();

        //Assert
        REQUIRE(style != nullptr);
        REQUIRE(NumberOfStylesBefore+1 == NumberOfStylesAfter);
    }
}
