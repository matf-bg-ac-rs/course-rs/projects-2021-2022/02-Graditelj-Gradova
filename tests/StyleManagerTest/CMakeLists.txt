find_package(Catch2 3 REQUIRED)

if(NOT TARGET spdlog)
    find_package(spdlog REQUIRED)
endif()

if(NOT TARGET SFML)
    find_package(
        SFML 2.5
        COMPONENTS
            system
            window
            graphics
            network
            audio
        REQUIRED
    )
endif()

#if(NOT TARGET tinyxml2)
#    find_package(tinyxml2 REQUIRED)
#endif()

include_directories(../../include)

add_executable(
    test
    StyleManagerTest.cpp
    ../../libs/tinyxml2/tinyxml2.cpp
    ../../source/core/Application.cpp
    ../../source/core/InputEngine.cpp
    ../../source/core/Logger.cpp
    ../../source/core/GameEngine.cpp
    ../../source/core/Event.cpp
    ../../source/core/Util.cpp
    ../../source/gamestates/GameState.cpp
    ../../source/gamestates/MainMenu.cpp
    ../../source/gamestates/LoadGame.cpp
    ../../source/gamestates/NewGame.cpp
    ../../source/gamestates/Gameplay.cpp
    ../../source/gamestates/PauseMenu.cpp
    ../../source/gamestates/Settings.cpp
    ../../source/gamestates/ControlsMenu.cpp
    ../../source/gui/SceneNode.cpp
    ../../source/gui/Scene.cpp
    ../../source/gui/Box.cpp
    ../../source/gui/Label.cpp
    ../../source/gui/Button.cpp
    ../../source/gui/Image.cpp
    ../../source/gui/Container.cpp
    ../../source/gui/ToggleButton.cpp
    ../../source/gui/ToggleGroup.cpp
    ../../source/resources/FontManager.cpp
    ../../source/resources/SceneManager.cpp
    ../../source/resources/TextureManager.cpp
    ../../source/resources/StyleManager.cpp
    ../../source/resources/Attributes.cpp
    ../../source/resources/XMLDocument.cpp
    ../../source/tile/Tile.cpp
    ../../source/tile/TerrainTile.cpp
    ../../source/tile/BuildingTile.cpp
    ../../source/tile/RoadTile.cpp
    ../../source/events/WindowEvent.cpp
    ../../source/events/MouseEvent.cpp
    ../../source/events/KeyboardEvent.cpp
    ../../source/events/SceneEvent.cpp
    ../../source/core/Map.cpp
    ../../source/core/Timer.cpp
    ../../source/events/TimeEvent.cpp
)

target_precompile_headers(
    test
    PRIVATE
        <iostream>
        <stack>
        <vector>
        <array>
        <map>
        <set>
        <unordered_map>
        <unordered_set>
        <utility>
        <algorithm>
        <functional>
        <string>
        <sstream>
        <fstream>
        <memory>
        "../../include/core/Config.hpp"
        "../../include/core/Logger.hpp"
)



target_link_libraries(
    test PRIVATE
    spdlog::spdlog
    sfml-system
    sfml-window
    sfml-graphics
    sfml-network
    sfml-audio
    tinyxml2
    Catch2::Catch2WithMain
)

# Scan through resource folder for updated files and copy if none existing or changed
file (GLOB_RECURSE resources "../../resources/*.*")
foreach(resource ${resources})
    get_filename_component(filename ${resource} NAME)
    get_filename_component(dir ${resource} DIRECTORY)
    get_filename_component(dirname ${dir} NAME)

    set (output "")

    while(NOT ${dirname} STREQUAL resources)
        get_filename_component(path_component ${dir} NAME)
        set (output "${path_component}/${output}")
        get_filename_component(dir ${dir} DIRECTORY)
        get_filename_component(dirname ${dir} NAME)
    endwhile()

    set(output "${CMAKE_CURRENT_BINARY_DIR}/resources/${output}${filename}")

    add_custom_command(
        COMMENT "Moving updated resource-file '${filename}'"
        OUTPUT ${output}
        DEPENDS ${resource}
        COMMAND ${CMAKE_COMMAND} -E copy_if_different
        ${resource}
        ${output}
        )
    add_custom_target(${filename} ALL DEPENDS ${resource} ${output})

endforeach()
