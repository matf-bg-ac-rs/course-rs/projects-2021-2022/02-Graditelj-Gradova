#include <catch2/catch_test_macros.hpp>

#include<tile/TerrainTile.hpp>
#include<resources/Attributes.hpp>

#include<SFML/Graphics.hpp>

gg::TerrainTile* makeTestTile(gg::TerrainType type) {
	std::string typeStr = gg::TerrainTile::TerrainTypeToString(type);

	sf::Vector2f coordinates(0.0f, 0.0f);
	unsigned int rotation = 0;

	gg::TerrainTile* tile = new gg::TerrainTile(coordinates, type, rotation);

	return tile;
}

TEST_CASE("Terrain tile testing!", "[class]") {

	SECTION("StringToTerrainType returns TerrainType::GRASS for \"grass\" ") {
		std::string str = "grass";

		gg::TerrainType result = gg::TerrainTile::StringToTerrainType(str);
		gg::TerrainType expected = gg::TerrainType::GRASS;

		REQUIRE(result == expected);
	}

	SECTION("Water tile returns isWater is true") {
		gg::TerrainTile* tile = makeTestTile(gg::TerrainType::WATER);

		bool result = tile->IsWater();
		bool expected = true;

		REQUIRE(result == expected);
	}

	SECTION("Grass tile returns isGrass is true") {
		gg::TerrainTile* tile = makeTestTile(gg::TerrainType::GRASS);

		bool result = tile->IsGrass();

		REQUIRE(result == true);
	}

	SECTION("GetAttributes returns 3 attributes: type, terrain_type, rotation") {
		gg::TerrainTile* tile = makeTestTile(gg::TerrainType::GRASS);

		gg::Attributes result = tile->GetAttributes();

		bool hasType = result.Has("type");
		bool hasTerrainType = result.Has("terrain_type");
		bool hasRotation = result.Has("rotation");

		REQUIRE(hasType == true);
		REQUIRE(hasTerrainType == true);
		REQUIRE(hasRotation == true);
	}

	SECTION("GetTileType returns Terrain") {
		gg::TerrainTile* tile = makeTestTile(gg::TerrainType::GRASS);

		gg::TileType result = tile->GetTileType();
		gg::TileType expected = gg::TileType::Terrain;

		REQUIRE(result == expected);
	}

	SECTION("TerrainTypeToString returns \"grass\" for terrain type grass") {
		gg::TerrainTile* tile = makeTestTile(gg::TerrainType::GRASS);

		gg::TerrainType type = tile->GetTerrainType();

		std::string result = gg::TerrainTile::TerrainTypeToString(type);
		std::string expected = "grass";

		REQUIRE(result == expected);
	}

	SECTION("Align moves the coordinates if the terrain_type is water") {
		gg::TerrainTile* tile = makeTestTile(gg::TerrainType::GRASS);

		sf::Vector2f oldCoordinates = tile->GetCoordinates();
		tile->Align();
		sf::Vector2f result = tile->GetCoordinates();

		REQUIRE_FALSE(result == oldCoordinates);
	}
}