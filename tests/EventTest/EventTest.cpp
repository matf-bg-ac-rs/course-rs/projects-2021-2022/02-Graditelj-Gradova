#include <catch2/catch_test_macros.hpp>
#include <core/Event.hpp>
#include "TestClass.cpp"
#include <iostream>
TEST_CASE("IEvent testing constructor", "[IEvent][Constructor]")
{
    SECTION("IEvent can be instantiated only if virtual method GetEventType is implemented")
    {
        TestClassIEvent *event = new TestClassIEvent();
        // Arrange
        auto expected = gg::EventType::TestEvent;

        // Act
        auto result = event->GetEventType();

        // Assert
        REQUIRE(result == expected);
    }

}

TEST_CASE("IEventSubscriber constructor", "[IEventSubscriber][Constructor]")
{
    SECTION("When IEventSubscriber is created, it DOES NOT need to implement OnEvent right away because it is already superficially implemented")
    {
        TestClassIEventSubscriber *event_subscriber;

        event_subscriber = new TestClassIEventSubscriber();

    }

    SECTION("IEventSubscriber can implement OnEvent virtual function")
    {
        TestClassIEventSubscriber *event_subscriber;
        event_subscriber = new TestClassIEventSubscriber();
        auto expected = gg::EventType::TestEvent;
        auto event = new TestClassIEvent();


        event_subscriber->OnEvent(event);
        auto result = event_subscriber->m_test;

        REQUIRE(result == expected);

    }
}

TEST_CASE("IEventSubscriber basic methods", "[IEventSubscriber][Method]")
{
    SECTION("When IEventSubscriber is not subscribed to any events, number of subscribed events should be 0 and it should not be in subscribers map")
    {
        auto event_subscriber = new TestClassIEventSubscriber();
        size_t expected_number_of_events = 0;
        bool expected_exists = false;

        size_t result_number_of_events = event_subscriber->GetNumberOfSubscribedEvents();
        bool result_exists = gg::CheckSubscriberExistence(event_subscriber);

        REQUIRE(result_number_of_events == expected_number_of_events);
        REQUIRE(result_exists == expected_exists);
    }

}

TEST_CASE("EventDispatcher singleton constructor", "[EventDispatcher][Constructor]")
{
    SECTION("You CAN NOT make an instance of EventDispatcher therefore there can only be one instance of EventDispatcher")
    {
        // auto dispatcher = new gg::EventDispatcher(); -> COMPILING ERROR
        auto& dispatcher_1 = gg::EventDispatcher::GetInstance();
        auto& dispatcher_2 = gg::EventDispatcher::GetInstance();

        //REQUIRE(dispatcher_1 == dispatcher_2); -> could be tested if there was "==" operator

    }

}

TEST_CASE("EventDispatcher Subscribe method", "[EventDispatcher][Method][Subscribe]")
{
    SECTION("When IEventSubscriber is subscribed to one event, number of subscribed events should be 1")
    {
        auto event_subscriber = new TestClassIEventSubscriber();
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::TestEvent);
        size_t expected = 1;

        size_t result = event_subscriber->GetNumberOfSubscribedEvents();

        REQUIRE(result == expected);
    }

    SECTION("When IEventSubscriber is subscribed to one event multiple times, number of subscribed events should be 1")
    {
        auto event_subscriber = new TestClassIEventSubscriber();
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::TestEvent);
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::TestEvent);
        size_t expected = 1;

        size_t result = event_subscriber->GetNumberOfSubscribedEvents();

        REQUIRE(result == expected);
    }

    SECTION("When IEventSubscriber is subscribed to multiple events, number of subscribed events should be the number of unique events he is subbed to")
    {
        auto event_subscriber = new TestClassIEventSubscriber();
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::TestEvent);
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::WindowResize);
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::TestEvent);
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::ButtonClicked);
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::WindowResize);
        size_t expected = 3;

        size_t result = event_subscriber->GetNumberOfSubscribedEvents();

        REQUIRE(result == expected);
    }
}

TEST_CASE("EventDispatcher Unsubscribe method", "[EventDispatcher][Method][Unsubscribe]")
{
    SECTION("When IEventSubscriber is subscribed to zero events, unsubscribing it from any event type leaves number of subbed events for it at 0")
    {
        auto event_subscriber = new TestClassIEventSubscriber();
        gg::EventDispatcher::Unsubscribe(event_subscriber, gg::EventType::TestEvent);
        size_t expected_number_of_events = 0;

        size_t result_number_of_events = event_subscriber->GetNumberOfSubscribedEvents();

        REQUIRE(result_number_of_events == expected_number_of_events);
    }

    SECTION("When IEventSubscriber is subscribed to one event type, unsubscribing it from that event leaves number of subbed events for it at 0 and is removed from subscribers map")
    {
        auto event_subscriber = new TestClassIEventSubscriber();
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::TestEvent);
        gg::EventDispatcher::Unsubscribe(event_subscriber, gg::EventType::TestEvent);
        size_t expected_number_of_events = 0;
        bool expected_exists = false;

        size_t result_number_of_events = event_subscriber->GetNumberOfSubscribedEvents();
        bool result_exists = gg::CheckSubscriberExistence(event_subscriber);

        REQUIRE(result_number_of_events == expected_number_of_events);
        REQUIRE(result_exists == expected_exists);
    }

    SECTION("When IEventSubscriber is subscribed to one event, unsubscribing it from any other event leaves number of subbed events for it at 1 and it remains in subscribers map")
    {
        auto event_subscriber = new TestClassIEventSubscriber();
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::TestEvent);
        gg::EventDispatcher::Unsubscribe(event_subscriber, gg::EventType::WindowResize);
        gg::EventDispatcher::Unsubscribe(event_subscriber, gg::EventType::ToggleButtonClicked);
        size_t expected_number_of_events = 1;
        bool expected_exists = true;

        size_t result_number_of_events = event_subscriber->GetNumberOfSubscribedEvents();
        bool result_exists = gg::CheckSubscriberExistence(event_subscriber);

        REQUIRE(result_number_of_events == expected_number_of_events);
        REQUIRE(result_exists == expected_exists);
    }


    SECTION("When IEventSubscriber is subscribed to more than one event, unsubscribing it from one of those events will leave it at one less subbed events")
    {
        auto event_subscriber = new TestClassIEventSubscriber();
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::TestEvent);
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::WindowResize);
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::ToggleButtonClicked);
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::KeyboardReleased);
        gg::EventDispatcher::Unsubscribe(event_subscriber, gg::EventType::ToggleButtonClicked);
        size_t expected_number_of_events = 3;
        bool expected_exists = true;

        size_t result_number_of_events = event_subscriber->GetNumberOfSubscribedEvents();
        bool result_exists = gg::CheckSubscriberExistence(event_subscriber);

        REQUIRE(result_number_of_events == expected_number_of_events);
        REQUIRE(result_exists == expected_exists);
    }

    SECTION("When IEventSubscriber is subscribed to more than one event, unsubscribing it from all of those events will leave it at 0 events subbed and it will be removed from subscribers map")
    {
        auto event_subscriber = new TestClassIEventSubscriber();
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::TestEvent);
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::WindowResize);
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::ToggleButtonClicked);
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::KeyboardReleased);
        gg::EventDispatcher::Unsubscribe(event_subscriber, gg::EventType::ToggleButtonClicked);
        gg::EventDispatcher::Unsubscribe(event_subscriber, gg::EventType::KeyboardReleased);
        gg::EventDispatcher::Unsubscribe(event_subscriber, gg::EventType::TestEvent);
        gg::EventDispatcher::Unsubscribe(event_subscriber, gg::EventType::WindowResize);

        size_t expected_number_of_events = 0;
        bool expected_exists = false;

        size_t result_number_of_events = event_subscriber->GetNumberOfSubscribedEvents();
        bool result_exists = gg::CheckSubscriberExistence(event_subscriber);

        REQUIRE(result_number_of_events == expected_number_of_events);
        REQUIRE(result_exists == expected_exists);
    }

}

TEST_CASE("EventDispatcher RemoveSubscriber method", "[EventDispatcher][Method][RemoveSubscriber]")
{
    SECTION("When IEventSubscriber is not subbed to any events and is removed from subscribers, its number of subscribed events will be 0 and it won't be in subscribers map")
    {
        auto event_subscriber = new TestClassIEventSubscriber();
        gg::EventDispatcher::RemoveSubscriber(event_subscriber);
        size_t expected_number_of_events = 0;
        bool expected_exists = false;

        size_t result_number_of_events = event_subscriber->GetNumberOfSubscribedEvents();
        bool result_exists = gg::CheckSubscriberExistence(event_subscriber);

        REQUIRE(result_number_of_events == expected_number_of_events);
        REQUIRE(result_exists == expected_exists);

    }

    SECTION("When IEventSubscriber is subbed to 1 event and is removed from subscribers, its number of subscribed events will be 0 and it won't be in subscribers map")
    {
        auto event_subscriber = new TestClassIEventSubscriber();
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::TestEvent);
        gg::EventDispatcher::RemoveSubscriber(event_subscriber);
        size_t expected_number_of_events = 0;
        bool expected_exists = false;

        size_t result_number_of_events = event_subscriber->GetNumberOfSubscribedEvents();
        bool result_exists = gg::CheckSubscriberExistence(event_subscriber);

        REQUIRE(result_number_of_events == expected_number_of_events);
        REQUIRE(result_exists == expected_exists);

    }

    SECTION("When IEventSubscriber is subbed to more than 1 event and is removed from subscribers, its number of subscribed events will be 0 and it won't be in subscribers map")
    {
        auto event_subscriber = new TestClassIEventSubscriber();
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::TestEvent);
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::TextInserted);
        gg::EventDispatcher::RemoveSubscriber(event_subscriber);
        size_t expected_number_of_events = 0;
        bool expected_exists = false;

        size_t result_number_of_events = event_subscriber->GetNumberOfSubscribedEvents();
        bool result_exists = gg::CheckSubscriberExistence(event_subscriber);

        REQUIRE(result_number_of_events == expected_number_of_events);
        REQUIRE(result_exists == expected_exists);

    }
}

TEST_CASE("EventDispatcher NumberOfSubscribers method", "[EventDispatcher][Method][NumberOfSubscribers]")
{
    SECTION("When there are no subscribers in the map, number of subscribers should be 0")
    {
        auto expected_number_of_subscribers = 5; // 5 subs we've added during testing above

        auto result_number_of_subscribers = gg::EventDispatcher::NumberOfSubscribers();

        REQUIRE(result_number_of_subscribers == expected_number_of_subscribers);
    }

    SECTION("When subscriber monitors at least one event, it will be added to  map and number of subscribers should be increased by 1")
    {
        auto event_subscriber = new TestClassIEventSubscriber();
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::TestEvent);
        auto expected_number_of_subscribers = 6;

        auto result_number_of_subscribers = gg::EventDispatcher::NumberOfSubscribers();

        REQUIRE(result_number_of_subscribers == expected_number_of_subscribers);
    }

    SECTION("When subscriber monitors more than one event, it will be added to  map and number of subscribers should be increased by 1")
    {
        auto event_subscriber = new TestClassIEventSubscriber();
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::TestEvent);
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::MousePressed);
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::TimePassed);
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::ToggleButtonClicked);
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::None);
        auto expected_number_of_subscribers = 7; // because we've added another sub above and this one

        auto result_number_of_subscribers = gg::EventDispatcher::NumberOfSubscribers();

        REQUIRE(result_number_of_subscribers == expected_number_of_subscribers);
    }
}

TEST_CASE("EventDispatcher NotifySubscribers method", "[EventDispatcher][Method][NotifySubscribers]")
{
    SECTION("When IEventSubscriber is listening for an event, NotifySubscribers will alert him of an event that he is listening to and its OnEvent will be triggered")
    {
        auto event_subscriber = new TestClassIEventSubscriber();
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::TestEvent);
        auto event_type = new TestClassIEvent();
        auto expected_response = "I was notified that event happened";
        auto expected_event_type = gg::EventType::TestEvent;

        gg::EventDispatcher::NotifySubscribers(event_type);
        auto result_response = event_subscriber->m_i_was_notified;
        auto result_event_type = event_subscriber->m_test;

        REQUIRE(result_event_type == expected_event_type);
        REQUIRE(result_response == expected_response);

    }

    SECTION("When IEventSubscriber is not listening for a specific event, NotifySubscribers will not alert when an event of that type happens")
    {
        auto event_subscriber = new TestClassIEventSubscriber();
        gg::EventDispatcher::Subscribe(event_subscriber, gg::EventType::ButtonClicked);
        auto event_type = new TestClassIEvent();
        auto expected_response = "I WAS NOT notified that any of my events happened...";

        gg::EventDispatcher::NotifySubscribers(event_type);
        auto result_response = event_subscriber->m_i_was_notified;
        
        REQUIRE(result_response == expected_response);

    }
}