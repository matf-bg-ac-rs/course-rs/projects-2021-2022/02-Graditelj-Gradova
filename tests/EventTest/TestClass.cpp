#include <core/Event.hpp>

class TestClassIEvent : public gg::IEvent {
public:
    TestClassIEvent() {
    }

    inline gg::EventType GetEventType() const override {
        return gg::EventType::TestEvent;
    }

};

class TestClassIEventSubscriber : public gg::IEventSubscriber {
public:
    TestClassIEventSubscriber() {
    }

    inline void OnEvent(gg::IEvent *e) {
        m_test = e->GetEventType();
        m_i_was_notified = "I was notified that event happened";
    }

public:
    gg::EventType m_test;
    std::string m_i_was_notified = "I WAS NOT notified that any of my events happened...";

};
