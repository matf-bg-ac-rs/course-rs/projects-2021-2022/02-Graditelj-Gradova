#include<catch2/catch_test_macros.hpp>

#include<tile/BuildingTile.hpp>
#include<resources/Attributes.hpp>


gg::BuildingTile makeTestTile(gg::BuildingType type) {
    std::string typeStr = gg::BuildingTile::BuildingTypeToString(type);

    gg::Attributes attributes;
    attributes.Add("building_type", typeStr);
    attributes.Add("coordinates", "0.0 0.0");
    attributes.Add("rotation", "0");
    attributes.Add("cost", "50");
    attributes.Add("population", "100");
    attributes.Add("profit", "10");

    return gg::BuildingTile(attributes);
}

TEST_CASE("Building tile testing!", "[class]") {

    SECTION("IsResidential returns true for residential building") {
        gg::BuildingTile tile = makeTestTile(gg::BuildingType::RESIDENTIAL);

        bool expected = true;
        bool result = tile.IsResidential();

        REQUIRE(result == expected);
    }

    SECTION("IsIndustrial returns true for industrial building")
    {
        gg::BuildingTile tile = makeTestTile(gg::BuildingType::INDUSTRIAL);

        bool expected = true;
        bool result = tile.IsIndustrial();

        REQUIRE(result == expected);
    }

    SECTION("Get attributes returns type, building_type, cost, population, profit rotation") {
        gg::BuildingTile tile = makeTestTile(gg::BuildingType::RESIDENTIAL);

        gg::Attributes result = tile.GetAttributes();

        bool hasType = result.Has("type");
        bool hasBuildingType = result.Has("building_type");
        bool hasCost = result.Has("cost");
        bool hasPopulation = result.Has("population");
        bool hasProfit = result.Has("profit");
        bool hasRotation = result.Has("rotation");

        REQUIRE(hasType == true);
        REQUIRE(hasBuildingType == true);
        REQUIRE(hasCost == true);
        REQUIRE(hasPopulation == true);
        REQUIRE(hasProfit == true);
        REQUIRE(hasRotation == true);
    }

    SECTION("GetTileType returns Building type") {
        gg::BuildingTile tile = makeTestTile(gg::BuildingType::RESIDENTIAL);

        gg::TileType expected = gg::TileType::Building;
        gg::TileType result = tile.GetTileType();

        REQUIRE(result == expected);
    }

    SECTION("GetCost returns correct cost") {
        gg::BuildingTile tile = makeTestTile(gg::BuildingType::RESIDENTIAL);

        unsigned expected = 50;
        unsigned result = tile.GetCost();

        REQUIRE(result == expected);
    }

    SECTION("GetPopulation returns correct population") {
        gg::BuildingTile tile = makeTestTile(gg::BuildingType::RESIDENTIAL);

        unsigned expected = 100;
        unsigned result = tile.GetPopulation();

        REQUIRE(result == expected);
    }

    SECTION("GetProfit returns correct profit") {
        gg::BuildingTile tile = makeTestTile(gg::BuildingType::RESIDENTIAL);

        int expected = 10;
        int result = tile.GetProfit();

        REQUIRE(result == expected);
    }

    SECTION("BuildingTypeToString returns \"residential\" for residential buildingType") {
        gg::BuildingTile tile = makeTestTile(gg::BuildingType::RESIDENTIAL);

        std::string expected = "residential";
        std::string result = gg::BuildingTile::BuildingTypeToString(tile.GetBuildingType());

        REQUIRE(result == expected);
    }

    SECTION("StringToBuldingType returns residential type for string \"residential\" ") {
        std::string input = "residential";

        gg::BuildingType expected = gg::BuildingType::RESIDENTIAL;
        gg::BuildingType result = gg::BuildingTile::StringToBuildingType(input);

        REQUIRE(result == expected);
    }

    SECTION("Align moves a building tile") {
        gg::BuildingTile tile = makeTestTile(gg::BuildingType::RESIDENTIAL);

        sf::Vector2f oldCoordinates = tile.GetCoordinates();
        tile.Align();
        sf::Vector2f newCoordinates = tile.GetCoordinates();

        REQUIRE_FALSE(oldCoordinates == newCoordinates);
    }
}