# Componenets Diagram

![Components Diagram](components_diagram.png)

# Use Case Diagram

![Use Case Diagram](use_case_diagram.png)

## Use Case: `Playing game`

* **Name**: Playing name
* **Actors**: Player
* **Short description**:
Player starts game from main menu. Application load parameters of chosen game from list of saved, or uses default parameters if new game is chosen. Application then loads game settings, create game and render game window. Random events may happen and game parameters are changed depending on events that occured. Player has options to build objects, build nature and naviagte the map, check on tiles and objects and get informations about them.  
* **Event flow**:
* 1. Player starts game from main menu
  * 1.1 If he chose load game:
    * 1.1.1 He picks a game from list of saved games
    * 1.1.2 Application loads parameters of chosen game(state of map, population, money, taxes, happiness, time passed)
    * 1.1.3 Go to step 2.
* 2. Application fetheces game settings
* 3. Application creates game (with settings and parameters)
* 4. Application return game window to player
* 5. These steps are repeated untill player stops the game and quit it:
  * 5.1 Time is updated
  * 5.2 Money is updated depending on population and taxes
  * 5.3 If random event occured:
    * 5.3.1 If object is on fire:
      * 5.3.1.1 Money decreases to pay firefighters
      * 5.3.1.2 Population decreases for portion of current population in object
    * 5.3.2 If new people came to city
      * 5.3.2.1 Overall population increases
  * 5.4 Application updates games window
  * 5.5 If player wants to build objects:
    * 5.5.1 Go to Uce case city building, when done, go to 5.
  * 5.6 If player wants to change nature by changing kind of tiles:
    * 5.6.1 Go to Use case terraforming, when done, go to 5. 
  * 5.7 If player wants to navigate map
    * 5.7.1 Go to use case navigating map, when done, go to 5.
  * 5.8 If player presses pause button
    * 5.8.1 Application gives player the pause menu
      * 5.8.1.1 If player presses quit button
      * 5.8.1.2 Current game is saved
      * 5.8.1.3 Go to 6.
  * 5.9 Go to step 5.
* 6. Application returns main menu
	
* **Preconditions**:
* **Postconditions**:
* **Sequence diagram**:

![Sequence Diagram: `Playing game`](playing_game_sequence_diagram.png)

<!---

## Use Case: `Main menu`

* **Name**: Main menu
* **Actors**: Player
* **Short Description**: Player starts the game. Player select one of the
  actions that main menu offers.
* **Event Flow**:
  * **Basic Flow**:
    1. Player starts Application, quits current `Gameplay`,
       returns from either `New Game`, `Load Game`, `Settings`.
    2. Application show main menu, which has following buttons: New Game,
       Load Game, Settings, Exit.
    3. Player selects one of the buttons:
       * If New Game is selected, then `New Game` use case precides.
       * If Load Game is selected, then `Load Game` use case precides.
       * If Settings is selected, then `Settings` use case precides.
       * If Exit is selected, then Application terminates.
  * **Alternative Flow**:
    1. If Player forcefully quits Application, Application just terminates.
* **Preconditions**: Application is closed, or in one of the
  following states: `Gameplay`, `New Game`, `Load Game`, `Settings`.
* **Postconditions**: /
* **Sequence Diagram**:

![Sequence Diagram: `Main menu`](main_menu_sequence_diagram.png)

## Use Case: `New game`
- **Name**: New game
- **Actors**: Player
- **Short Description**: Player selects *New game* option from the main menu and has a new window in front of him that he ought to go
through to start a new game.
- **Event Flow**:
	1. Player selects the *New game* button from the main menu.
	2. Until the player chooses ***Start game*** or ***Back*** button, the ***Create new game*** window remains on the screen.
		- 2.1. In an input field of that window player inserts the new game ***name***.
        - 2.2 If the player chooses Start game button and did insert the new game name, **initialize map and game settings loading**.
			- 2.2.1. Close *create new game* and open the ***gameplay*** window
		- 2.3. If the player chooses *Start game* button but did not insert a new game name, highlight that input field for him in red and go to **2.1.**
		- 2.4. If the player chooses the *Back* button bring him back to the *main window*.
* **Preconditions**: Application is in `Main menu` state.
* **Postconditions**: /
* **Sequence Diagram**:
![Sequence Diagram: `New game`](new_game_sequence_diagram.png)

-->

## Use Case: `Settings`
- **Name**: Settings
- **Actors**: Player
- **Short Description**: Player selects *Settings* from the main menu and has a new window in front of him where he can change desired ***game settings***.
- **Event Flow**:
	1. Player selects *Settings* button from the main menu.
	2. Until the player chooses ***Back to main menu*** button, the ***Settings*** window remains on the screen.
		- 2.1 If player selects:
			- 2.1.1 ***Windowed*** || ***Fullscreen*** toggle, the Application Window changes size according to previous state of the toggle position:
				- 2.1.1.1 If the toggle was ON, toggle is now OFF and the Window is in Windowed mode.
				- 2.1.1.2 If the toggle was OFF, toggle is now ON and the Window is in Fullscreen mode.
			- 2.1.2 ***Music*** *ON* || *OFF* toggle, music starts or stops according to the previous state of the toggle position:
				- 2.1.2.1 If the toggle was ON, toggle is now OFF and the Music is turned OFF.
				- 2.1.2.2 If the toggle was OFF, toggle is now ON and the Music if turned ON.
			- 2.1.3 Player selects *Back to main menu* button.
* **Preconditions**: Application is in `Main menu` state.
* **Postconditions**: Application is in `Main menu` state.
* **Sequence Diagram**:
![Sequence Diagram: `Settings`](settings_sequence_diagram.png)

<!---

## Use Case: `Load game`
- **Name**: Load game
- **Actors**: Player
- **Short Description**: Player selects *Load game* option from the main menu and goes to the load game window and selects a loaded game.
- **Event Flow**:
  1. Player selects the *Load game* button from the main menu
  2. Player chooses a saved game listed in the load game window
   * If the player selects the *Load game* button
    * If the selected game can be loaded close *Load game* and open the **gameplay** window 
    * If the selected game can't be loaded a message will be displayed on the screen and go to **2.**
   * If the player chooses the *Back* button bring him back to the *main window*
* **Preconditions**: Application is in 'Main menu' state.
* **Postconditions**: /
* **Sequence Diagram**:
![Sequence Diagram: `Load game`](load_game_sequence_diagram.png)

## Use Case: `Gameplay`

## Use Case: `Pause`
- **Name**: Pause
- **Actors**: Player
- **Short Description**: Player pauses the game. He can choose from one of the options in the pause menu.
- **Event Flow**:
  1. Player pauses the game from *Gameplay*.
  2. If player selects *Resume* return to *Gameplay*
    * If player pauses return to **1.**
  3. If player selects *Save game* open save game menu
    * player enters save game name
    * player selects *Save* button and the game is saved
    * If player selects *Back* button return to **1.** 
  4. If player selects *Settings* button go to *Settings*
    * If player selects *Back* button return to **1.**
  5. If player selects *Quit* button go to *Main menu*.
* **Preconditions**: Aplication is in *Gameplay*
* **Postconditions**: /
* **Sequence Diagram**:
![Sequence Diagram: `Pause`](pause_sequence_diagram.png)

-->

## Use Case: `Navigating Map`
* **Name**: Navigating Map
* **Actors**: Player
* **Short Description**: Player is given a choice to navigate map. Player can
  wonder around Map and zoom in/out of some area. Player can select some Tile 
  on Map to get status of that Tile.
* **Event Flow**:
  * 1. Application is in UC:`Playing Game` and is waiting for Player.
  * 2. Application receive some navigating map event from Player.
  * 3. If application receives Camera Movement event:
    * 3.1. Movement to Left: Map shifts to Right;
    * 3.2. Movement to Right: Map shifts to Left;
    * 3.3. Movement to Up: Map shifts to Down;
    * 3.4. Movement to Down: Map shifts to Up.
  * 4. If application receives Camera Zoom event:
    * 4.1. Zoom In: Map zooms in to center of Camera;
    * 4.2. Zoom Out: Map zooms out of center of Camera.
  * 5. If application receives Select Tile event:
    * 5.1. Tile is Building: Shows status of that Building:
      * 5.1.1. Status Window pop up.
      * 5.1.2. Status Window describes Building type.
      * 5.1.3. Status Window describes population in Building.
      * 5.1.4. Status Window describes additional info.
      * 5.1.5. Player can close status Window.
    * 5.2. Tile is Road: 
      * 5.2.1. Status Window pop up.
      * 5.2.2. Status Window describes Road type.
      * 5.2.3. Status Window describes connectivnes of the Road.
      * 5.2.4. Status Window describes additional info.
      * 5.2.5. Player can close status Window.
    * 5.3. Tile is Empty: 
      * 5.3.1. Status Window pop up.
      * 5.3.2. Status Windows describes basic Tile type (Dirt, Water,...).
      * 5.3.3. Player can close status Window.

* **Preconditions**: Application must be in UC: `Playing Game`.
* **Postconditions**: /
* **Sequence Diagram**:

![Sequence Diagram: `Navigating Map`](navigating_map_sequence_diagram.png)

## Use Case: `Building`
* **Name**: Building
* **Actors**: Player
* **Short Description**: Player selects an item from the build menu, and then selects
the location where the building will be placed or demolished.
* **Event Flow**:
* 1. Player selects an item from the build menu.
  * 1.1. If item was already selected player can continue to step 2.
  * 1.2. Player can select demolish.
  * 1.3. Player can select a building.
* 2. Select a tile
  * 2.1. If the player has previously selected a building and there is
  space for it on the map go to step 3.
  * 2.2. If the player has previously selected demolish and there is
  a building on that tile map go to step 4.
  * 2.3.Else go to step 2.
* 3. Place building on map.
* 4. Remove building from map.
* 5. If the player unselects an item from the build menu exit use case.
  * 5.1.Else go back to step 1.

* **Preconditions**: Application must be in UC : `Playing Game`.
* **Postconditions**: /
* **Sequence Diagram**:

![Sequence Diagram: `Building`](building_sequence_diagram.png)


## Use Case: `Terraforming Map`
* **Name**: Terraforming Map
* **Actors**: Player
* **Short Description**: Allows the Player to change the Map landscape. Player is given a few options to modify Tiles on the Map of his choice.
* **Event Flow**:
  * 1. Application is in UC:`Playing Game` and is waiting for Player.
  * 2. Application receives terraforming event from Player.
  * 3. Application offers menu to choose which Tile the Player wants to modify the Map with.
  * 4. If Application receives select tile event on the menu:
    * 4.1. Select Tile: Player selects a Tile to modify with
      * 4.1.1. If Player clicks on a Tile on the Map:
        * 4.1.1.1. Change desired Tile to Tile selected
      * 4.1.2. If Player uses Right Mouse button:
        * 4.1.2.1. Return to 1
    * 4.2. Player clicks out of bounds of the menu: return to 1

* **Preconditions**: Application must be in UC: `Playing Game`.
* **Postconditions**: /
* **Sequence Diagram**:

![Sequence Diagram: `Terraforming Map`](terraforming_sequence_diagram.png)

## `Class diagram`
![`Class diagram`](class_diagram.png)

## `Implemented class diagram`
![Implemented Class Diagram:](ImplementedClassDIagram.jpeg)

## `Demo video:`
https://drive.google.com/file/d/16deIhXKMNg49i1lMhWvcI1t-jbq5_yAw/view?usp=sharing
